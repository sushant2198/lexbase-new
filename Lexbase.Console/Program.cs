﻿using Lexbase.Jobs;
using log4net;
using System;

namespace Lexbase
{
    internal class Program
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        private static void Main(string[] args)
        {
            string jobParam = "";
            string jobName = "";
            if (args != null && args.Length > 0)
            {
                try
                {
                    jobName = args[0];

                    switch (jobName.ToUpper())
                    {
                        case "ACCOUNTACTIVATION":
                            jobName = "AccountActivation";
                            break;

                        case "CHILDOBLIGATIONS":
                            jobName = "ChildObligations";
                            break;

                        case "NOTIFICATIONBYTIMEZONE":
                            jobName = "NotificationByTimezone";
                            break;

                        case "ACCOUNTSTATUS":
                            jobName = "AccountStatus";
                            break;

                        case "MARKOVERDUE":
                            jobName = "MarkOverdue";
                            break;

                        case "COBREPORT":
                            jobName = "COBReport";
                            break;

                        case "HEATMAPREPORT":
                            jobName = "HeatMapReport";
                            break;

                        case "ACCOUNTDUMP":
                            jobName = "AccountDump";
                            break;

                        case "TRENDREPORT":
                            jobName = "TrendReport";
                            break;

                        case "ACCOUNTINFORMATIONREPORT":
                            jobName = "AccountInformationReport";
                            break;

                        case "CHANGEREQUESTNOTIFICATION":
                            jobName = "ChangeRequestNotification";
                            break;

                        case "REINDEX":
                            jobName = "ReIndex";
                            break;

                        case "SCHEDULEDREPORT":
                            jobName = "ScheduledReport";
                            break;

                        case "DELETETEMPBLOB":
                            jobName = "DeleteTempBlob";
                            break;

                        case "USERAUDITLOG":
                            jobName = "UserAuditLog";
                            break;

                        default:
                            log.Error($"Invalid Job: {jobName}");
                            return;
                    }
                    if (args.Length > 1)
                    {
                        jobParam = args[1];
                    }
                }
                catch (Exception ex)
                {
                    log.Info(ex.Message);
                }
            }
            else
            {
                log.Info("Using Defaults");
                log.Info("You can specify Job ( AccountActivation)");
                return;
            }

            log.Info($"Job: {jobName}, Param: {jobParam} Environment: {Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "production"}");

            Context context = Context.Current;
            StartUp.Configure(context);

            IJob job = null;
            try
            {
                switch (jobName.ToUpper())
                {
                    case "ACCOUNTACTIVATION":
                        job = Context.Current.GetService<IAccountActivationProcessJob>();
                        break;

                    case "CHILDOBLIGATIONS":
                        job = Context.Current.GetService<ISchedulerJob>();
                        break;

                    case "NOTIFICATIONBYTIMEZONE":
                        job = Context.Current.GetService<INotificationJob>();
                        break;

                    case "ACCOUNTSTATUS":
                        job = Context.Current.GetService<IAccountStatusProcessJob>();
                        break;

                    case "MARKOVERDUE":
                        job = Context.Current.GetService<IMarkOverDueJob>();
                        break;

                    case "COBREPORT":
                        job = Context.Current.GetService<ICOBReportJob>();
                        break;

                    case "HEATMAPREPORT":
                        job = Context.Current.GetService<IHeatMapReportJob>();
                        break;

                    case "ACCOUNTDUMP":
                        job = Context.Current.GetService<IAccountDumpJob>();
                        break;

                    case "TRENDREPORT":
                        job = Context.Current.GetService<ITrendReportJob>();
                        break;

                    case "REINDEX":
                        job = Context.Current.GetService<IReIndexJob>();
                        break;

                    case "ACCOUNTINFORMATIONREPORT":
                        job = Context.Current.GetService<IInformationReportJob>();
                        break;

                    case "CHANGEREQUESTNOTIFICATION":
                        job = Context.Current.GetService<IChangeRequestNotificationJob>();
                        break;

                    case "SCHEDULEDREPORT":
                        job = Context.Current.GetService<IScheduledReportJob>();
                        break;

                    case "USERAUDITLOG":
                        job = Context.Current.GetService<IUserAuditLogJob>();
                        break;

                    case "DELETETEMPBLOB":
                        job = Context.Current.GetService<IDeleteTempBlobJob>();
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Info(ex.Message);
            }

            job.Execute(jobParam);
        }
    }
}