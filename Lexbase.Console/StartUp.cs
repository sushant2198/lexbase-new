using System.IO;
using System.Reflection;
using System.Xml;

namespace Lexbase
{
    public static class StartUp
    {
        public static void Configure(Context context)
        {
            if (!context.IsDebug)
            {
                context.Register(new Implementation.Register.RegisterServices());
                context.Register(new Implementation.Register.RegisterRepositories());
                context.Register(new Implementation.Register.RegisterJobs());
                context.Register(new Implementation.Register.RegisterProviders());
            }
            else
            {
                context.Register(new Mocks.Register.RegisterServices());
                context.Register(new Mocks.Register.RegisterRepositories());
                context.Register(new Mocks.Register.RegisterJobs());
                context.Register(new Mocks.Register.RegisterProviders());
            }

            XmlDocument log4netConfig = new XmlDocument();
            log4netConfig.Load(inStream: File.OpenRead("log4net.config"));

            var repo = log4net.LogManager.CreateRepository(
                Assembly.GetEntryAssembly(), typeof(log4net.Repository.Hierarchy.Hierarchy));

            log4net.Config.XmlConfigurator.Configure(repo, log4netConfig["log4net"]);
        }
    }
}