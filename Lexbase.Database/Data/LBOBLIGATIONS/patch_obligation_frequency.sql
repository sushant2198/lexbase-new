INSERT INTO
        obligation_frequency (
            Name,
            Id,
            Advance_by_Months,
            Is_Active
            )
Values (
    'Two Months',
    (SELECT MAX(ID) +1 FROM obligation_frequency),
    3,
    1
    );


INSERT INTO 
    OBLIGATION_REMINDER_FREQUENCY (
        REMINDER_ID,
        DAYS_BACK,
        ID,
        OBLIGATION_FREQUENCY_ID,
        TO_LIST,
        RETURN_RECEIPT,
        TO_BE_CLUBBED
    ) 
VALUES (
        (SELECT REMINDER_ID FROM OBLIGATION_REMINDER WHERE REMINDER_NAME = 'Reminder 1'),
        5,
        (SELECT MAX(ID) +1 FROM OBLIGATION_REMINDER_FREQUENCY),
        (SELECT ID FROM OBLIGATION_FREQUENCY WHERE NAME = 'Two Months'),
        '[{"id":101}]',
        0,
        1
    );
    
INSERT INTO 
    OBLIGATION_REMINDER_FREQUENCY (
        REMINDER_ID,
        DAYS_BACK,
        ID,
        OBLIGATION_FREQUENCY_ID,
        TO_LIST,
        RETURN_RECEIPT,
        TO_BE_CLUBBED
    ) 
VALUES (
        (SELECT REMINDER_ID FROM OBLIGATION_REMINDER WHERE REMINDER_NAME = 'Escalation'),
        5,
        (SELECT MAX(ID) +1 FROM OBLIGATION_REMINDER_FREQUENCY),
        (SELECT ID FROM OBLIGATION_FREQUENCY WHERE NAME = 'Two Months'),
        '[{"id":102}]',
        0,
        1
    );
    
INSERT INTO
        obligation_frequency (
            Name,
            Id,
            Advance_by_Months,
            Is_Active
            )
Values (
    'Two Years',
    (SELECT MAX(ID) +1 FROM obligation_frequency),
    3,
    1
    );

INSERT INTO 
    OBLIGATION_REMINDER_FREQUENCY (
        REMINDER_ID,
        DAYS_BACK,
        ID,
        OBLIGATION_FREQUENCY_ID,
        TO_LIST,
        RETURN_RECEIPT,
        TO_BE_CLUBBED
    ) 
VALUES (
        (SELECT REMINDER_ID FROM OBLIGATION_REMINDER WHERE REMINDER_NAME = 'Reminder 1'),
        5,
        (SELECT MAX(ID) +1 FROM OBLIGATION_REMINDER_FREQUENCY),
        (SELECT ID FROM OBLIGATION_FREQUENCY WHERE NAME = 'Two Years'),
        '[{"id":101}]',
        0,
        1
    );
    
INSERT INTO 
    OBLIGATION_REMINDER_FREQUENCY (
        REMINDER_ID,
        DAYS_BACK,
        ID,
        OBLIGATION_FREQUENCY_ID,
        TO_LIST,
        RETURN_RECEIPT,
        TO_BE_CLUBBED
    ) 
VALUES (
        (SELECT REMINDER_ID FROM OBLIGATION_REMINDER WHERE REMINDER_NAME = 'Escalation'),
        5,
        (SELECT MAX(ID) +1 FROM OBLIGATION_REMINDER_FREQUENCY),
        (SELECT ID FROM OBLIGATION_FREQUENCY WHERE NAME = 'Two Years'),
        '[{"id":102}]',
        0,
        1
    );
Commit;

