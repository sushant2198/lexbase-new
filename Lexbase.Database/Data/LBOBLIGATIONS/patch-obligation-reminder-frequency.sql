
select  ofr.NAME
    from OBLIGATION_REMINDER_FREQUENCY  orf 
    inner join OBLIGATION_FREQUENCY  ofr on ofr.id=orf.obligation_frequency_id 
    where ofr.NAME IN ('Weekly','Fortnightly','Monthly','Daily')
    and orf.REMINDER_ID = (SELECT REMINDER_ID FROM OBLIGATION_REMINDER WHERE REMINDER_NAME = 'Alert-A');
--------------
INSERT INTO 
    OBLIGATION_REMINDER_FREQUENCY (
        REMINDER_ID,
        DAYS_BACK,
        ID,
        OBLIGATION_FREQUENCY_ID,
        TO_LIST,
        RETURN_RECEIPT,
        TO_BE_CLUBBED
    ) 
VALUES (
        (SELECT REMINDER_ID FROM OBLIGATION_REMINDER WHERE REMINDER_NAME = 'Alert-A'),
        30,
        (SELECT MAX(ID) +1 FROM OBLIGATION_REMINDER_FREQUENCY),
        (SELECT ID FROM OBLIGATION_FREQUENCY WHERE NAME = 'Daily'),
        '[{"id":101}]',
        1,
        1
    );

-- run for 'Weekly','Fortnightly','Monthly','Daily'

UPDATE
    OBLIGATION_REMINDER_FREQUENCY 
SET 
    DAYS_BACK = 30
WHERE
    REMINDER_ID = (SELECT REMINDER_ID FROM OBLIGATION_REMINDER WHERE REMINDER_NAME = 'Alert-A')
    AND
    OBLIGATION_FREQUENCY_ID IN (SELECT ID FROM OBLIGATION_FREQUENCY WHERE NAME IN ('Weekly','Fortnightly','Monthly','Daily'));