create or replace PROCEDURE   "USP_JOB_INSERT_UPDATE_COBSUMMARY"
(
    p_AccountID IN Number,
    p_COUNT IN Number,
    p_REPORTGROUP IN VARCHAR2,
    p_TYPE IN VARCHAR2,
    p_PERIOD IN VARCHAR2,
    p_OWNER IN Number
)
AS  COBSummry_ID Nvarchar2(50) DEFAULT NULL; LIST_CURSOR SYS_REFCURSOR; l_cobId Number;l_hashvalue NVARCHAR2(4000);COBSummry_ID_1 Nvarchar2(50) DEFAULT NULL;l_FINANCIALVALUE NUMBER;l_Check Number;l_COB_Check Number;
BEGIN

    IF(p_TYPE='OpenWithPenalties' OR p_TYPE='OverdueWithPenalties') Then
        l_FINANCIALVALUE:=1;
    ELSE
        l_FINANCIALVALUE:=0;
    END IF;
    
    SELECT nvl((Select 1 from COBSummary where Account_Id=p_AccountID And Report_Group=p_REPORTGROUP And Type=p_TYPE And Period=p_PERIOD And Status='Active') , 0) INTO l_COB_Check FROM dual;
    
    IF(l_COB_Check>0) Then
        Select ID INTO COBSummry_ID from COBSummary where Account_Id=p_AccountID And Report_Group=p_REPORTGROUP And Type=p_TYPE And Period=p_PERIOD And Status='Active';
    END IF;
    
    IF (COBSummry_ID IS NULL) Then
        BEGIN
           COBSummry_ID_1:=sys_guid();
            Insert Into COBSummary (Id,Account_Id,Count,Report_Group,Type,Period,Owner_Id,Status)
            Values (COBSummry_ID_1,p_AccountID,p_COUNT,p_REPORTGROUP,p_TYPE,p_PERIOD,p_OWNER,'Active');
            
            IF(p_REPORTGROUP='CSCHeatMap' OR p_REPORTGROUP='CustomerHeatMap') Then
                Begin
                    IF(p_PERIOD='DueFivedays' OR p_PERIOD='Total' OR p_PERIOD='OldTotal') Then
                        BEGIN
                            IF (p_PERIOD='OldTotal') Then
                                    USP_JOBS_GET_HEAT_MAP_REPORT_OLD_TOTAL (p_AccountID,p_REPORTGROUP,p_TYPE,l_FINANCIALVALUE,LIST_CURSOR);
                            ELSE
                                USP_JOBS_GET_HEAT_MAP_REPORT (p_AccountID,p_REPORTGROUP,p_TYPE,p_PERIOD,l_FINANCIALVALUE,LIST_CURSOR);
                            END IF;
                        END;
                    ELSE 
                        USP_JOBS_GET_HEAT_MAP_REPORT_HIS (p_AccountID,p_REPORTGROUP,p_TYPE,l_FINANCIALVALUE,p_PERIOD,LIST_CURSOR);
                    END IF;
                End;
            End IF;
            
            IF(p_REPORTGROUP='OwnerOpenWithPenalties' OR p_REPORTGROUP='OwnerOpenWithoutPenalties' OR  p_REPORTGROUP='OwnerOverdueWithPenalties' OR p_REPORTGROUP='OwnerOverdueWithoutPenalties') Then
                BEGIN
                    IF(p_PERIOD='DueFivedays' OR p_PERIOD='Total' OR p_PERIOD='OldTotal') Then
                        BEGIN
                            IF (p_PERIOD='OldTotal') Then
                                USP_JOBS_GET_HEAT_MAP_OWNER_REPORT_OLD_TOTAL(p_AccountID,p_TYPE,l_FINANCIALVALUE,p_OWNER,LIST_CURSOR);
                            ELSE
                                USP_JOBS_GET_HEAT_MAP_OWNER_REPORT (p_AccountID,p_TYPE,p_PERIOD,l_FINANCIALVALUE,p_OWNER,LIST_CURSOR);
                            END IF;
                        END;
                    Else
                        USP_JOBS_GET_HEAT_MAP_OWNER_REPORT_HIS (p_AccountID,p_TYPE,l_FINANCIALVALUE,p_OWNER,p_PERIOD,LIST_CURSOR);
                    END IF;
                END;
            END IF;
            
            
            Loop
                FETCH LIST_CURSOR INTO l_cobId;
                EXIT WHEN LIST_CURSOR%NOTFOUND;
                IF(l_hashvalue IS Null) Then
                    l_hashvalue:=l_cobId;
                ELSE
                    l_hashvalue:=l_hashvalue || ',' || l_cobId;
                END IF;
                Insert Into cobsummary_cob_map (COB_Summary_Id,Child_Obligation_Id)
                Values(COBSummry_ID_1,l_cobId);
            END LOOP;
            CLOSE LIST_CURSOR;
            Update COBSummary Set HashValue=l_hashvalue where Id=COBSummry_ID_1;
            Commit;
        END;
     END IF;
     
     IF (COBSummry_ID Is NOT NULL) THEN
        BEGIN
            IF(p_REPORTGROUP='CSCHeatMap' OR p_REPORTGROUP='CustomerHeatMap') Then
                Begin
                    IF(p_PERIOD='DueFivedays' OR p_PERIOD='Total' OR p_PERIOD='OldTotal') Then
                        BEGIN
                            IF (p_PERIOD='OldTotal') Then
                                    USP_JOBS_GET_HEAT_MAP_REPORT_OLD_TOTAL (p_AccountID,p_REPORTGROUP,p_TYPE,l_FINANCIALVALUE,LIST_CURSOR);
                            ELSE
                                USP_JOBS_GET_HEAT_MAP_REPORT (p_AccountID,p_REPORTGROUP,p_TYPE,p_PERIOD,l_FINANCIALVALUE,LIST_CURSOR);
                            END IF;
                        END;
                    ELSE 
                        USP_JOBS_GET_HEAT_MAP_REPORT_HIS (p_AccountID,p_REPORTGROUP,p_TYPE,l_FINANCIALVALUE,p_PERIOD,LIST_CURSOR);
                    END IF;
                End;
            End IF;
            
            IF(p_REPORTGROUP='OwnerOpenWithPenalties' OR p_REPORTGROUP='OwnerOpenWithoutPenalties' OR  p_REPORTGROUP='OwnerOverdueWithPenalties' OR p_REPORTGROUP='OwnerOverdueWithoutPenalties') Then
                BEGIN
                    IF(p_PERIOD='DueFivedays' OR p_PERIOD='Total' OR p_PERIOD='OldTotal') Then
                        BEGIN
                            IF (p_PERIOD='OldTotal') Then
                                USP_JOBS_GET_HEAT_MAP_OWNER_REPORT_OLD_TOTAL(p_AccountID,p_TYPE,l_FINANCIALVALUE,p_OWNER,LIST_CURSOR);
                            ELSE
                                USP_JOBS_GET_HEAT_MAP_OWNER_REPORT (p_AccountID,p_TYPE,p_PERIOD,l_FINANCIALVALUE,p_OWNER,LIST_CURSOR);
                            END IF;
                        END;
                    Else
                        USP_JOBS_GET_HEAT_MAP_OWNER_REPORT_HIS (p_AccountID,p_TYPE,l_FINANCIALVALUE,p_OWNER,p_PERIOD,LIST_CURSOR);
                    END IF;
                END;
            END IF;
            
            Loop
                FETCH LIST_CURSOR INTO l_cobId;
                EXIT WHEN LIST_CURSOR%NOTFOUND;
                IF(l_hashvalue IS Null) Then
                    l_hashvalue:=l_cobId;
                ELSE
                    l_hashvalue:=l_hashvalue || ',' || l_cobId;
                END IF;
            END LOOP;
            CLOSE LIST_CURSOR;
            
            SELECT nvl((select 1 from COBSummary where HashValue = l_hashvalue AND Id=COBSummry_ID) , 0) INTO l_Check FROM dual;
            
            IF (l_Check=0) Then
                BEGIN
                    UPDATE COBSummary Set Status='InActive' Where Id=COBSummry_ID;
                    COBSummry_ID:=sys_guid();
                    Insert Into COBSummary (Id,Account_Id,Count,Report_Group,Type,Period,Owner_Id,Status)
                    Values (COBSummry_ID,p_AccountID,p_COUNT,p_REPORTGROUP,p_TYPE,p_PERIOD,p_OWNER,'Active');
                    
                    IF(p_REPORTGROUP='CSCHeatMap' OR p_REPORTGROUP='CustomerHeatMap') Then
                Begin
                    IF(p_PERIOD='DueFivedays' OR p_PERIOD='Total' OR p_PERIOD='OldTotal') Then
                        BEGIN
                            IF (p_PERIOD='OldTotal') Then
                                    USP_JOBS_GET_HEAT_MAP_REPORT_OLD_TOTAL (p_AccountID,p_REPORTGROUP,p_TYPE,l_FINANCIALVALUE,LIST_CURSOR);
                            ELSE
                                USP_JOBS_GET_HEAT_MAP_REPORT (p_AccountID,p_REPORTGROUP,p_TYPE,p_PERIOD,l_FINANCIALVALUE,LIST_CURSOR);
                            END IF;
                        END;
                    ELSE 
                        USP_JOBS_GET_HEAT_MAP_REPORT_HIS (p_AccountID,p_REPORTGROUP,p_TYPE,l_FINANCIALVALUE,p_PERIOD,LIST_CURSOR);
                    END IF;
                End;
            End IF;
            
            IF(p_REPORTGROUP='OwnerOpenWithPenalties' OR p_REPORTGROUP='OwnerOpenWithoutPenalties' OR  p_REPORTGROUP='OwnerOverdueWithPenalties' OR p_REPORTGROUP='OwnerOverdueWithoutPenalties') Then
                BEGIN
                    IF(p_PERIOD='DueFivedays' OR p_PERIOD='Total' OR p_PERIOD='OldTotal') Then
                        BEGIN
                            IF (p_PERIOD='OldTotal') Then
                                USP_JOBS_GET_HEAT_MAP_OWNER_REPORT_OLD_TOTAL(p_AccountID,p_TYPE,l_FINANCIALVALUE,p_OWNER,LIST_CURSOR);
                            ELSE
                                USP_JOBS_GET_HEAT_MAP_OWNER_REPORT (p_AccountID,p_TYPE,p_PERIOD,l_FINANCIALVALUE,p_OWNER,LIST_CURSOR);
                            END IF;
                        END;
                    Else
                        USP_JOBS_GET_HEAT_MAP_OWNER_REPORT_HIS (p_AccountID,p_TYPE,l_FINANCIALVALUE,p_OWNER,p_PERIOD,LIST_CURSOR);
                    END IF;
                END;
            END IF;
                    
                    
                    Loop
                        FETCH LIST_CURSOR INTO l_cobId;
                        EXIT WHEN LIST_CURSOR%NOTFOUND;
                        Insert Into cobsummary_cob_map (COB_Summary_Id,Child_Obligation_Id)
                        Values(COBSummry_ID,l_cobId);
                    END LOOP;
                    CLOSE LIST_CURSOR;
                    Update COBSummary Set HashValue=l_hashvalue where Id=COBSummry_ID;
                END;
            Commit;
            END IF;
        END;
     END IF;
END;