set define off;

CREATE OR REPLACE PROCEDURE "LBOBLIGATIONS"."USP_JOBS_CLEAN_CHILD_OBLIGATIONS_BY_OBLIGATIONID" 
( 
    OBLIGATIONID  IN NUMBER,

    DATERESULT OUT TIMESTAMP
)
AS
    OBLIGATIONDATE TIMESTAMP;
    OBSOLETECOUNT NUMBER;
    VALIDCOUNT NUMBER;
BEGIN
    SELECT
        count(CO.ID) INTO OBSOLETECOUNT
    FROM
        CHILD_OBLIGATION CO
        LEFT OUTER JOIN OBLIGATION_TRIGGER_LOG LG  ON CO.ID = LG.CHILD_OBLIGATION_ID
    WHERE 
        LG.SENT_DATE IS NULL AND 
        CO.STATUS in ('alerted', 'pending') AND
        CO.OBLIGATION_ID = OBLIGATIONID;

    IF (OBSOLETECOUNT > 0 ) THEN
        SELECT 
            OBLIGATION_DATE INTO OBLIGATIONDATE 
        FROM (
            SELECT
                CO.OBLIGATION_DATE 
            FROM
                CHILD_OBLIGATION CO
                LEFT OUTER JOIN OBLIGATION_TRIGGER_LOG LG  ON CO.ID = LG.CHILD_OBLIGATION_ID
            WHERE 
                LG.SENT_DATE IS NULL AND 
                CO.STATUS in ('alerted', 'pending') AND
                CO.OBLIGATION_ID = OBLIGATIONID
            ORDER BY
                CO.OBLIGATION_DATE ASC
        )
        WHERE 
            ROWNUM <= 1;

        DELETE FROM OBLIGATION_TRIGGER_LOG
        WHERE 
            CHILD_OBLIGATION_ID IN ( 
                SELECT  ID 
                FROM CHILD_OBLIGATION 
                WHERE 
                    OBLIGATION_DATE >= OBLIGATIONDATE AND
                    OBLIGATION_ID = OBLIGATIONID
            );

        DELETE FROM CHILD_OBLIGATION 
        WHERE 
            OBLIGATION_DATE >= OBLIGATIONDATE AND
            OBLIGATION_ID = OBLIGATIONID;
        COMMIT;
    END IF;

    SELECT
        count(CO.ID) INTO VALIDCOUNT
    FROM
        CHILD_OBLIGATION CO
    WHERE 
        CO.OBLIGATION_ID = OBLIGATIONID;

    IF (VALIDCOUNT > 0 ) THEN
        SELECT 
            OBLIGATION_DATE INTO DATERESULT 
        FROM (
            SELECT 
                OBLIGATION_DATE
            FROM
                CHILD_OBLIGATION
            WHERE 
                OBLIGATION_ID =  OBLIGATIONID 
            ORDER BY
                OBLIGATION_DATE DESC
        )
        WHERE 
            ROWNUM <= 1;
    ELSE 
        SELECT 
            START_DATE INTO DATERESULT
        FROM
            OBLIGATION
        WHERE 
            ID =  OBLIGATIONID; 
    END IF;
END;
