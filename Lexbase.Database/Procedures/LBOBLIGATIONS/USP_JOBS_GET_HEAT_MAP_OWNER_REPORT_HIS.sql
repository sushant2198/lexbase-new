create or replace PROCEDURE                 "USP_JOBS_GET_HEAT_MAP_OWNER_REPORT_HIS"
(
    ACCOUNTID IN NUMBER,
    REPORTTYPE IN NVARCHAR2,
    FINANCIALVALUE IN NUMBER,
    p_OWNER IN NUMBER,
    p_Period IN NVARCHAR2,
    LIST_CURSOR OUT SYS_REFCURSOR
) IS 
query_str VARCHAR2(5000);
BEGIN

    query_str:='SELECT
        COB.Id 
    FROM 
        CHILD_OBLIGATION COB 
        INNER JOIN OBLIGATION MOB ON COB.OBLIGATION_ID = MOB.ID ';

    IF REPORTTYPE Not In('TotalOpenObligations','CompletedByDueDate','CompletedAfterDueDate','TotalCompleted') THEN
    BEGIN
        query_str:=query_str || 'AND MOB.FINANCIAL ='|| FINANCIALVALUE || '';
    END;
    END IF;

    query_str:=query_str || ' AND MOB.ACCOUNT_ID='|| ACCOUNTID ||'   
    INNER JOIN (
        SELECT 
            COB.ID, 
            TO_CHAR(OBLIGATION_DATE,''MMYYYY'') MONTH_1 
        FROM 
            CHILD_OBLIGATION COB 
        
    ) COB_MNTH ON COB_MNTH.ID = COB.ID
    WHERE ';

    IF REPORTTYPE='OpenWithPenalties' OR REPORTTYPE='OpenWithoutPenalties' THEN
    BEGIN
    query_str:=query_str || 'COB.STATUS NOT IN(''completed'',''overdue'')';
    END;
    END IF;
    IF REPORTTYPE='OverdueWithPenalties' OR REPORTTYPE='OverdueWithoutPenalties' THEN
    BEGIN
    query_str:=query_str || 'COB.STATUS NOT IN(''overdue'')';
    END;
    End IF;

    query_str:=query_str || ' AND 
        COB.ACTION_TYPE=''M'' AND 
        COB.IS_ACTIVE = 1 AND 
        COB.STATE_ID = 1  AND 
        OBLIGATION_DATE BETWEEN ADD_MONTHS(SYSDATE ,-14) AND SYSDATE AND MOB.OWNER='|| p_OWNER ||' AND COB_MNTH.MONTH_1= '|| p_PERIOD ||' ';
    
    dbms_output.put_line(query_str);
    OPEN LIST_CURSOR FOR query_str;
END;