set define off;

CREATE OR REPLACE PROCEDURE "LBOBLIGATIONS"."USP_JOBS_GET_CHILD_OBLIGATIONS" 
( 
    cobId IN NUMBER DEFAULT NULL,
    cobStatus IN VARCHAR2 DEFAULT NULL,
    cobActionType IN VARCHAR2 DEFAULT NULL,
    cobDate IN TIMESTAMP DEFAULT NULL,
    obId IN NUMBER DEFAULT NULL,
    accountId IN NUMBER DEFAULT NULL,
    ACCOUNTIDS IN VARCHAR2 DEFAULT NULL,
    reportPeriod IN NUMBER DEFAULT NULL,
    LIST_CURSOR OUT SYS_REFCURSOR
)
AS
BEGIN   
    OPEN LIST_CURSOR FOR
        SELECT 
            COB.ID,
            COB.STATUS,
            COB.STATUS_HISTORY,
            COB.OBLIGATION_DATE,
            COB.IS_ACTIVE,

            OB.ID AS PARENT_ID,
            OB.NAME AS PARENT_NAME,

            OB.ACTION_TYPE AS PARENT_ACTION_TYPE,
            OB.IS_TRIGGER AS PARENT_IS_TRIGGER,
            OB.FINANCIAL AS PARENT_IS_FINANCIAL,
            OB.NAME AS PARENT_STATUS,

            OB.GROUP_FOR AS PARENT_GROUP_FOR,
            OB.GROUP_FOR_ID AS PARENT_GROUP_FOR_ID,
            OB.ACCOUNT_ID AS PARENT_ACCOUNT_ID,

            OB.START_DATE AS PARENT_START_DATE,
            OB.COMPLETION_DATE AS PARENT_COMPLETION_DATE,

            OB.UPTO_DATE AS PARENT_UPTO_DATE,
            OB.NEXT_CREATION_DATE AS PARENT_NEXT_CREATION_DATE,

            OB.RULES_UPDATED AS PARENT_RULES_UPDATED,

            OB.SHARED_WITH AS PARENT_SHARED_WITH_ID,
            OB.OWNER AS PARENT_OWNER_ID,
            OB.OWNER_MANAGER AS PARENT_MANAGER_ID,

            OFC.ID AS PARENT_FREQUENCY_ID,
            OFC.NAME AS PARENT_FREQUENCY_NAME,
            OFC.ADVANCE_BY_MONTHS AS PARENT_FREQUENCY_ADVANCE_BY_MONTHS,

            OT.ID AS PARENT_TYPE_ID,
            OT.NAME AS PARENT_TYPE_NAME,

            SOT.ID AS PARENT_SUB_TYPE_ID,
            SOT.NAME AS PARENT_SUB_TYPE_NAME,

            ST.ID AS PARENT_STATE_ID,
            ST.NAME AS PARENT_STATE_NAME,

            (
                select OP.PHASE_NAME 
                from 
                    OBLIGATION_PHASE OP,
                    OBLIGATION_PHASE_MAPPING OPM
                where 
                    OPM.OBLIGATION_ID = OB.ID AND
                    OPM.PHASE_ID = OP.PHASE_ID AND
                    ROWNUM <= 1
            ) AS PARENT_PHASE_NAME,
            
            (
                select RE.RESPONSIBILITY_NAME 
                from 
                    OBLIGATION_RESPONSIBILITY_NEW RE,
                    OBLIGATION_RESPONSIBILITY ORM
                where 
                    ORM.OBLIGATION_ID = OB.ID AND
                    ORM.RESPONSIBILITY_ID = RE.RESPONSIBILITY_ID AND
                    ROWNUM <= 1
            ) AS PARENT_RESPONSIBILITY_NAME
        FROM 
            CHILD_OBLIGATION COB,
            OBLIGATION OB 
            LEFT OUTER JOIN OBLIGATION_FREQUENCY OFC ON (OFC.ID = OB.FREQUENCY)
            LEFT OUTER JOIN OBLIGATION_TYPE OT ON (OT.ID = OB.OBLIGATION_TYPE)
            LEFT OUTER JOIN OBLIGATION_SUB_TYPE SOT ON (SOT.ID = OB.SUB_TYPE)
            LEFT OUTER JOIN STATE ST ON (ST.ID = OB.STATE_ID)
        WHERE
            OB.ID = COB.OBLIGATION_ID AND

            COB.IS_ACTIVE = 1 AND 
            COB.GL_ACTIVE = 1 AND

            (accountId is null OR OB.ACCOUNT_ID = accountId) AND 
            (ACCOUNTIDS is null OR OB.ACCOUNT_ID IN (SELECT COLUMN_VALUE FROM TABLE(COMMA_TO_TABLE(ACCOUNTIDS)))) AND
            (obId is null OR OB.ID = obId) AND 
            (cobId is null OR COB.ID = cobId) AND 
            (cobStatus is null OR COB.STATUS = cobStatus) AND 
            (cobActionType is null OR COB.ACTION_TYPE = cobActionType) AND 
            (cobDate is null OR TRUNC(COB.OBLIGATION_DATE) = TRUNC(cobDate)) AND 
            (   
                reportPeriod is null OR (
                    (OB.APPROVAL_REQUIRED = 1 AND TRUNC(COB.SUBMISSION_DATE) <= TRUNC(SYSDATE) AND TRUNC(COB.SUBMISSION_DATE) >=  TRUNC(SYSDATE-reportPeriod)) 
                    OR 
                    (OB.APPROVAL_REQUIRED > 1 AND TRUNC(COB.CUSTOMER_APPROVAL_DATE) <= TRUNC(SYSDATE) AND TRUNC(COB.CUSTOMER_APPROVAL_DATE) >= TRUNC(SYSDATE-reportPeriod))
                ) 
            );

END;
