create or replace NONEDITIONABLE PROCEDURE "USP_JOBS_GET_FULL_HEAT_MAP"
(
    ACCOUNTID IN NUMBER,
    LIST_CURSOR OUT SYS_REFCURSOR
) 
AS
BEGIN
    
    OPEN LIST_CURSOR FOR
        SELECT 
            ID,
            REPORT_GROUP,
            TYPE,
            PERIOD,
            COUNT,

            OWNER_ID
        FROM 
            COBSUMMARY 
        WHERE
            ACCOUNT_ID = ACCOUNTID AND
            STATUS = 'Active'
            
END;