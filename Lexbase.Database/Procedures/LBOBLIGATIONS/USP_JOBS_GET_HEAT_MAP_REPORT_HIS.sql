create or replace PROCEDURE                 "USP_JOBS_GET_HEAT_MAP_REPORT_HIS"
(
    ACCOUNTID IN NUMBER,
    LISTTYPE IN NVARCHAR2,
    REPORTTYPE IN NVARCHAR2,
    FINANCIALVALUE IN NUMBER,
    p_PERIOD IN NVARCHAR2,
    LIST_CURSOR OUT SYS_REFCURSOR
) IS 
query_str VARCHAR2(5000);
BEGIN

    query_str:='SELECT
        COB.ID
    FROM 
        CHILD_OBLIGATION COB 
        INNER JOIN OBLIGATION MOB ON COB.OBLIGATION_ID = MOB.ID ';

    IF REPORTTYPE NOT IN('TotalOpenObligations','CompletedByDueDate','CompletedAfterDueDate','TotalCompleted') THEN
    BEGIN
        query_str:=query_str || 'AND MOB.FINANCIAL ='|| FINANCIALVALUE || '';
    END;
    END IF;

    query_str:=query_str || 'AND MOB.ACCOUNT_ID='|| ACCOUNTID ||'   
    INNER JOIN (
        SELECT DISTINCT(OBLIGATION_ID) 
        FROM OBLIGATION_RESPONSIBILITY WHERE ';

    IF LISTTYPE='CSCHeatMap' THEN
    BEGIN
        query_str:=query_str || 'responsibility_id =1 or responsibility_id =3) ';
    END;
    END IF;

    IF LISTTYPE='CustomerHeatMap' THEN
    BEGIN
        query_str:=query_str || 'responsibility_id =2) ';
    END;
    END IF;

    query_str:=query_str || 'MOB_R ON MOB.ID = MOB_R.OBLIGATION_ID
    INNER JOIN (
        SELECT COB.ID,
        TO_CHAR(OBLIGATION_DATE,''MMYYYY'') MONTH_1 
        FROM 
        CHILD_OBLIGATION COB 
        
    ) COB_MNTH ON COB_MNTH.ID = COB.ID
    WHERE ';

    IF REPORTTYPE='OpenWithPenalties' OR REPORTTYPE='OpenWithoutPenalties' THEN
    BEGIN
        query_str:=query_str || 'COB.STATUS NOT IN(''completed'',''overdue'')';
    END;
    END IF;

    IF REPORTTYPE='OverdueWithPenalties' OR REPORTTYPE='OverdueWithoutPenalties' THEN
    BEGIN
        query_str:=query_str || 'COB.STATUS NOT IN(''overdue'')';
    END;
    END IF;

    IF REPORTTYPE='TotalOpenObligations' THEN
    BEGIN
        query_str:=query_str || 'COB.STATUS != ''completed'' ';
    END;
    END IF;
    IF REPORTTYPE='CompletedByDueDate' OR REPORTTYPE='CompletedAfterDueDate' OR REPORTTYPE='TotalCompleted' THEN
    BEGIN
        query_str:=query_str || 'COB.STATUS = ''completed'' ';
    END;
    END IF;

    query_str:=query_str || ' AND 
    COB.ACTION_TYPE=''M'' AND 
    COB.IS_ACTIVE = 1  AND 
    COB.STATE_ID = 1';

    IF REPORTTYPE='CompletedByDueDate' THEN
    BEGIN
        query_str:= query_str || 'AND trunc(COB.SUBMISSION_DATE) <= trunc(COB.OBLIGATION_DATE)';
    END;
    END IF;

    IF REPORTTYPE='CompletedAfterDueDate' THEN
    BEGIN
        query_str:= query_str || 'AND trunc(COB.SUBMISSION_DATE)> trunc(COB.OBLIGATION_DATE)';
    END;
    END IF;

    query_str:= query_str || ' AND 
        OBLIGATION_DATE BETWEEN ADD_MONTHS(SYSDATE ,-14) and SYSDATE AND COB_MNTH.MONTH_1= '''|| p_PERIOD ||''' ';
    OPEN LIST_CURSOR FOR query_str;
END;