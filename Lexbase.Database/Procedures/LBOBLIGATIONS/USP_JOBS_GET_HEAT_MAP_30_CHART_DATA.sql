set define off;

CREATE OR REPLACE PROCEDURE "LBOBLIGATIONS"."USP_JOBS_GET_HEAT_MAP_30_CHART_DATA"
(
    ACCOUNTID IN NUMBER,
    FRQTYPE IN NUMBER,
    LIST_CURSOR OUT SYS_REFCURSOR
) IS 
query_str VARCHAR2(5000);
BEGIN
    query_str:='SELECT 
      MOB.ACCOUNT_ID AS ACCNT, 
      COB.ID as ID, 
      '|| FRQTYPE ||' AS FRQ_TYPE 
    FROM 
      CHILD_OBLIGATION COB 
      INNER JOIN OBLIGATION MOB ON COB.OBLIGATION_ID = MOB.ID 
    WHERE 
      COB.STATUS != ''completed'' AND 
      COB.IS_ACTIVE = 1 AND 
      COB.STATE_ID = 1 AND 
      COB.ACTION_TYPE=''M'' AND 
      trunc(COB.OBLIGATION_DATE) >= trunc(SYSDATE) AND 
      trunc(COB.OBLIGATION_DATE) < trunc(SYSDATE + 30) AND
      MOB.ACCOUNT_ID='|| ACCOUNTID || '';

    IF FRQTYPE=1 THEN
    BEGIN
      query_str:=query_str ||'  AND MOB.FREQUENCY = 363';
    END;
    END IF;

    IF FRQTYPE=2 THEN
    BEGIN
      query_str:=query_str ||'  AND MOB.FREQUENCY = 365';
    END;
    END IF;

    IF FRQTYPE=3 THEN
    BEGIN
      query_str:=query_str ||'  AND MOB.FREQUENCY IN(366,367,368,373)';
    END;
    END IF;

    OPEN LIST_CURSOR FOR query_str;
END;