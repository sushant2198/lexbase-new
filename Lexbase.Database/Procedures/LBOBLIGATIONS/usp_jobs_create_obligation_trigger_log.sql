
CREATE OR REPLACE PROCEDURE "LBOBLIGATIONS"."USP_JOBS_CREATE_OBLIGATION_TRIGGER_LOG" 
( 
    CHILDOBLIGATIONID  IN NUMBER, 
    REMINDERDATE IN TIMESTAMP,
    REMINDERID IN NUMBER,
    FREQUENCYID IN NUMBER,
    ISACTIVE IN NUMBER, 
--    TRIGGERID IN NUMBER, 
--    RESPONSE_REQUIRED IN VARCHAR2,
--    STATUSHISTORY IN CLOB, 
--    RETURN_RECEIPT IN VARCHAR2,

    CREATEDID OUT NUMBER
)
AS
BEGIN   
        INSERT INTO "OBLIGATION_TRIGGER_LOG"
		(
            "ID",
            "CHILD_OBLIGATION_ID",
--            "TRIGGER_ID",
            "REMINDER_ID",
		    "WHEN",
            "IS_ACTIVE",
--            "RESPONSE_REQUIRED",
--            "RETURN_RECEIPT", 
            "FREQUENCY",
            "DATE_CREATED"
        )
		VALUES
		(
            OB_TRIGGER_LOG_SEQ.NEXTVAL,
            CHILDOBLIGATIONID,
--            TRIGGERID,
            REMINDERID,
            REMINDERDATE,
            ISACTIVE,
--            RESPONSEREQUIRED,
--            RETURNRECEIPT,
            FREQUENCYID,
            SYSDATE
        )
        RETURNING 
            ID INTO CREATEDID;
        COMMIT;
END;




