set define off;

CREATE OR REPLACE PROCEDURE "LBOBLIGATIONS"."USP_JOBS_GET_CHILD_OBLIGATIONS_OVERDUE" 
( 
    ACCOUNTIDS IN VARCHAR2,
    DUEDATE IN TIMESTAMP,
    LIST_CURSOR OUT SYS_REFCURSOR
)
AS
BEGIN   
    OPEN LIST_CURSOR FOR
        SELECT 
            COB.ID,
            COB.STATUS,
            COB.OBLIGATION_DATE,
            COB.STATUS_HISTORY
        FROM 
            CHILD_OBLIGATION COB,
            OBLIGATION OB
        WHERE
            OB.ID = COB.OBLIGATION_ID AND
            COB.ACTION_TYPE='M' AND
            (
                (trunc(COB.OBLIGATION_DATE) < trunc(DUEDATE) AND  COB.STATUS IN ('pending','escalated','inAcceptance')) OR 
                (trunc(COB.OBLIGATION_DATE) >= trunc(DUEDATE) AND  COB.STATUS IN ('overdue')) 
            ) AND 
            COB.IS_ACTIVE = 1 AND 
            COB.GL_ACTIVE = 1 AND
            OB.ACCOUNT_ID IN (SELECT COLUMN_VALUE FROM TABLE(COMMA_TO_TABLE(ACCOUNTIDS)));
END;