set define off;

CREATE OR REPLACE PROCEDURE "LBOBLIGATIONS"."USP_JOBS_GET_OBLIGATIONS_BY_ACCOUNT_ID" 
( 
    ACCOUNTID  IN NUMBER,
    LIST_CURSOR OUT SYS_REFCURSOR
)
AS
BEGIN   

    OPEN LIST_CURSOR FOR
        SELECT                      
            DISTINCT (OB.ID),
            OB.NAME,

            OB.ACTION_TYPE,
            OB.IS_TRIGGER,
            OB.FINANCIAL,

            OB.GROUP_FOR,
            OB.GROUP_FOR_ID,
            OB.ACCOUNT_ID,

            OB.START_DATE,
            OB.COMPLETION_DATE,

            OB.UPTO_DATE,
            OB.NEXT_CREATION_DATE,

            OB.RULES_UPDATED,

            OB.SHARED_WITH AS SHARED_WITH_ID,
            OB.OWNER AS OWNER_ID,
            OB.OWNER_MANAGER AS MANAGER_ID,

            OFC.ID AS FREQUENCY_ID,
            OFC.NAME AS FREQUENCY_NAME,
            OFC.ADVANCE_BY_MONTHS AS FREQUENCY_ADVANCE_BY_MONTHS,

            OT.ID AS TYPE_ID,
            OT.NAME AS TYPE_NAME,

            SOT.ID AS SUB_TYPE_ID,
            SOT.NAME AS SUB_TYPE_NAME,

            ST.ID AS STATE_ID,
            ST.NAME AS STATE_NAME,

            (
                select OP.PHASE_NAME 
                from 
                    OBLIGATION_PHASE OP,
                    OBLIGATION_PHASE_MAPPING OPM
                where 
                    OPM.OBLIGATION_ID = OB.ID AND
                    OPM.PHASE_ID = OP.PHASE_ID AND
                    ROWNUM <= 1
            ) AS PHASE_NAME,

            (
                select RE.RESPONSIBILITY_NAME
                from 
                    OBLIGATION_RESPONSIBILITY_NEW RE,
                    OBLIGATION_RESPONSIBILITY ORM
                where 
                    ORM.OBLIGATION_ID = OB.ID AND
                    ORM.RESPONSIBILITY_ID = RE.RESPONSIBILITY_ID AND
                    ROWNUM <= 1
            ) AS RESPONSIBILITY_NAME
        FROM
            OBLIGATION OB
            LEFT OUTER JOIN OBLIGATION_FREQUENCY OFC ON (OFC.ID = OB.FREQUENCY)
            LEFT OUTER JOIN OBLIGATION_TYPE OT ON (OT.ID = OB.OBLIGATION_TYPE)
            LEFT OUTER JOIN OBLIGATION_SUB_TYPE SOT ON (SOT.ID = OB.SUB_TYPE)
            LEFT OUTER JOIN STATE ST ON (ST.ID = OB.STATE_ID)
        WHERE 
            OB.IS_ACTIVE = 0 AND
            OB.ACCOUNT_ID = ACCOUNTID 
        ORDER BY OB.OWNER;

END;




