set define off;

CREATE OR REPLACE PROCEDURE "LBOBLIGATIONS"."USP_JOBS_GET_OBLIGATION_TYPES" 
( 
    LIST_CURSOR OUT SYS_REFCURSOR
)
AS
BEGIN   

    OPEN LIST_CURSOR FOR
        SELECT                      
            ID,
            NAME
        FROM
            OBLIGATION_TYPE OT
        WHERE 
            OT.IS_ACTIVE = 1;
END;



