set define off;

CREATE OR REPLACE PROCEDURE "LBUSER"."USP_JOBS_GET_USER_BY_ID" 
( 
    USER_ID IN NUMBER, 
    LIST_CURSOR OUT SYS_REFCURSOR
)
AS
BEGIN   

    OPEN LIST_CURSOR FOR
        SELECT
            U.ID,
            U.FIRST_NAME,
            U.MIDDLE_NAME,
            U.LAST_NAME,
            U.SENSITIVE_ACCESS,
            U.EMAIL,
            U.IS_ACTIVE
        FROM
            USERS U
        WHERE 
            U.ID = USER_ID;
END;




