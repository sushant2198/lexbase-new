set define off;

CREATE OR REPLACE PROCEDURE "LBUSER"."USP_JOBS_GET_USERS_BY_IDS" 
( 
    IDS IN VARCHAR2, 
    LIST_CURSOR OUT SYS_REFCURSOR
)
AS
BEGIN   

    OPEN LIST_CURSOR FOR
        SELECT
            U.ID,
            U.FIRST_NAME,
            U.MIDDLE_NAME,
            U.LAST_NAME,
            U.SENSITIVE_ACCESS,
            U.EMAIL,
            U.IS_ACTIVE
        FROM
            USERS U
        WHERE 
            U.ID IN (SELECT COLUMN_VALUE FROM TABLE(COMMA_TO_TABLE(IDS)));
END;




