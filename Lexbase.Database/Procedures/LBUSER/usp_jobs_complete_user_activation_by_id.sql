set define off;

CREATE OR REPLACE PROCEDURE "LBUSER"."USP_JOBS_COMPLETE_USER_ACTIVATION_BY_ID" 
( 
    USER_ID  IN NUMBER
)
AS
BEGIN   
    UPDATE 
        USERS 
    SET 
        IN_ACTIVATION = 0,
        IS_ACTIVE = 1,
        DATE_MODIFIED = SYSDATE
	WHERE ID = USER_ID;
    COMMIT;
END;





