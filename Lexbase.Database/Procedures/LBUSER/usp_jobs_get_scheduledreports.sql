set define off;

CREATE OR REPLACE PROCEDURE "LBUSER"."USP_JOBS_GET_SCHEDULEDREPORTS" 
( 
    LIST_CURSOR OUT SYS_REFCURSOR
)
AS
BEGIN   

    OPEN LIST_CURSOR FOR
        SELECT
            SR.ID,
            SR.REPORTNAME AS JOBNAME,
            SR.REPORTPARAM AS JOBPARAM,
            SR.ERROR,
            SR.STARTED_DATE,
            SR.COMPLETED_DATE
        FROM
            SCHEDULEDREPORT SR
        WHERE 
            SR.STARTED_DATE IS NULL;
END;