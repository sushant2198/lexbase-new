set define off;

CREATE OR REPLACE PROCEDURE "LBAUDITLOGS"."USP_JOBS_GET_AUDIT_LOGS" 
(
noOfDays NUMERIC,
LIST_CURSOR OUT SYS_REFCURSOR
)
AS
BEGIN   
OPEN LIST_CURSOR FOR
    SELECT 
        AL.REQUESTED_BY as USER_ID,
       case when AL.AUDIT_LOG_FOR=1 then 'Account' 
        when AL.AUDIT_LOG_FOR=2 then 'MSA'
          when AL.AUDIT_LOG_FOR=5 then 'SA'
           when AL.AUDIT_LOG_FOR=6 then 'Obligation' end
        as GROUP_FOR,
        AL.AUDIT_LOG_FOR_ID as Group_FOR_ID,
        ALE.DISPLAY_NAME as EVENT_NAME,
        ALE.ID AS EVENT_ID,
        ALE.EVENT_ID AS EVENT_CODE,
        AL.DATE_CREATED
    FROM 
    AUDIT_LOG AL 
    INNER JOIN
    AUDIT_LOG_EVENTS ALE on AL.EVENT_ID=ALE.ID
    WHERE 
        AL.DATE_CREATED >=sysdate-noOfDays 
    ORDER BY 
        AL.DATE_CREATED ASC,
        ALE.DISPLAY_NAME DESC;
END;
