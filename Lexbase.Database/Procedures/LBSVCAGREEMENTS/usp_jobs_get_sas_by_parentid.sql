set define off;

CREATE OR REPLACE PROCEDURE "LBSVCAGREEMENTS"."USP_JOBS_GET_SAS_BY_PARENTID" 
( 
    PARENTID  IN NUMBER, 
    PARENTTYPE IN NUMBER,
    LIST_CURSOR OUT SYS_REFCURSOR
)
AS
BEGIN   
    IF (PARENTTYPE = 1 ) THEN
        OPEN LIST_CURSOR FOR
            SELECT 
                S.ID
            FROM 
                TBL_SA S 
            WHERE 
                S.IS_ACTIVE = 0 AND
                S.ACCOUNT_ID  = PARENTID;
    ELSE 
        OPEN LIST_CURSOR FOR
            SELECT 
                S.ID
            FROM 
                TBL_SA S 
            WHERE 
                S.IS_ACTIVE = 0 AND
                S.PARENT_FOR_ID  = PARENTID AND
                S.PARENT_FOR = PARENTTYPE;  
    END IF;
END;