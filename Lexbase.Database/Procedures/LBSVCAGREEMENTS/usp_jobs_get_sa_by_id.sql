set define off;


CREATE OR REPLACE PROCEDURE "LBSVCAGREEMENTS"."USP_JOBS_GET_SA_BY_ID" 
( 
    SA_ID  IN NUMBER, 
    LIST_CURSOR OUT SYS_REFCURSOR
)
AS
BEGIN   
    OPEN LIST_CURSOR FOR
        SELECT 
            S.ID,
            S.NAME,
            S.DATE_MODIFIED,
            S.PARENT_FOR_ID, 
            S.PARENT_FOR,
            S.DATE_CREATED,
            S.DATAXML,
            S.IS_ACTIVE,
            S.GL_ACTIVE,
            S.ACCESS_EMPTY,
            S.GL_ACCESS_EMPTY,
            S.CM_ALERTED,
            S.PRE_ACTIVATED,
            S.DOCUMENT_NUMBER,
            S.DOCUMENT_TITLE,
            S.IS_SUB_CONTARCT,
            S.LEAD_COUNTRY,
            S.EFFECTIVE_DATE,
            S.EXPIRATION_DATE,
            S.AGREEMENT_NUMBER,
            S.OPTIONAL_SERVICES,
            S.REFERENCE,
            S.NOTIFICATION_REQUIREMENTS,
            S.MFC,
            S.THIRD_PARTY_IPO,
            S.OFFSHORE_SERVICES,
            S.TERMINATION_ASSISTANCE,
            S.IN_PROCESSING, 
            S.IN_RE_INDEXING,
            S.SEQ_NO,
            S.ACCOUNT_ID
        FROM 
            TBL_SA S 
        WHERE 
            S.ID = SA_ID;
END;




