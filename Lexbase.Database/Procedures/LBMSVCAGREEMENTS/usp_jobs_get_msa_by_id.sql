set define off;


CREATE OR REPLACE PROCEDURE "LBMSVCAGREEMENTS"."USP_JOBS_GET_MSA_BY_ID" 
( 
    MSA_ID  IN NUMBER, 
    LIST_CURSOR OUT SYS_REFCURSOR
)
AS
BEGIN   
    OPEN LIST_CURSOR FOR
        SELECT 
            MS.ID,
            MS.ACCOUNT_ID,
            MS.PARENT_FOR_ID, 
            MS.PARENT_FOR,
            MS.NAME,
            MS.STATUS AS MSA_STATUS ,
            MS.DATE_CREATED ,
            MS.DATE_MODIFIED,
            MS.DATAXML,
            MS.IS_ACTIVE,
            MS.GL_ACTIVE,
            MS.ACCESS_EMPTY,
            MS.GL_ACCESS_EMPTY,
            MS.DOCUMENT_NUMBER,
            MS.DOCUMENT_TITLE,
            MS.IS_SUB_CONTARCT,
            MS.LEAD_COUNTRY AS MSA_LEAD_COUNTRY,
            MS.EFFECTIVE_DATE,
            MS.EXPIRATION_DATE,
            MS.AGREEMENT_NUMBER,
            MS.OPTIONAL_SERVICES,
            MS.REFERENCE,
            MS.NOTIFICATION_REQUIREMENTS,
            MS.MFC,
            MS.THIRD_PARTY_IPO,
            MS.OFFSHORE_SERVICES,
            MS.TERMINATION_ASSISTANCE,
            MS.IN_PROCESSING, 
            MS.IN_RE_INDEXING,
            MS.VERTICAL_ID,
            MS.CM_ALERTED,
            MS.PRE_ACTIVATED,
            MS.SEQ_NO
        FROM 
            MSA MS
        WHERE 
            MS.ID = MSA_ID;
END;




