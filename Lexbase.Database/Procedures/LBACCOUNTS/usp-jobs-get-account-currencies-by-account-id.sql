set define off;

CREATE OR REPLACE PROCEDURE "LBACCOUNTS"."USP_JOBS_GET_ACCOUNT_CURRENCIES_BY_ACCOUNT_ID" 
( 
    ACCOUNT_ID  IN NUMBER, 
    LIST_CURSOR OUT SYS_REFCURSOR
)
AS
BEGIN   

    OPEN LIST_CURSOR FOR
    SELECT * 
    FROM 
        ACCOUNT_CURRENCY C 
    WHERE 
        C.ACCOUNT_ID = ACCOUNT_ID;
END;




