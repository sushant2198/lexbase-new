set define off;

CREATE OR REPLACE PROCEDURE "LBACCOUNTS"."USP_JOBS_GET_ACCOUNTS_NEEDS_ACTIVATION" 
( 
    LIST_CURSOR OUT SYS_REFCURSOR
)
AS
BEGIN   

    OPEN LIST_CURSOR FOR
        SELECT 
            Id
        FROM 
            ACCOUNT
        WHERE
            IS_ACTIVE = 0 AND
            IN_PROCESSING = 1 AND
            BRAG_STATUS = 0  AND
            CM_ALERTED = 1  AND
            PRE_ACTIVATED = 1 AND
            IS_ARCHIVE = 0;
END;
