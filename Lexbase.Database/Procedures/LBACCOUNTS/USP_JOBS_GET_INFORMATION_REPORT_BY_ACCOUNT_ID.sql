set define off;

create or replace NONEDITIONABLE PROCEDURE              "USP_JOBS_GET_INFORMATION_REPORT_BY_ACCOUNT_ID" 
( 
    LIST_CURSOR OUT SYS_REFCURSOR
)
AS
BEGIN  

    OPEN LIST_CURSOR FOR
    SELECT 
        AC.ID AS AccountId, 
        AC.NAME AS Name,
        AT.NAME AS AccountType,
        AC.BRAG_STATUS AS Status,
        R.NAME AS RegionName, 
        ac.account_value AS AccountValue,
        ac.currency_type As CurrencyType, 
        SUBSTR(CAST(TO_CHAR(AC.DATE_CREATED,'dd-Mon-yy') as varchar(50)), 1, 11) AS AccountDateCreated,
        SUBSTR(CAST(TO_CHAR(AC.DATE_MODIFIED,'dd-Mon-yy') as varchar(50)), 1, 11) AS AccountDateModified,        
        DECODE (AC.IS_ACTIVE, 1, 'Yes', 0, 'No')as IsActive,
        (SELECT TIMEZONE_VALUE FROM TIMEZONE WHERE ID = AC.TIMEZONE) AS TimeZone,
        AC.IS_SENSITIVE as Sensitive,
        AC."LEGAL_NAME" AS LegelName,
        AC."ACCOUNT_SIZE" As AccountSize,
        ACF.NAME as AccountClassification,
        AC."ALIAS" As Alias,
        AC."ADDRESS" As Address,
        DECODE (AC."IN_TRANSITION", 1, 'Yes', 0, 'No')as InTransition,
        AC."CONTRACT_CURRENCY",SUBSTR(CAST(AC.SUBMISSION_DATE as varchar(50)), 1, 9) AS SubmissionDate,
        (SELECT LEAD_C."COUNTRY_NAME" FROM DELIVERY_COUNTRY LEAD_C WHERE LEAD_C.country_id=AC.LEAD_COUNTRY) AS LeadCountryName,
        SUBSTR(CAST(TO_CHAR(AC.DATE_CONFORMED,'dd-Mon-yy') as varchar(50)), 1, 11) AS AccountDateConfirmed,
        (SELECT LISTAGG(COUNTRY_NAME,',')WITHIN GROUP (ORDER BY COUNTRY_NAME) FROM DELIVERY_COUNTRY  WHERE COUNTRY_ID in (SELECT DC.COUNTRY_ID FROM ACCOUNT_DELIVERY_COUNTRY DC WHERE DC.ACCOUNT_ID=AC.ID)) AS DeliveryCountryName
    FROM
        ACCOUNT AC inner join
        ACCOUNT_CLASSIFICATION ACF on AC.CLASSIFICATION=ACF.ID 
        inner join ACCOUNT_TYPE AT on AT.ID=AC.TYPE 
        inner join REGION R on AC.REGION_ID=R.ID
    WHERE 
        AC.IS_ACTIVE=1
    ORDER BY UPPER(ac.NAME);
END;