create or replace PROCEDURE "USP_JOBS_UPDATE_ACCOUNT_STATUS_INACTIVE" 
(
  ACCOUNTID IN NUMBER 
, BRAGSTATUS IN NUMBER
, STARTDATE IN TimeStamp
, ENDDATE IN TimeStamp
, BRAGRATIO IN NUMBER
, FINANCIALCOBCOUNT IN NUMBER
--, LASTDAYRECALCULATION IN NUMBER
, FREEZINGSTATUS IN NUMBER
, ISINACTIVE NUMBER 
) AS 
l_Count NUMBER;
BEGIN

IF (FREEZINGSTATUS <= 0)
THEN 

    IF (ISINACTIVE > 0) 
        THEN
              SELECT Count(*) INTO l_Count FROM ACCOUNT_STATUS WHERE ACCOUNT_ID = ACCOUNTID
              AND TRUNC(START_DATE) = TRUNC(STARTDATE)  AND TRUNC(END_DATE) = TRUNC(ENDDATE);
                IF(l_Count <= 0) 
                    THEN
                        INSERT INTO 
                                ACCOUNT_STATUS ( ACCOUNT_ID,BRAG_STATUS,START_DATE,END_DATE)
                                         VALUES( ACCOUNTID,BRAGSTATUS,STARTDATE,ENDDATE);
                                COMMIT;
                    ELSE
                        UPDATE ACCOUNT_STATUS SET BRAG_STATUS=BRAGSTATUS,
                                                  START_DATE=STARTDATE,
                                                  END_DATE=ENDDATE
                                              WHERE ACCOUNT_ID=ACCOUNTID and TRUNC(START_DATE) = TRUNC((sysdate),'month') ;
                                COMMIT;
                END IF;
        ELSE

            SELECT Count(*) INTO l_Count FROM ACCOUNT_STATUS WHERE ACCOUNT_ID = ACCOUNTID 
            AND TRUNC(START_DATE) = TRUNC(STARTDATE)  AND TRUNC(END_DATE) = TRUNC(ENDDATE);
                IF(l_Count <= 0) 
                    THEN
                        INSERT INTO 
                                ACCOUNT_STATUS ( ACCOUNT_ID,BRAG_STATUS,START_DATE,END_DATE,BRAG_RATIO,
                                                FINANCIAL_COB_COUNT,FREEZING_STATUS)
                                         VALUES( ACCOUNTID,BRAGSTATUS,STARTDATE,ENDDATE,BRAGRATIO,
                                                FINANCIALCOBCOUNT,FREEZINGSTATUS);
                                COMMIT;
                    ELSE
                        UPDATE ACCOUNT_STATUS SET BRAG_STATUS=BRAGSTATUS,START_DATE=STARTDATE,
                                       END_DATE=ENDDATE,
                                       BRAG_RATIO=BRAGRATIO,
                                       FINANCIAL_COB_COUNT=FINANCIALCOBCOUNT,
                                     --  LAST_DAY_RECALCULATION=LASTDAYRECALCULATION,
                                       FREEZING_STATUS=FREEZINGSTATUS
                                 WHERE ACCOUNT_ID=ACCOUNTID and TRUNC(START_DATE) = TRUNC((sysdate),'month');
                                COMMIT;
                    END IF;

                    UPDATE ACCOUNT SET BRAG_STATUS=BRAGSTATUS WHERE ID=ACCOUNTID;
                    COMMIT;
        END IF;
ELSE
                    UPDATE ACCOUNT_STATUS SET BRAG_STATUS=BRAGSTATUS,START_DATE=STARTDATE,
                                       END_DATE=ENDDATE,
                                       BRAG_RATIO=BRAGRATIO,
                                       FINANCIAL_COB_COUNT=FINANCIALCOBCOUNT,
                                     --  LAST_DAY_RECALCULATION=LASTDAYRECALCULATION,
                                       FREEZING_STATUS=FREEZINGSTATUS
                                 WHERE ACCOUNT_ID=ACCOUNTID and TRUNC(START_DATE) = TRUNC(STARTDATE);
                                COMMIT;

        END IF;


END USP_JOBS_UPDATE_ACCOUNT_STATUS_INACTIVE;
