set define off;

CREATE OR REPLACE PROCEDURE "LBACCOUNTS"."USP_JOBS_GET_ACCOUNTS_BY_TIMEZONE" 
( 
    TIMEZONEID  IN NUMBER,
    LIST_CURSOR OUT SYS_REFCURSOR
)
AS
BEGIN   
    OPEN LIST_CURSOR FOR  
        SELECT 
            AC.ID,
            AC.NAME,

            TZ.ID AS TIMEZONE_ID,
            TZ.TIMEZONE_ID AS TIMEZONE_NAME,
            TZ.TIMEZONE_VALUE AS TIMEZONE_DESCRIPTION,
            TZ.GMT_VALUE AS TIMEZONE_VALUE,

            AC.BRAG_STATUS,
            
            AC.IS_ACTIVE,
            AC.IS_ARCHIVE,
            AC.IS_AMENDED,
            AC.AMENDMENT_BANNER_DATE,

            AC.GL_ACTIVE,
            AC.ACCESS_EMPTY,
            AC.DATAXML,

            T.Id AS TYPE_ID,
            T.NAME AS TYPE_NAME,

            COV.ID AS COVERAGE_ID,
            COV.NAME AS COVERAGE_NAME,

            R.ID AS REGION_ID,
            R.NAME AS REGION_NAME,

            AC.ACCOUNT_SIZE,
            AC.ACCOUNT_VALUE,

            AC.CM_ALERTED,
            AC.PRE_ACTIVATED,
            AC.IS_SENSITIVE, 

            AC.CURRENCY_TYPE,
            AC.LEGAL_NAME,
            AC.ALIAS,

            AC.ADDRESS,

            AC.LEAD_COUNTRY,
            LEAD_C.COUNTRY_NAME AS LEAD_COUNTRY_NAME,

            A_CUR.CONTRACT_CURRENCY,

            AC.CONTRACTING_ENTITY,
            CE.NAME AS CONTRACTING_ENTITY_NAME,

            AC.OFFERING_ORG,
            ORG.OFFERING_ORG_NAME,

            AC.IN_TRANSITION,
            AC.SUBMISSION_DATE,

            AC.IN_DUMP_GENERATION,
            AC.IN_PROCESSING,
            AC.IN_RE_INDEXING

        FROM
            ACCOUNT AC
            LEFT OUTER JOIN CONTRACT_ENTITY CE ON (CE.ID = AC.CONTRACTING_ENTITY)
            LEFT OUTER JOIN ACCOUNT_CURRENCY A_CUR  ON AC.ID = A_CUR.ACCOUNT_ID
            LEFT OUTER JOIN ACCOUNT_OFFERING_ORG ORG ON AC.OFFERING_ORG = ORG.ID,
            ACCOUNT_COVERAGE COV,
            ACCOUNT_TYPE T,
            DELIVERY_COUNTRY LEAD_C,
            REGION R,
            TIMEZONE TZ

        WHERE 
            COV.ID = AC.COVERAGE AND
            T.ID = AC.TYPE AND
            AC.IS_DELETE = 0 AND
            AC.TIMEZONE = TZ.ID AND
            AC.REGION_ID = R.Id AND
            AC.LEAD_COUNTRY = LEAD_C.COUNTRY_ID AND
            TZ.ID = TIMEZONEID AND
            AC.IS_ARCHIVE =0 ;
END;




