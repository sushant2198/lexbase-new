--------------------------------------------------------
--  File created - Friday-August-30-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure USP_JOBS_GET_ACCOUNT_FOR_STATUS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE NONEDITIONABLE PROCEDURE "LBACCOUNTS"."USP_JOBS_GET_ACCOUNT_FOR_STATUS" 
(
    LIST_CURSOR OUT SYS_REFCURSOR
)
AS 

BEGIN
            OPEN LIST_CURSOR FOR  
                SELECT 
                    AC.ID,
                    AC.NAME,
        
                    TZ.ID AS TIMEZONE_ID,
                    TZ.TIMEZONE_ID AS TIMEZONE_NAME,
                    TZ.TIMEZONE_VALUE AS TIMEZONE_DESCRIPTION,
                    TZ.GMT_VALUE AS TIMEZONE_VALUE,
        
                    AC.BRAG_STATUS,
                    
                    AC.IS_ACTIVE,
                    AC.IS_ARCHIVE
                    FROM
                    ACCOUNT AC
                    LEFT OUTER JOIN TIMEZONE TZ  ON AC.TIMEZONE = TZ.ID;
END USP_JOBS_GET_ACCOUNT_FOR_STATUS;

/
