﻿using Lexbase.Jobs;
using Lexbase.Providers;
using log4net;
namespace Lexbase.Implementation.Jobs
{
    public class DeleteTempBlobJob : IDeleteTempBlobJob
    {

        private readonly ILog log = LogManager.GetLogger(typeof(DeleteTempBlobJob));

        private readonly IBlobProvider blobProvider;

        public DeleteTempBlobJob(IBlobProvider blobProvider)
        {
            this.blobProvider = blobProvider;
        }
        void IJob.Execute(string param)
        {
            var logContext = $"[Execute:{param}]";
            log.Info($"{logContext} Started");

            this.blobProvider.DeleteTempBlob();
        }
    }
}
