﻿using Lexbase.Jobs;
using Lexbase.Models;
using Lexbase.Providers;
using Lexbase.Repositories;
using Lexbase.Services;
using log4net;
using System.Collections.Generic;

namespace Lexbase.Implementation.Jobs
{
    public class AccountDumpJob : IAccountDumpJob
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ActivationProcessJob));
        private readonly IAccountRepository accountRepository;

        private readonly IAccountService accountService;
        private readonly IExcelService excelService;
        private readonly IEmailService emailService;
        private readonly IFileProvider fileProvider;
        private readonly IUserRepository userRepository;

        public AccountDumpJob(
            IAccountRepository accountRepository,
            IAccountService accountService,
            IExcelService excelService,
            IEmailService emailService,
            IFileProvider fileProvider,
            IUserRepository userRepository
            )
        {
            this.accountRepository = accountRepository;
            this.accountService = accountService;
            this.excelService = excelService;
            this.emailService = emailService;
            this.fileProvider = fileProvider;
            this.userRepository = userRepository;
        }

        public void Execute(string param)
        {
            var logContext = $"[Execute:{param}]";
            log.Info($"{logContext} Started");

            var accounts = accountRepository.Search(new Queries.AccountQuery
            {
                IsActive = true
            });

            log.Debug($"{logContext} {accounts.Count} account(s) found");

            if (accounts.Count <= 0)
            {
                return;
            }
            IDictionary<long, IDictionary<string, AccountDump>> itemMap = new Dictionary<long, IDictionary<string, AccountDump>>();
            foreach (var account in accounts)
            {
                account.Managers = account.Managers ?? accountRepository.GetManagers(account.Id);
                var dump = accountService.GetAccountDump(account.Id);
                dump.Account = account;
                foreach (var user in account.Managers)
                {
                    GroupReports(itemMap, dump, user.Id, account.Name);
                }

                foreach (var userId in itemMap.Keys)
                {
                    var attachment = excelService.Create(dump, "AccountDump");
                    var users = userRepository.Get(userId);
                    if (users != null)
                    {
                        emailService.Send(users, "Account-Dump", attachment);
                    }
                    else
                    {
                        log.Error($"user with id '{userId}' does not exist");
                    }
                }
                //attachment.Path = fileProvider.Upload(attachment.Path);                
            }

        }
        private void GroupReports(
           IDictionary<long, IDictionary<string, AccountDump>> itemsMap,
           AccountDump item,
           long userId,
           string group
           )
        {
            if (!itemsMap.ContainsKey(userId))
            {
                itemsMap[userId] = new Dictionary<string, AccountDump>();
            }

            var userItems = itemsMap[userId];
            userItems[group] = item;
        }
    }
}