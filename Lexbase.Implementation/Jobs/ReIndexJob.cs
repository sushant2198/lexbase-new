﻿using Lexbase.Exceptions;
using Lexbase.Jobs;
using Lexbase.Services;
using log4net;

namespace Lexbase.Implementation.Jobs
{
    public class ReIndexJob : IReIndexJob
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ReIndexJob));
        private readonly IIndexingService _indexingService;

        public ReIndexJob(IIndexingService indexingService)
        {
            _indexingService = indexingService;
        }
        public void Execute(string param)
        {
            var logContext = $"[Execute:{param}]";
            log.Info($"{logContext} Started");

            if (string.IsNullOrEmpty(param))
            {
                throw new JobException("no index name specified");
            }

            _indexingService.ReIndex(param);
        }
    }
}
