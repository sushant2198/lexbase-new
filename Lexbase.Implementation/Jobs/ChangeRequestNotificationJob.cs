﻿using Lexbase.Exceptions;
using Lexbase.Jobs;
using Lexbase.Services;
using log4net;

namespace Lexbase.Implementation.Jobs
{
    public class ChangeRequestNotificationJob : IChangeRequestNotificationJob
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ChangeRequestNotificationJob));

        private readonly INotificationService notificationService;

        public ChangeRequestNotificationJob(
            INotificationService notificationService
        )
        {
            this.notificationService = notificationService;
        }

        public void Execute(string param)
        {
            var logContext = $"[Execute:{param}]";
            log.Info($"{logContext} Started");

            if (string.IsNullOrEmpty(param))
            {
                throw new JobException("no timezone id specified");
            }

            if (!int.TryParse(param, out int timezoneId))
            {
                throw new JobException($"invalid timezone id: {param}");
            }

            notificationService.SendChangeRequests(timezoneId);

            log.Info($"{logContext} Done");
        }
    }

}