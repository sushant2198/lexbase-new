using Lexbase.Exceptions;
using Lexbase.Helpers;
using Lexbase.Jobs;
using Lexbase.Models;
using Lexbase.Providers;
using Lexbase.Repositories;
using Lexbase.Services;
using log4net;
using System;
using System.Collections.Generic;

namespace Lexbase.Implementation.Jobs
{
    public class MarkOverDueJob : IMarkOverDueJob
    {
        private readonly ILog log = LogManager.GetLogger(typeof(MarkOverDueJob));

        private readonly IChildObligationRepository childObligationRepository;
        private readonly ITimezoneRepository timezoneRepository;
        private readonly IAccountRepository accountRepository;
        private readonly IObligationService obligationService;
        private readonly ICacheProvider cacheProvider;

        public MarkOverDueJob(
            IChildObligationRepository childObligationRepository,
            ITimezoneRepository timezoneRepository,
            IAccountRepository accountRepository,
            IObligationService obligationService,
            ICacheProvider cacheProvider
        )
        {
            this.childObligationRepository = childObligationRepository;
            this.timezoneRepository = timezoneRepository;
            this.accountRepository = accountRepository;
            this.obligationService = obligationService;
            this.cacheProvider = cacheProvider;
        }

        public void Execute(string param)
        {
            var logContext = $"[Execute:{param}]";
            log.Info($"{logContext} Started");

            if (string.IsNullOrEmpty(param))
            {
                throw new JobException("no timezone id specified");
            }

            if (!int.TryParse(param, out int timezoneId))
            {
                throw new JobException($"invalid timezone id: {param}");
            }

            var timezone = timezoneRepository.Get(timezoneId);

            log.Debug($"{logContext} getting accounts by time zone");

            List<Account> accounts = accountRepository.Search(new Queries.AccountQuery
            {
                TimezoneId = timezoneId
            }) as List<Account>;

            var accountIds = new List<long>();
            foreach (var item in accounts)
            {
                accountIds.Add(item.Id);
            }
            var offsetDaysValue = UlxConfig.GetInteger($"Timezones:{timezoneId}:NotificationOffsetDays", 0);

            var dueDate = DateHelper.ToBOD(DateTime.Today.AddDays(offsetDaysValue), timezone);


            var clearCache = false;
            var childObligations = childObligationRepository.Search(new Queries.ChildObligationQuery
            {
                AccountIds = accountIds,
                DueDate = dueDate
            });

            log.Info($"{logContext} {childObligations.Count} childObligation(s) are overdue");

            foreach (var item in childObligations)
            {
                obligationService.MarkOverDue(item);
                clearCache = true;
            }

            if (clearCache)
            {
                cacheProvider.Remove("ObligaitonList");
                cacheProvider.Remove("Obligations");
            }

            log.Info($"{logContext} Done");
        }
    }
}