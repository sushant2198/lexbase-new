﻿using Lexbase.Jobs;
using Lexbase.Models;
using Lexbase.Repositories;
using Lexbase.Services;
using log4net;
using System;
using System.Collections.Generic;

namespace Lexbase.Implementation.Jobs
{
    public class UserAuditLogJob : IUserAuditLogJob
    {
        private readonly ILog log = LogManager.GetLogger(typeof(UserAuditLogJob));
        private readonly IAccountRepository accountRepository;
        private readonly ISARepository saRepository;
        private readonly IMSARepository msaRepository;
        private readonly IUserRepository userRepository;
        private readonly IUserService userService;
        private readonly IEmailService emailService;
        private readonly IUserAuditLogRepository userAuditLogRepository;
        private readonly IObligationRepository obligationRepository;
        private readonly IExcelService excelService;

        public UserAuditLogJob(
            IAccountRepository accountRepository,
            ISARepository saRepository,
            IMSARepository msaRepository,
            IUserRepository userRepository,
            IUserService userService,
            IEmailService emailService,
            IUserAuditLogRepository userAuditLogRepository,
            IObligationRepository obligationRepository,
            IExcelService excelService
            )
        {
            this.accountRepository = accountRepository;
            this.saRepository = saRepository;
            this.msaRepository = msaRepository;
            this.userRepository = userRepository;
            this.userService = userService;
            this.emailService = emailService;

            this.userAuditLogRepository = userAuditLogRepository;
            this.obligationRepository = obligationRepository;
            this.excelService = excelService;
        }
        public void Execute(string param)
        {
            var logContext = $"[Execute:{param}]";
            log.Info($"{logContext} Started");
            var auditLogList = userAuditLogRepository.Search(new Queries.UserAuditLogQuery
            {
                NoOfDays = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)
            });

            var itemsMap = new Dictionary<long, IDictionary<string, IList<UserAuditLog>>>();
            var accountManagers = new Dictionary<long, IList<User>>();

            foreach (var item in auditLogList)
            {
                if (item.GroupFor != 0)
                {
                    switch (item.GroupFor)
                    {
                        case GroupFor.Account:
                            item.Account = accountRepository.Get(item.GroupId);
                            if (item.Account != null)
                            {
                                item.GroupName = item.Account.Name;
                            }
                            break;

                        case GroupFor.MSA:
                            item.MSA = msaRepository.Get(item.GroupId);
                            if (item.MSA != null)
                            {
                                item.GroupName = item.MSA.Name;
                                item.Account = accountRepository.Get(item.MSA.Account.Id);
                            }
                            break;

                        //case GroupFor.SOW:
                        //    item.SOW = msaRepository.Get(item.GroupId);
                        //    item.GroupName = item.MSA.Name;
                        //    item.Account = accountRepository.Get(item.MSA.Account.Id);
                        //    break;

                        case GroupFor.SA:
                            item.SA = saRepository.Get(item.GroupId);
                            if (item.SA != null)
                            {
                                item.GroupName = item.SA.Name;
                                item.Account = accountRepository.Get(item.SA.Account.Id);
                            }
                            break;

                        //case GroupFor.SubSOW:
                        //    item.SubSOW = msaRepository.Get(item.GroupId);
                        //    item.GroupName = item.MSA.Name;
                        //    item.Account = accountRepository.Get(item.MSA.Account.Id);
                        //    break;

                        case GroupFor.Obligation:
                            item.Obligation = obligationRepository.Get(item.GroupId);
                            if (item.Obligation != null)
                            {
                                item.GroupName = item.Obligation.Name;
                                item.Account = accountRepository.Get(item.Obligation.Account.Id);
                                //item.Account = item.Obligation.Account;
                            }
                            break;
                    }

                    item.User = userRepository.Get(item.User.Id);
                    if (item.Account != null)
                    {
                        if (!accountManagers.ContainsKey(item.Account.Id))
                        {
                            accountManagers[item.Account.Id] = userService.GetManagers(item.Account.Id);
                        }

                        foreach (var manager in accountManagers[item.Account.Id])
                        {
                            Group(itemsMap, item, manager.Id, item.Account.Name);
                        }
                    }
                }
            }

            foreach (var userId in itemsMap.Keys)
            {
                var attachment = excelService.Create(itemsMap[userId], "UserAuditLog");
                var user = userRepository.Get(userId);
                emailService.Send(user, $"UserAuditLog-Report", attachment);
            }
        }



        private void Group(
           IDictionary<long, IDictionary<string, IList<UserAuditLog>>> itemsMap,
           UserAuditLog item,
           long userId,
           string group
           )
        {
            if (!itemsMap.ContainsKey(userId))
            {
                itemsMap[userId] = new Dictionary<string, IList<UserAuditLog>>();
            }

            var userItems = itemsMap[userId];

            if (!userItems.ContainsKey(group))
            {
                userItems[group] = new List<UserAuditLog>();
            }

            var groupedItems = userItems[group];

            groupedItems.Add(item);
        }
    }
}