using Lexbase.Jobs;
using Lexbase.Providers;
using Lexbase.Repositories;
using Lexbase.Services;
using log4net;

namespace Lexbase.Implementation.Jobs
{
    public class ActivationProcessJob : IAccountActivationProcessJob
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ActivationProcessJob));

        private readonly IAccountRepository accountRepository;
        private readonly IAccountService accountService;
        private readonly ISARepository saRepository;
        private readonly IMSARepository msaRepository;
        private readonly ISAService saService;
        private readonly ICacheProvider cacheProvider;

        public ActivationProcessJob(
            IAccountRepository accountRepository,
            IAccountService accountService,
            ISARepository saRepository,
            IMSARepository msaRepository,
            ISAService saService,
            ICacheProvider cacheProvider
        )
        {
            this.accountRepository = accountRepository;
            this.accountService = accountService;
            this.saRepository = saRepository;
            this.msaRepository = msaRepository;
            this.saService = saService;
            this.cacheProvider = cacheProvider;
        }

        public void Execute(string param)
        {
            var logContext = $"[Execute:{param}]";
            log.Info($"{logContext} Started");

            var clearCache = false;
            var accounts = accountRepository.Search(new Queries.AccountQuery
            {
                NeedActivation = true
            });
            log.Debug($"{logContext} {accounts.Count} account(s) found");
            foreach (var item in accounts)
            {
                accountService.Activate(item.Id);
                clearCache = true;
            }

            var msaServiceAgreements = msaRepository.Search(new Queries.MServiceAgreementQuery
            {
                NeedActivation = true
            });
            log.Info($"{logContext} {msaServiceAgreements.Count} msa(s) needs activation");
            foreach (var item in msaServiceAgreements)
            {
                saService.ActivateMSA(item.Id);
                clearCache = true;
            }

            var saServiceAgreements = saRepository.Search(new Queries.ServiceAgreementQuery
            {
                NeedActivation = true
            });
            log.Info($"{logContext} {saServiceAgreements.Count} sa(s) needs activation");
            foreach (var item in saServiceAgreements)
            {
                saService.ActivateSA(item.Id);
                clearCache = true;
            }

            if (clearCache)
            {
                cacheProvider.Remove("AccountList");
                cacheProvider.Remove("MsaList");
                cacheProvider.Remove("SaList");
                cacheProvider.Remove("ObligaitonList");
                cacheProvider.Remove("Obligations");
                cacheProvider.Remove("MasterOB");
                cacheProvider.Remove("UserList");
            }

            log.Info($"{logContext} Done");
        }
    }
}