﻿using Lexbase.Jobs;
using Lexbase.Providers;
using Lexbase.Repositories;
using Lexbase.Services;
using log4net;

namespace Lexbase.Implementation.Jobs
{
    public class SchedulerJob : ISchedulerJob
    {
        private readonly ILog log = LogManager.GetLogger(typeof(SchedulerJob));
        private readonly IObligationRepository obligationRepository;
        private readonly IObligationService obligationService;
        private readonly ICacheProvider cacheProvider;

        public SchedulerJob(
            IObligationRepository obligationRepository,
            IObligationService obligationService,
            ICacheProvider cacheProvider
        )
        {
            this.obligationRepository = obligationRepository;
            this.obligationService = obligationService;
            this.cacheProvider = cacheProvider;
        }

        public void Execute(string param)
        {
            var logContext = $"[Execute:{param}]";
            log.Info($"{logContext} Started");

            var clearCache = false;

            var obligations = obligationRepository.Search(new Queries.ObligationQuery
            {
                ForScheduler = true
            });

            log.Debug($"{logContext} {obligations.Count} obligation(s) found");
            foreach (var item in obligations)
            {
                obligationService.Schedule(item.Id);
                clearCache = true;
            }

            if (clearCache)
            {
                cacheProvider.Remove("ObligaitonList");
                cacheProvider.Remove("Obligations");
                cacheProvider.Remove("MasterOB");
            }

            log.Info($"{logContext} Done");
        }
    }
}