﻿using Lexbase.Jobs;
using Lexbase.Models;
using Lexbase.Repositories;
using Lexbase.Services;
using log4net;
using System.Collections.Generic;

namespace Lexbase.Implementation.Jobs
{
    public class COBReportJob : ICOBReportJob
    {
        private readonly ILog log = LogManager.GetLogger(typeof(COBReportJob));

        private readonly IChildObligationRepository childObligationRepository;
        private readonly IAccountRepository accountRepository;
        private readonly IUserRepository userRepository;
        private readonly IExcelService excelService;
        private readonly IEmailService emailService;

        public COBReportJob(
            IChildObligationRepository childObligationRepository,
            IUserRepository userRepository,
            IExcelService excelService,
            IEmailService emailService,
            IAccountRepository accountRepository)
        {
            this.childObligationRepository = childObligationRepository;
            this.accountRepository = accountRepository;
            this.userRepository = userRepository;
            this.excelService = excelService;
            this.emailService = emailService;
        }
        void IJob.Execute(string param)
        {
            var logContext = $"[Execute:{param}]";
            log.Info($"{logContext} Started");

            var accounts = accountRepository.Search(new Queries.AccountQuery
            {
                IsActive = true
            });

            log.Debug($"{logContext} {accounts.Count} account(s) found");

            if (accounts.Count <= 0)
            {
                return;
            }

            var accountIds = new List<long>();
            foreach (var item in accounts)
            {
                accountIds.Add(item.Id);
            }

            IDictionary<long, IDictionary<string, IList<ChildObligation>>> itemMap = new Dictionary<long, IDictionary<string, IList<ChildObligation>>>();

            var excelTemplate = "";

            var query = new Queries.ChildObligationQuery
            {
                AccountIds = accountIds
            };

            switch (param.ToLower())
            {
                case "missed":
                    excelTemplate = "COBsMissed";
                    query.Status = "overdue";
                    break;

                case "completed6m":
                    excelTemplate = "COBsCompleted6M";
                    query.Status = "completed";
                    query.ReportPeriod = 180;
                    break;

                case "completed3m":
                    excelTemplate = "COBsCompleted3M";
                    query.Status = "completed";
                    query.ReportPeriod = 90;
                    break;

                case "monitored":
                    excelTemplate = "COBsMonitored";
                    query.ActionType = "M";
                    query.ReportPeriod = 1;
                    break;

                case "overduereport01":
                    excelTemplate = "COBsOverdueReport01";
                    query.Status = "overdue";
                    query.ActionType = "M";
                    break;

                case "overduereport16":
                    excelTemplate = "COBsOverdueReport16";
                    query.Status = "overdue";
                    query.ActionType = "M";
                    break;
            }

            var cobs = childObligationRepository.Search(query);

            log.Debug($"{logContext} {cobs.Count} cob(s) found");

            if (cobs.Count <= 0)
            {
                return;
            }

            var accountMap = new Dictionary<long, Account>();
            foreach (var account in accounts)
            {
                accountMap[account.Id] = account;
            }

            foreach (var item in cobs)
            {
                var account = accountMap[item.Parent.Account.Id];
                account.Managers = account.Managers ?? accountRepository.GetManagers(account.Id);

                item.Parent.Account = account;

                if (item.Parent.OwnerUser != null)
                {
                    item.Parent.OwnerUser = userRepository.Get(item.Parent.OwnerUser.Id);
                }

                if (item.Parent.OwnerManager != null)
                {
                    item.Parent.OwnerManager = userRepository.Get(item.Parent.OwnerManager.Id);
                }

                foreach (var user in account.Managers)
                {
                    GroupCOBs(itemMap, item, user.Id, account.Name);
                }
            }

            foreach (var userId in itemMap.Keys)
            {
                var attachment = excelService.Create(itemMap[userId], excelTemplate);
                var user = userRepository.Get(userId);
                emailService.Send(user, $"COB-Report-{param}", attachment);
            }
        }

        private void GroupCOBs(
            IDictionary<long, IDictionary<string, IList<ChildObligation>>> itemsMap,
            ChildObligation item,
            long userId,
            string group
            )
        {
            if (!itemsMap.ContainsKey(userId))
            {
                itemsMap[userId] = new Dictionary<string, IList<ChildObligation>>();
            }

            var userItems = itemsMap[userId];

            if (!userItems.ContainsKey(group))
            {
                userItems[group] = new List<ChildObligation>();
            }

            var groupedItems = userItems[group];

            groupedItems.Add(item);
        }
    }
}
