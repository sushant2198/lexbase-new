using Lexbase.Exceptions;
using Lexbase.Jobs;
using Lexbase.Providers;
using Lexbase.Services;
using log4net;

namespace Lexbase.Implementation.Jobs
{
    public class NotificationJob : INotificationJob
    {
        private readonly ILog log = LogManager.GetLogger(typeof(NotificationJob));
        private readonly INotificationService notificationService;
        private readonly ICacheProvider cacheProvider;

        public NotificationJob(
            INotificationService notificationService,
            ICacheProvider cacheProvider
        )
        {
            this.notificationService = notificationService;
            this.cacheProvider = cacheProvider;
        }

        public void Execute(string param)
        {
            var logContext = $"[Execute:{param}]";
            log.Info($"{logContext} Started");

            if (string.IsNullOrEmpty(param))
            {
                throw new JobException("no timezone id specified");
            }

            if (!int.TryParse(param, out int timezoneId))
            {
                throw new JobException($"invalid timezone id: {param}");
            }

            var clearCache = notificationService.Send(timezoneId);

            if (clearCache)
            {
                cacheProvider.Remove("ObligaitonList");
                cacheProvider.Remove("Obligations");
            }

            log.Info($"{logContext} Done");
        }
    }
}