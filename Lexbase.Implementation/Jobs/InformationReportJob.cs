﻿using Lexbase.Jobs;
using Lexbase.Models;
using Lexbase.Repositories;
using Lexbase.Services;
using log4net;
using System.Collections.Generic;

namespace Lexbase.Implementation.Jobs
{
    public class InformationReportJob : IInformationReportJob
    {
        private readonly ILog log = LogManager.GetLogger(typeof(TrendReportJob));
        private readonly IAccountRepository _accountRepository;
        private readonly IUserRepository _userRepository;
        private readonly IExcelService _excelService;
        private readonly IEmailService _emailService;

        public InformationReportJob(IUserRepository userRepository,
            IExcelService excelService,
            IEmailService emailService,
            IAccountRepository accountRepository
            )
        {
            _userRepository = userRepository;
            _excelService = excelService;
            _emailService = emailService;
            _accountRepository = accountRepository;
        }
        public void Execute(string param)
        {
            var logContext = $"[Execute:{param}]";
            log.Info($"{logContext} Started");


            var accounts = _accountRepository.GetAccountInformationReport();

            log.Debug($"{logContext} {accounts.Count} account(s) found");

            if (accounts.Count <= 0)
            {
                return;
            }

            foreach (var account in accounts)
            {
                account.Managers = account.Managers ?? _accountRepository.GetManagers(account.AccountId);
            }

            var itemMap = new Dictionary<long, IDictionary<string, IList<AccountInformationData>>>();

            foreach (var account in accounts)
            {
                foreach (var user in account.Managers)
                {
                    GroupReports(itemMap, account, user.Id, account.Name);
                }
            }

            foreach (var userId in itemMap.Keys)
            {
                var user = _userRepository.Get(userId);
                var attachment = _excelService.Create(itemMap[userId], "AccountInformationReport", user.FullName);
                if (user != null)
                {
                    _emailService.Send(user, "Account-Report-Information", attachment);
                }
                else
                {
                    log.Error($"user with id '{userId}' does not exist");
                }
            }
        }

        private void GroupReports(
           IDictionary<long, IDictionary<string, IList<AccountInformationData>>> itemsMap,
           AccountInformationData item,
           long userId,
           string group
           )
        {
            if (!itemsMap.ContainsKey(userId))
            {
                itemsMap[userId] = new Dictionary<string, IList<AccountInformationData>>();
            }

            var userItems = itemsMap[userId];

            if (!userItems.ContainsKey(group))
            {
                userItems[group] = new List<AccountInformationData>();
            }
            userItems[group].Add(item);
        }
    }
}
