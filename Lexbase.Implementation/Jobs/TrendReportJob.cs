﻿using Lexbase.Jobs;
using Lexbase.Models;
using Lexbase.Repositories;
using Lexbase.Services;
using log4net;
using System.Collections.Generic;

namespace Lexbase.Implementation.Jobs
{
    public class TrendReportJob : ITrendReportJob
    {
        private readonly ILog log = LogManager.GetLogger(typeof(TrendReportJob));
        private readonly IAccountService accountService;
        private readonly IAccountReportService accountReportService;
        private readonly IAccountRepository accountRepository;
        private readonly IUserRepository userRepository;
        private readonly IExcelService excelService;
        private readonly IEmailService emailService;

        public TrendReportJob(IAccountService accountService,
            IAccountReportService accountReportService,
            IAccountRepository accountRepository,
            IUserRepository userRepository,
            IExcelService excelService,
            IEmailService emailService
            )
        {
            this.accountService = accountService;
            this.accountReportService = accountReportService;
            this.accountRepository = accountRepository;
            this.userRepository = userRepository;
            this.excelService = excelService;
            this.emailService = emailService;
        }

        public void Execute(string param)
        {
            var logContext = $"[Execute:{param}]";
            log.Info($"{logContext} Started");

            var accounts = accountRepository.Search(new Queries.AccountQuery
            {
                IsActive = true
            });

            log.Debug($"{logContext} {accounts.Count} account(s) found");

            if (accounts.Count <= 0)
            {
                return;
            }

            foreach (var account in accounts)
            {
                account.Managers = account.Managers ?? accountRepository.GetManagers(account.Id);
            }

            switch (param.ToLower())
            {
                case "functional":
                    CreateAndSendFunctional(accounts);
                    break;

                case "cumulative":
                    CreateAndSendCumulative(accounts);
                    break;

            }
        }

        private void CreateAndSendFunctional(IList<Account> accounts)
        {
            var itemMap = new Dictionary<long, IDictionary<string, AccountTrendReportFunctional>>();

            foreach (var account in accounts)
            {
                var trendReport = accountReportService.GetAccountTrendFunctional(account.Id);
                trendReport.Account = account;

                foreach (var user in account.Managers)
                {
                    GroupReports(itemMap, trendReport, user.Id, account.Name);
                }
            }

            foreach (var userId in itemMap.Keys)
            {
                var attachment = excelService.Create(itemMap[userId], "AccountTrendReportFunctional");

                var user = userRepository.Get(userId);
                if (user != null)
                {
                    emailService.Send(user, "Account-Report-Trend", attachment);
                }
                else
                {
                    log.Error($"user with id '{userId}' does not exist");
                }
            }
        }

        private void CreateAndSendCumulative(IList<Account> accounts)
        {
            var itemMap = new Dictionary<long, IDictionary<string, IList<AccountTrendReportCumulative>>>();

            foreach (var account in accounts)
            {
                var trendReport = accountReportService.GetAccountTrendCumulative(account.Id);
                trendReport.Account = account;

                foreach (var user in account.Managers)
                {
                    GroupReports(itemMap, trendReport, user.Id, "Report");
                }
            }

            foreach (var userId in itemMap.Keys)
            {
                var attachment = excelService.Create(itemMap[userId], "AccountTrendReportCumulative");

                var user = userRepository.Get(userId);
                if (user != null)
                {
                    emailService.Send(user, "Account-Report-Trend", attachment);
                }
                else
                {
                    log.Error($"user with id '{userId}' does not exist");
                }
            }
        }

        private void GroupReports(
           IDictionary<long, IDictionary<string, IList<AccountTrendReportCumulative>>> itemsMap,
           AccountTrendReportCumulative item,
           long userId,
           string group
           )
        {
            if (!itemsMap.ContainsKey(userId))
            {
                itemsMap[userId] = new Dictionary<string, IList<AccountTrendReportCumulative>>();
            }

            var userItems = itemsMap[userId];

            if (!userItems.ContainsKey(group))
            {
                userItems[group] = new List<AccountTrendReportCumulative>();
            }
            userItems[group].Add(item);
        }


        private void GroupReports(
           IDictionary<long, IDictionary<string, AccountTrendReportFunctional>> itemsMap,
           AccountTrendReportFunctional item,
           long userId,
           string group
           )
        {
            if (!itemsMap.ContainsKey(userId))
            {
                itemsMap[userId] = new Dictionary<string, AccountTrendReportFunctional>();
            }

            var userItems = itemsMap[userId];
            userItems[group] = item;
        }
    }

}
