﻿using Lexbase.Jobs;
using Lexbase.Models;
using Lexbase.Queries;
using Lexbase.Repositories;
using Lexbase.Services;
using log4net;
using System.Collections.Generic;

namespace Lexbase.Implementation.Jobs
{
    public class HeatMapReportJob : IHeatMapReportJob
    {
        private readonly ILog log = LogManager.GetLogger(typeof(HeatMapReportJob));
        private readonly IAccountReportService accountReportService;
        private readonly IAccountReportRepository accountReportRepository;
        private readonly IAccountRepository accountRepository;
        private readonly IUserRepository userRepository;

        private readonly IExcelService excelService;
        private readonly IEmailService emailService;

        public HeatMapReportJob(
            IAccountReportService accountReportService,
            IAccountReportRepository accountReportRepository,
            IAccountRepository accountRepository,
            IUserRepository userRepository,
            IExcelService excelService,
            IEmailService emailService
            )
        {
            this.accountReportService = accountReportService;
            this.accountReportRepository = accountReportRepository;
            this.accountRepository = accountRepository;
            this.userRepository = userRepository;
            this.excelService = excelService;
            this.emailService = emailService;
        }

        public void Execute(string param)
        {
            var logContext = $"[Execute:{param}]";
            log.Info($"{logContext} Started");

            switch (param.ToLower())
            {
                case "send":
                    Send(logContext);
                    break;

                case "generate":
                    Generate(logContext);
                    break;
            }

        }

        private void Generate(string logContext)
        {
            var accounts = accountRepository.Search(new Queries.AccountQuery
            {
                IsActive = true
            });

            log.Debug($"{logContext} {accounts.Count} account(s) found");

            if (accounts.Count <= 0)
            {
                return;
            }

            foreach (var account in accounts)
            {
                accountReportRepository.GenerateHeatMap(new HeatMapQuery { AccountId = account.Id });
            }

        }

        private void Send(string logContext)
        {

            var accounts = accountRepository.Search(new Queries.AccountQuery
            {
                IsActive = true
            });

            log.Debug($"{logContext} {accounts.Count} account(s) found");

            if (accounts.Count <= 0)
            {
                return;
            }

            IDictionary<long, IDictionary<string, HeatMapReport>> itemMap = new Dictionary<long, IDictionary<string, HeatMapReport>>();

            foreach (var account in accounts)
            {
                account.Managers = account.Managers ?? accountRepository.GetManagers(account.Id);

                var heatMap = accountReportService.GetHeatMap(account.Id);
                heatMap.Account = account;

                foreach (var user in account.Managers)
                {
                    GroupReports(itemMap, heatMap, user.Id, account.Name);
                }

                foreach (var userId in itemMap.Keys)
                {
                    var attachment = excelService.Create(itemMap[userId], "HeatMapReport");

                    var user = userRepository.Get(userId);
                    if (user != null)
                    {
                        emailService.Send(user, "Account-Report-HeatMap", attachment);
                    }
                    else
                    {
                        log.Error($"user with id '{userId}' does not exist");
                    }
                }
            }
        }

        private void GroupReports(
           IDictionary<long, IDictionary<string, HeatMapReport>> itemsMap,
           HeatMapReport item,
           long userId,
           string group
           )
        {
            if (!itemsMap.ContainsKey(userId))
            {
                itemsMap[userId] = new Dictionary<string, HeatMapReport>();
            }

            var userItems = itemsMap[userId];
            userItems[group] = item;
        }
    }
}