﻿using Lexbase.Jobs;
using Lexbase.Providers;
using Lexbase.Repositories;
using Lexbase.Services;
using log4net;

namespace Lexbase.Implementation.Jobs
{
    public class AccountStatusProcessJob : IAccountStatusProcessJob
    {
        private readonly ILog log = LogManager.GetLogger(typeof(AccountStatusProcessJob));

        private readonly IAccountRepository accountRepository;
        private readonly IAccountService accountService;
        private readonly ICacheProvider cacheProvider;

        public AccountStatusProcessJob(
             IAccountRepository accountRepository,
             IAccountService accountService,
             ICacheProvider cacheProvider
       )
        {
            this.accountRepository = accountRepository;
            this.accountService = accountService;
            this.cacheProvider = cacheProvider;
        }

        public void Execute(string param)
        {
            log.Debug("Execute:Started AccountStatus");

            var accountStatus = accountRepository.GetAccountStatus();

            this.accountService.UpdateAccountStatus(accountStatus);

            cacheProvider.Remove("AccountList");
        }
    }
}