﻿using Lexbase.Jobs;
using Lexbase.Models;
using Lexbase.Repositories;
using log4net;
using System;

namespace Lexbase.Implementation.Jobs
{
    public class ScheduledReportJob : IScheduledReportJob
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ScheduledReportJob));
        private readonly IScheduledReportRepository scheduledReportRepository;

        public ScheduledReportJob(
            IScheduledReportRepository scheduledReportRepository)
        {
            this.scheduledReportRepository = scheduledReportRepository;
        }

        public void Execute(string param)
        {
            switch (param.ToLower())
            {
                case "send":
                    var items = this.scheduledReportRepository.Search(new Queries.ScheduledReportQuery());

                    foreach (var item in items)
                    {
                        Send(item);
                    }

                    break;

                case "clean":
                    this.scheduledReportRepository.Clean();
                    break;
            }
        }

        private void Send(ScheduledReport item)
        {
            scheduledReportRepository.StartProcessing(item.Id);

            var error = "";

            try
            {
                IJob job = null;
                switch (item.JobName.ToUpper())
                {

                    case "NOTIFICATIONBYTIMEZONE":
                        job = Context.Current.GetService<INotificationJob>();
                        break;

                    case "COBREPORT":
                        job = Context.Current.GetService<ICOBReportJob>();
                        break;

                    case "HEATMAPREPORT":
                        job = Context.Current.GetService<IHeatMapReportJob>();
                        break;

                    case "ACCOUNTDUMP":
                        job = Context.Current.GetService<IAccountDumpJob>();
                        break;

                    case "TRENDREPORT":
                        job = Context.Current.GetService<ITrendReportJob>();
                        break;

                    case "ACCOUNTINFORMATIONREPORT":
                        job = Context.Current.GetService<IInformationReportJob>();
                        break;

                    default:
                        throw new Exception($"{item.JobName} cannot be scheduled");
                }

                job.Execute(item.JobParam);
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }


            scheduledReportRepository.CompleteProcessing(item.Id, error);
        }
    }
}