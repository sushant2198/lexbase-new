﻿using Lexbase.Providers;
using log4net;

namespace Lexbase.Implementation.Providers
{
    public class IndexProvider : IIndexProvider
    {
        private readonly ILog log = LogManager.GetLogger(typeof(IndexProvider));
        public void ReIndex(string name)
        {
            log.Info($"TODO: Implement {name} Re-Index");
        }
    }
}
