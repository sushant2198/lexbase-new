﻿using Lexbase.Providers;
using log4net;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Redis;
using Microsoft.Extensions.Options;
using StackExchange.Redis;
using System;
using System.Linq;

namespace Lexbase.Implementation.Providers
{
    public class RedisCache : ICacheProvider
    {
        private readonly ILog log = LogManager.GetLogger(typeof(RedisCache));
        private readonly IDistributedCache distributedCache;
        private IServer serverReturn = null;
        private readonly IOptions<RedisCacheOptions> _options;
        private static ConnectionMultiplexer lazyConnection = null;

        public RedisCache(IOptions<RedisCacheOptions> redisCacheOptions, IDistributedCache distributedCache)
        {
            this.distributedCache = distributedCache;
            _options = redisCacheOptions ?? throw new ArgumentNullException(nameof(redisCacheOptions));
            if (lazyConnection == null)
                lazyConnection = ConnectionMultiplexer.Connect(_options.Value.Configuration);
        }

        public void Remove(string key)
        {
            var keys = GetServer().Keys();
            if (keys.Any())
            {
                var delKey = keys.Where(k => k.ToString().ToLower().Contains(key.ToString().ToLower())).ToList();
                foreach (var dKey in delKey)
                {
                    var logContext = $"[Remove:{dKey}]";
                    distributedCache.Remove(dKey.ToString());
                    log.Debug($"{logContext} Done");
                }
            }
        }

        public IServer GetServer()
        {
            if (serverReturn != null && serverReturn.IsConnected)
                return serverReturn;

            string connectionString = _options.Value.Configuration;
            var connectionMultiplexer = ConnectionMultiplexer.Connect(connectionString);
            if (connectionMultiplexer.IsConnected)
            {
                var endpoints = connectionMultiplexer.GetEndPoints();
                serverReturn = connectionMultiplexer.GetServer(endpoints[0]);
            }
            return serverReturn;
        }
    }
}
