﻿using Lexbase.Providers;
using log4net;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using System;

namespace Lexbase.Implementation.Providers
{
    public class BlobProvider : IBlobProvider
    {
        private readonly ILog log = LogManager.GetLogger(typeof(BlobProvider));
        public void DeleteTempBlob()
        {
            try
            {
                string blobFolderNameName = UlxConfig.Get("AZURE_BLOB_TEMP_FOLDER");
                CloudBlobContainer container = CreateCloudBlobContainer();
                DeleteBlobsInDirectory(container, blobFolderNameName);
            }
            catch (Exception ex)
            {
                log.Error("DeleteTempBlob:- ", ex);
            }

        }

        public void DeleteBlobsInDirectory(CloudBlobContainer container, string blobname)
        {

            CloudBlobDirectory blobDirectory = container.GetDirectoryReference(blobname);

            foreach (IListBlobItem item in blobDirectory.ListBlobs())
            {
                if (item is CloudBlobDirectory directory)
                {
                    DeleteBlobsInDirectory(container, directory.Prefix);
                }
                else if (item is CloudPageBlob pageBlob)
                {
                    CloudPageBlob cloudPageBlob = container.GetPageBlobReference(pageBlob.Name);
                    cloudPageBlob.Delete();
                }
                else if (item is CloudBlockBlob blockBlob)
                {

                    CloudBlockBlob cloudBlockBlob = container.GetBlockBlobReference(blockBlob.Name);
                    cloudBlockBlob.FetchAttributes();
                    if (cloudBlockBlob.Properties.LastModified < (DateTime.UtcNow.AddHours(-24)))
                    {
                        cloudBlockBlob.Delete(DeleteSnapshotsOption.IncludeSnapshots);
                    }

                }
            }

        }

        private CloudBlobContainer CreateCloudBlobContainer()
        {
            string blobconnectionString = UlxConfig.GetConnectionString("AZUREBLOB");
            string blobContainerName = UlxConfig.Get("AZURE_BLOB_CONTAINER_JOBS");

            CloudStorageAccount cloudStorageAccount = CloudStorageAccount.Parse(blobconnectionString);
            CloudBlobClient cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();
            CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference(blobContainerName);
            cloudBlobContainer.CreateIfNotExistsAsync();
            cloudBlobContainer.SetPermissionsAsync(new BlobContainerPermissions()
            {
                PublicAccess = BlobContainerPublicAccessType.Blob
            });
            return cloudBlobContainer;
        }
    }


}
