using Lexbase.Providers;
using Lexbase.Providers.Models;
using log4net;

namespace Lexbase.Implementation.Providers
{
    public class ExcelProvider : IExcelProvider
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ExcelProvider));

        public IExcel CreateExcel(string name)
        {
            // if(Context.Current.IsDebug)
            // {
            log.Debug("CreateExcel as local File");
            return new ExcelFile(name);
            // }
            // else
            // {
            //     log.Debug("CreateExcel as Azure Blob");
            //     return new ExcelAzure(name);
            // }
        }
    }
}