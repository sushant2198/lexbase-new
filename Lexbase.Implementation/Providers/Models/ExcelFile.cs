using System;
using System.IO;

namespace Lexbase.Providers.Models
{
    public class ExcelFile : ExcelBase
    {
        public ExcelFile(string name) : base(name)
        {
        }

        public override string Save()
        {
            var fileName = $"{DateTime.Now.ToString("yyyyMMddhhmmssff")}-{Name}";

            using (var stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
            {
                Workbook.Write(stream);
                return stream.Name;
            }
        }
    }
}