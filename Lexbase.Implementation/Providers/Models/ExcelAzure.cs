﻿using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using System;
using System.IO;

namespace Lexbase.Providers.Models
{
    public class ExcelAzure : ExcelBase
    {
        public ExcelAzure(string name) : base(name)
        {
        }

        public override string Save()
        {
            string connectionString = UlxConfig.GetConnectionString("AZUREBLOB");
            string containerName = UlxConfig.Get("AZURE_BLOB_CONTAINER_JOBS");

            var cloudStorageAccount = CloudStorageAccount.Parse(connectionString);
            var cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();
            var cloudBlobContainer = cloudBlobClient.GetContainerReference(containerName);

            var fileName = $"{DateTime.Now.ToString("yyyyMMddhhmmssff")}-{Name}";

            var cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(fileName);

            cloudBlockBlob.Properties.ContentType = System.Net.Mime.MediaTypeNames.Application.Octet;

            using (var stream = new MemoryStream())
            {
                Workbook.Write(stream);

                stream.Seek(0, SeekOrigin.Begin);

                cloudBlockBlob.UploadFromStream(stream);

                return cloudBlockBlob.Uri.OriginalString;
            }
        }
    }
}