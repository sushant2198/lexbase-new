using Lexbase.Models;
using Lexbase.Providers;
using Lexbase.Providers.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;

namespace Lexbase.Implementation.Providers
{
    public class SMTPProvider : IEmailProvider
    {
        private readonly ILog log = LogManager.GetLogger(typeof(SMTPProvider));

        public void Send(Message message,
            User to,
            User from,
            IList<User> cc = null,
            IList<User> bcc = null
        )
        {
            log.Debug("Send");

            MailMessage msg = new MailMessage
            {
                From = new MailAddress(from.Email, $"{from.FirstName} {from.LastName}"),
                Subject = message.Subject,
                Body = message.Body,
                IsBodyHtml = true
            };

            msg.To.Add(new MailAddress(to.Email, $"{to.FirstName} {to.LastName}"));

            if (cc != null && cc.Count > 0)
            {
                foreach (var item in cc)
                {
                    msg.CC.Add(new MailAddress(item.Email, $"{item.FirstName} {item.LastName}"));
                }
            }
            else
            {
                var ccEmail = UlxConfig.Get("Emails:CC:Email");

                if (!string.IsNullOrEmpty(ccEmail))
                {
                    if (ccEmail.Contains(';'))
                    {
                        var emails = ccEmail.Split(';', StringSplitOptions.RemoveEmptyEntries);
                        foreach (var item in emails)
                        {
                            msg.CC.Add(new MailAddress(item));
                        }
                    }
                    else
                    {
                        var ccFirstName = UlxConfig.Get("Emails:CC:Firstname");
                        var ccLastName = UlxConfig.Get("Emails:CC:LastName");
                        if (string.IsNullOrEmpty(ccFirstName))
                        {
                            msg.CC.Add(new MailAddress(ccEmail, $"{ccFirstName} {ccLastName}"));
                        }
                        else
                        {
                            msg.CC.Add(new MailAddress(ccEmail));
                        }
                    }
                }
            }

            if (bcc != null && bcc.Count > 0)
            {
                foreach (var item in bcc)
                {
                    msg.Bcc.Add(new MailAddress(item.Email, $"{item.FirstName} {item.LastName}"));
                }
            }

            if (message.Attachment != null && !message.Attachment.Path.ToLower().StartsWith("http"))
            {
                var attachment = new Attachment(message.Attachment.Path);

                attachment.ContentDisposition.FileName = message.Attachment.FileName;
                msg.Attachments.Add(attachment);
            }

            try
            {

                var port = UlxConfig.GetInteger("SMTP:Port");
                var host = UlxConfig.Get("SMTP:Host");
                var user = UlxConfig.Get("SMTP:UserName");

                log.Debug($"[Send] Host: {host}, Port: {port}, user: {user}");

                var smtp = new SmtpClient
                {
                    Port = port,
                    Host = host,
                    EnableSsl = true,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(user, UlxConfig.Get("SMTP:Password")),
                    DeliveryMethod = SmtpDeliveryMethod.Network
                };
                smtp.Send(msg);
            }
            catch (Exception ex)
            {
                // Kill an log
                log.Error(ex);
            }
        }
    }
}