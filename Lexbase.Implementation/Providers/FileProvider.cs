﻿using Lexbase.Providers;
using log4net;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using System;
using System.IO;

namespace Lexbase.Implementation.Providers
{
    public class FileProvider : IFileProvider
    {
        private readonly ILog log = LogManager.GetLogger(typeof(FileProvider));
        public string Upload(string filePath)
        {
            try
            {
                string connectionString = UlxConfig.GetConnectionString("AZUREBLOB");
                string containerName = UlxConfig.Get("AZURE_BLOB_CONTAINER_JOBS");

                var cloudStorageAccount = CloudStorageAccount.Parse(connectionString);
                var cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();
                var cloudBlobContainer = cloudBlobClient.GetContainerReference(containerName);

                var fileName = Path.GetFileName(filePath);
                var cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(fileName);

                cloudBlockBlob.Properties.ContentType = System.Net.Mime.MediaTypeNames.Application.Octet;

                using (var fileStream = File.OpenRead(filePath))
                {
                    cloudBlockBlob.UploadFromStream(fileStream);
                    return cloudBlockBlob.Uri.OriginalString;
                }
            }
            catch (Exception ex)
            {
                log.Error("Azure Blob Upload Error:- ", ex);
                return filePath;
            }
            finally
            {
                File.Delete(filePath);
            }

        }
    }
}
