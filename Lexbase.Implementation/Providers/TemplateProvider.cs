﻿using Lexbase.Providers;
using Lexbase.Providers.Models;
using log4net;
using System;
using System.IO;

namespace Lexbase.Implementation.Providers
{
    public class TemplateProvider : ITemplateProvider
    {
        private readonly ILog log = LogManager.GetLogger(typeof(TemplateProvider));

        private static string _emailHeader;
        private static string _emailFooter;

        public EmailTemplate GetEmailTemplate(string templateName)
        {
            var logContext = $"[GetEmailTemplate:{templateName}]";
            log.Debug($"{logContext} started");
            var subjectTemplate = $"{UlxConfig.EmailTemplates}/{templateName}-Subject.html";
            var bodyTempate = $"{UlxConfig.EmailTemplates}/{templateName}-Body.html";

            if (!File.Exists(subjectTemplate))
            {
                log.Error($"Subject Template Not Found: {subjectTemplate}");
                throw new Exception($"Subject Template Not Found: {subjectTemplate}");
            }

            if (!File.Exists(bodyTempate))
            {
                log.Error($"Body Template Not Found: {bodyTempate}");
                throw new Exception($"Body Template Not Found: {bodyTempate}");
            }

            var subject = File.ReadAllText(subjectTemplate);
            var body = File.ReadAllText(bodyTempate);

            if (body.Contains("{{> _header }}"))
            {
                body = body.Replace("{{> _header }}", EmailHeader);
            }

            if (body.Contains("{{> _footer }}"))
            {
                body = body.Replace("{{> _footer }}", EmailFooter);
            }

            return new EmailTemplate(subject, body);
        }

        private string EmailHeader
        {
            get
            {
                if (_emailHeader == null)
                {
                    _emailHeader = File.ReadAllText($"{UlxConfig.EmailTemplates}/_header.html");
                }

                return _emailHeader;
            }
        }

        private string EmailFooter
        {
            get
            {
                if (_emailFooter == null)
                {
                    _emailFooter = File.ReadAllText($"{UlxConfig.EmailTemplates}/_footer.html");
                }

                return _emailFooter;
            }
        }
    }
}