﻿using Lexbase.Enum;
using Lexbase.Extensions;
using Lexbase.Helpers;
using Lexbase.Models;
using Lexbase.Queries;
using Lexbase.Repositories;
using Lexbase.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Lexbase.Implementation.Services
{
    public class AccountReportService : IAccountReportService
    {
        private readonly IAccountReportRepository accountReportRepository;
        private readonly IUserRepository userRepository;
        private readonly IObligationRepository obligationRepository;
        private readonly IAccountRepository accountRepository;

        public AccountReportService(
            IAccountReportRepository accountReportRepository,
            IUserRepository userRepository,
            IObligationRepository obligationRepository,
            IAccountRepository accountRepository)
        {
            this.accountReportRepository = accountReportRepository;
            this.userRepository = userRepository;
            this.accountRepository = accountRepository;
            this.obligationRepository = obligationRepository;
        }

        public AccountTrendReportFunctional GetAccountTrendFunctional(long accountId)
        {
            var report = new AccountTrendReportFunctional
            {
                List = new Dictionary<ObligationType, IList<AccountTrendData>>()
            };
            var obligationTypes = obligationRepository.GetObligationTypes();
            var groupedData = new Dictionary<long, List<AccountTrendData>>();
            var rows = accountRepository.GetTrendDataForHeatMap(accountId);

            foreach (var item in obligationTypes)
            {
                groupedData[item.Id] = new List<AccountTrendData>();
            }

            foreach (var item in rows)
            {
                if (!groupedData.ContainsKey(item.ObligationTypeId))
                {
                    groupedData[item.ObligationTypeId] = new List<AccountTrendData>();
                }
                groupedData[item.ObligationTypeId].Add(item);
            }

            foreach (var obligationTypeId in groupedData.Keys)
            {
                var obligationType = obligationTypes.FirstOrDefault(i => i.Id == obligationTypeId);

                report.List[obligationType] = new List<AccountTrendData>();

                var result = groupedData[obligationTypeId];
                var months = DateHelper.Last12Months();
                foreach (var item in months)
                {
                    var data = result.FirstOrDefault(x => DateHelper.IsSameMonth(x.StartDate, item));
                    if (data != null)
                    {
                        report.List[obligationType].Add(data);
                    }
                    else
                    {
                        report.List[obligationType].Add(new AccountTrendData
                        {
                            ObligationTypeId = obligationTypeId,
                            StartDate = item,
                            BragRatio = default,
                            BragStatus = default
                        });
                    }
                }
            }

            return report;
        }

        public AccountTrendReportCumulative GetAccountTrendCumulative(long accountId)
        {
            var report = new AccountTrendReportCumulative
            {
                List = new List<AccountTrendData>()
            };
            var groupedData = new Dictionary<long, List<AccountTrendData>>();
            var rows = accountRepository.GetAccountTrendData(accountId);


            var months = DateHelper.Last12Months();
            foreach (var month in months)
            {
                var data = rows.FindAll(x => DateHelper.IsSameMonth(x.StartDate, month));

                decimal? status = null;
                if (data != null && data.Count > 0)
                {
                    data.ForEach(d =>
                    {
                        status = d.BragStatus;
                    });
                }

                report.List.Add(new AccountTrendData
                {
                    StartDate = month,
                    BragRatio = default,
                    BragStatus = status
                });
            }

            return report;
        }


        public HeatMapReport GetHeatMap(long accountId)
        {
            var ownerList = new List<List<Dictionary<string, HeatMapData>>>();

            var data = accountReportRepository.GetHeatMapReport(new HeatMapQuery { AccountId = accountId });

            var cscData = data.Where(x => x.ReportGroup != null && x.ReportGroup == HeatMapReportGroup.CSCHeatMap).ToList();
            var cusData = data.Where(x => x.ReportGroup != null && x.ReportGroup == HeatMapReportGroup.CustomerHeatMap).ToList();

            var openWithPenaltiesData = data.Where(x => x.ReportGroup != null && x.ReportGroup == HeatMapReportGroup.OwnerOpenWithPenalties).ToList();
            var ownerOpenWithoutPenaltiesData = data.Where(x => x.ReportGroup != null && x.ReportGroup == HeatMapReportGroup.OwnerOpenWithoutPenalties).ToList();
            var ownerOverdueWithoutPenaltiesData = data.Where(x => x.ReportGroup != null && x.ReportGroup == HeatMapReportGroup.OwnerOverdueWithoutPenalties).ToList();
            var ownerOverdueWithPenaltiesData = data.Where(x => x.ReportGroup != null && x.ReportGroup == HeatMapReportGroup.OwnerOverdueWithPenalties).ToList();

            var cscList = GetData(cscData, HeatMapListType.Csc);
            var cusList = GetData(cusData, HeatMapListType.Cus);
            ownerList.Add(GetDataOwner(openWithPenaltiesData, HeatMapReportTypes.OpenWithPenalties, HeatMapReportGroup.OwnerOpenWithPenalties));
            ownerList.Add(GetDataOwner(ownerOpenWithoutPenaltiesData, HeatMapReportTypes.OpenWithoutPenalties, HeatMapReportGroup.OwnerOpenWithoutPenalties));
            ownerList.Add(GetDataOwner(ownerOverdueWithoutPenaltiesData, HeatMapReportTypes.OverdueWithoutPenalties, HeatMapReportGroup.OwnerOverdueWithoutPenalties));
            ownerList.Add(GetDataOwner(ownerOverdueWithPenaltiesData, HeatMapReportTypes.OverdueWithPenalties, HeatMapReportGroup.OwnerOverdueWithPenalties));


            var dueInNextThirtyDays = GetChartDueNextThirtyDayData(accountId);
            var dueInNext90Days = GetChartDueNext90DayData(accountId);

            #region Trend data
            var rows = accountRepository.GetTrendDataForHeatMap(accountId);
            var trendList = GetTrendData(rows);
            #endregion
            #region HighRisk
            var highriskdata = accountReportRepository.GetHighRiskForHeatMap(accountId);
            var highriskList = GetDataHighRisk(highriskdata);
            #endregion


            return new HeatMapReport
            {
                HighRiskData = highriskList,
                TrendData = trendList,
                CSCs = cscList,
                CUSs = cusList,
                Owners = ownerList,
                DueIn30Days = dueInNextThirtyDays,
                DueIn90Days = dueInNext90Days
            };
        }
        private List<Dictionary<string, HeatMapData>> GetTrendData(List<HeatMapData> data)
        {
            var list = new List<Dictionary<string, HeatMapData>>();

            var lastYearMonths = new Dictionary<string, string>();
            var values = new Dictionary<string, HeatMapData>();
            values.Add("Overall Account Status for obligations due in each month", new HeatMapData());
            values.Add(" ", new HeatMapData());
            values.Add("Current Month", new HeatMapData());
            for (var i = 1; i < 13; i++)
            {
                lastYearMonths.Add(DateTime.Now.AddMonths(-i).ToString("MMyyyy"), DateTime.Now.AddMonths(-i).ToString("MMM yyyy"));
            }

            foreach (var item in lastYearMonths)
            {
                var dataValue = data.FirstOrDefault(x => x.StartDate.ToString() == item.Key);
                if (dataValue != null)
                {
                    values.Add(item.Value, dataValue);
                }
                else
                {
                    values.Add(item.Value, new HeatMapData());
                }
            }
            list.Add(values);
            return list;
        }
        private List<Dictionary<string, HeatMapData>> GetDataHighRisk(List<HeatMapData> data)
        {
            var list = new List<Dictionary<string, HeatMapData>>();

            var values = new Dictionary<string, HeatMapData>();
            var highRiskResult = data.ToList();
            if (highRiskResult.Count() <= 0)
            {
                highRiskResult.Add(new HeatMapData { HighriskId = 0, Description = "No Record Found", Penalty = "", Duedate = "", DaystillDue = "", Owner = new User { Id = 0 } });
            }

            values.Add("High Risk Items Description", new HeatMapData());
            values.Add("Penalty", new HeatMapData());
            values.Add("Due Date", new HeatMapData());
            values.Add("Days till Due", new HeatMapData());
            //values.Add("Owner", new HeatMapData());
            foreach (var obj in highRiskResult)
            {
                var user = userRepository.Get(obj.Owner.Id);
                var Obj = highRiskResult.FirstOrDefault(x => x.Description == obj.Description);
                if (Obj == null)
                {
                    //values.Add("OWNER", new HeatMapData());
                }
                else
                {
                    //values.Add("OWNER", Obj);
                }
                list.Add(values);
            }
            //list.Add(highRiskResult);
            return list;

        }
        private Dictionary<string, HeatMapData> GetChartDueNextThirtyDayData(long accountId)
        {
            var values = new Dictionary<string, HeatMapData>();
            var frequencyTypes = System.Enum.GetValues(typeof(FrequencyTypes)).Cast<FrequencyTypes>();
            foreach (var frequencyType in frequencyTypes)
            {
                var query = new HeatMapQuery
                {
                    AccountId = accountId,
                    FrequencyType = (int)frequencyType
                };
                var count = accountReportRepository.GetHeatMapChartData(query).Count();
                values.Add(frequencyType.GetDescription(), new HeatMapData
                {
                    Id = "0", // TODO 
                    Count = count
                });
            }
            return values;
        }

        private Dictionary<string, HeatMapData> GetChartDueNext90DayData(long accountId)
        {
            var values = new Dictionary<string, HeatMapData>();
            var types = System.Enum.GetValues(typeof(HeatMapStatusType)).Cast<HeatMapStatusType>();
            foreach (var type in types)
            {
                var query = new HeatMapQuery
                {
                    AccountId = accountId,
                    StatusType = (int)type
                };
                var count = accountReportRepository.GetHeatMap90ChartData(query).Count();
                values.Add(type.GetDescription(), new HeatMapData
                {
                    Id = "0", // TODO 
                    Count = count
                });
            }
            return values;
        }

        private List<Dictionary<string, HeatMapData>> GetData(List<HeatMapData> data, HeatMapListType dataType)
        {
            var list = new List<Dictionary<string, HeatMapData>>();

            var reportTypes = System.Enum.GetValues(typeof(HeatMapReportTypes)).Cast<HeatMapReportTypes>();
            var durationTypes = System.Enum.GetValues(typeof(HeatMapDurationTypes)).Cast<HeatMapDurationTypes>();

            foreach (var retportType in reportTypes)
            {
                var lastYearMonths = new Dictionary<string, string>();
                var values = new Dictionary<string, HeatMapData>
                {
                    {
                        dataType.GetDescription(),
                        new HeatMapData
                        {
                            Header = retportType.GetDescription()
                        }
                    }
                };
                var tempDurationTypes = durationTypes.Where(x => x.ToString() != "Older");
                foreach (var durationType in tempDurationTypes)
                {
                    var result = data.FirstOrDefault(x => x.Type == retportType && x.Period == durationType.ToString());
                    if (result != null)
                    {
                        values.Add(durationType.GetDescription(), result);
                    }
                    else
                    {
                        values.Add(durationType.GetDescription(), new HeatMapData());
                    }
                }
                for (var i = 1; i < 13; i++)
                {
                    lastYearMonths.Add(DateTime.Now.AddMonths(-i).ToString("MMyyyy"), DateTime.Now.AddMonths(-i).ToString("MMM yyyy"));
                }
                var hisResult = data.Where(x => x.Type == retportType && x.Period != HeatMapDurationTypes.DueFivedays.ToString() && x.Period != HeatMapDurationTypes.Total.ToString());
                foreach (var item in lastYearMonths)
                {
                    var dataValue = hisResult.FirstOrDefault(x => x.Period == item.Key);
                    if (dataValue != null)
                    {
                        values.Add(item.Value, dataValue);
                    }
                    else
                    {
                        values.Add(item.Value, new HeatMapData());
                    }
                }
                var resultOlder = data.FirstOrDefault(x => x.Type == retportType && x.Period == HeatMapDurationTypes.Older.ToString());
                if (resultOlder != null)
                {
                    values.Add(HeatMapDurationTypes.Older.ToString(), resultOlder);
                }
                else
                {
                    values.Add(HeatMapDurationTypes.Older.ToString(), new HeatMapData());
                }
                list.Add(values);
            }

            return list;
        }


        private List<Dictionary<string, HeatMapData>> GetDataOwner(List<HeatMapData> data, HeatMapReportTypes reportType, HeatMapReportGroup type)
        {
            var list = new List<Dictionary<string, HeatMapData>>();
            var durationTypes = System.Enum.GetValues(typeof(HeatMapDurationTypes)).Cast<HeatMapDurationTypes>();

            var lastYearMonths = new Dictionary<string, string>();
            var values = new Dictionary<string, HeatMapData>();

            var durationTypeDueResult = data.Where(x => x.Type == reportType && x.Period == HeatMapDurationTypes.DueFivedays.ToString()).ToList();
            var durationTypeTotalResult = data.Where(x => x.Type == reportType && x.Period == HeatMapDurationTypes.Total.ToString()).ToList();
            var durationTypeOldTotalResult = data.Where(x => x.Type == reportType && x.Period == HeatMapDurationTypes.OldTotal.ToString()).ToList();
            var durationTypeOlderResult = data.Where(x => x.Type == reportType && x.Period == HeatMapDurationTypes.Older.ToString()).ToList();
            if (durationTypeDueResult.Count() <= 0)
            {
                durationTypeDueResult.Add(new HeatMapData { ReportGroup = type, Type = reportType, Period = "Due <5 days", Count = 0, Owner = new User { Id = 0 } });
            }
            if (durationTypeTotalResult.Count() <= 0)
            {
                durationTypeTotalResult.Add(new HeatMapData { ReportGroup = type, Type = reportType, Period = "Total", Count = 0, Owner = new User { Id = 0 } });
            }
            if (durationTypeOldTotalResult.Count() <= 0)
            {
                durationTypeOldTotalResult.Add(new HeatMapData { ReportGroup = type, Type = reportType, Period = "Totals", Count = 0, Owner = new User { Id = 0 } });
            }
            if (durationTypeOlderResult.Count() <= 0)
            {
                durationTypeOlderResult.Add(new HeatMapData { ReportGroup = type, Type = reportType, Period = "Older", Count = 0, Owner = new User { Id = 0 } });
            }

            foreach (var obj in durationTypeTotalResult)
            {
                var user = userRepository.Get(obj.Owner.Id);

                values.Add(reportType.GetDescription(), new HeatMapData
                {
                    Header = user != null ? user.FullName.ToString() : "No Record Found"
                });

                var dueObj = durationTypeDueResult.FirstOrDefault(x => x.Owner == obj.Owner);

                values.Add("Totals", obj);
                if (dueObj == null)
                {
                    values.Add("Due <5 days", new HeatMapData());
                }
                else
                {
                    values.Add("Due <5 days", dueObj);
                }
                //values.Add("Due < 5 days", dueObj);
                values.Add("Total", obj);

                for (var i = 0; i < 12; i++)
                {
                    lastYearMonths.Add(DateTime.Now.AddMonths(-i).ToString("MMyyyy"), DateTime.Now.AddMonths(-i).ToString("MMM yyyy"));
                }


                var hisResult = data.Where(x => x.Type == reportType && x.Period != HeatMapDurationTypes.OldTotal.ToString() && x.Period != HeatMapDurationTypes.Older.ToString() && x.Period != HeatMapDurationTypes.DueFivedays.ToString() && x.Period != HeatMapDurationTypes.Total.ToString()).ToList();
                foreach (var item in lastYearMonths)
                {
                    var dataValue = hisResult.FirstOrDefault(x => x.Period == item.Key && x.Owner == obj.Owner);
                    if (dataValue != null)
                    {
                        values.Add(item.Value, dataValue);
                    }
                    else
                    {
                        values.Add(item.Value, new HeatMapData());
                    }
                }
                var olderObj = durationTypeOlderResult.FirstOrDefault(x => x.Owner == obj.Owner);
                if (olderObj == null)
                {
                    values.Add("Olders", new HeatMapData());
                }
                else
                {
                    values.Add("Older", olderObj);
                }
                list.Add(values);
            }

            return list;
        }

    }
}