﻿using Lexbase.Providers;
using Lexbase.Services;
using log4net;

namespace Lexbase.Implementation.Services
{
    public class IndexingService : IIndexingService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(IndexingService));
        private readonly IIndexProvider _indexProvider;
        public IndexingService(IIndexProvider indexProvider)
        {
            _indexProvider = indexProvider;
        }
        public bool ReIndex(string name)
        {
            var logContext = $"[Send:{name}]";
            log.Debug($"{logContext} Started");
            _indexProvider.ReIndex(name);
            return true;
        }
    }
}
