﻿using Lexbase.Helpers;
using Lexbase.Models;
using Lexbase.Providers;
using Lexbase.Providers.Models;
using Lexbase.Repositories;
using Lexbase.Services;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Lexbase.Implementation.Services
{
    public class ExcelService : IExcelService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ExcelService));
        private readonly IExcelProvider excelProvider;
        private readonly IObligationRepository obligationRepository;
        private readonly IAccountRepository accountRepository;
        private readonly ISARepository saRepository;
        private readonly IMSARepository msaRepository;
        private readonly IUserRepository userRepository;

        public ExcelService(
            IExcelProvider excelProvider,
            IObligationRepository obligationRepository,
            IAccountRepository accountRepository,
            ISARepository saRepository,
            IMSARepository msaRepository,
            IUserRepository userRepository
        )
        {
            this.excelProvider = excelProvider;
            this.obligationRepository = obligationRepository;
            this.accountRepository = accountRepository;
            this.saRepository = saRepository;
            this.msaRepository = msaRepository;
            this.userRepository = userRepository;
        }

        public MessageAttachment Create(AccountDump dump, string name)
        {
            var logContext = $"[Create: AccountDump]";
            log.Debug($"{logContext} Started");
            var fileName = UlxConfig.Get($"Excels:{name}:Name");
            var account = dump.Account;
            var excel = excelProvider.CreateExcel(fileName);

            try
            {
                #region accountSheet

                var accountSheet = excel.CreateSheet(UlxConfig.Get($"Excels:{name}:Sheets:Account", "Account"));

                accountSheet.AddHeaderColumns(new List<string> {
                        "ID",
                        "Name",
                        "Type",
                        "Coverage",
                        "TimeZone",
                        "Region",
                    }, 0);

                var accountValues = new List<string>();
                var accountData = ExtractColumns(account);

                accountValues.Add(accountData["account.Id"]);
                accountValues.Add(accountData["account.Name"]);
                accountValues.Add(accountData["account.Type"]);
                accountValues.Add(accountData["account.Coverage"]);
                accountValues.Add(accountData["account.TimeZone"]);
                accountValues.Add(accountData["account.Region"]);
                accountSheet.AddDataRow(accountValues, 1);

                #endregion accountSheet

                #region accountSheet

                var sheet = excel.CreateSheet(UlxConfig.Get($"Excels:{name}:Sheets:Documents", "Documents"));
                var rowNo = 0;
                sheet.AddHeaderColumns(new List<string> {
                        "ID",
                        "Name",
                        "Type"
                    }, rowNo);
                foreach (var item in dump.Documents)
                {
                    var values = new List<string>();
                    var data = ExtractColumns(item);

                    values.Add(data["document.Id"]);
                    values.Add(data["document.Name"]);
                    values.Add(data["document.Type"]);
                    sheet.AddDataRow(values, ++rowNo);
                }

                #endregion accountSheet

                #region obligationSheet

                var obligationSheet = excel.CreateSheet(UlxConfig.Get($"Excels:{name}:Sheets:Obligations", "Obligations"));
                rowNo = 0;
                obligationSheet.AddHeaderColumns(new List<string> {
                        "Obligation ID",
                        "Obligation Topic & Description",
                        "Account Name",
                        "Document Name",
                        "Obligation Functional Group",
                        "Obligation Owner",
                        "Escalation Owner",
                        "Frequency",
                        "Initial Due Date",
                        "Triggered",
                        "Obligation Management Type",
                        "Responsibility",
                        "Financial Impact",
                        "Phase",
                    }, rowNo);
                foreach (var item in dump.Obligations)
                {
                    var values = new List<string>();
                    var data = ExtractColumns(account);
                    data = ExtractColumns(item, data);

                    values.Add(data["obligation.Id"]);
                    values.Add(data["obligation.Name"]);
                    values.Add(data["account.Name"]);
                    values.Add(data["document.Name"]);
                    values.Add(data["obligation.Type.Name"]);
                    values.Add(data["obligation.Owner.FullName"]);
                    values.Add(data["obligation.Manager.FullName"]);
                    values.Add(data["obligation.Frequency.Name"]);
                    values.Add(data["obligation.StartDate"]);
                    values.Add(data["obligation.IsTrigger"]);
                    values.Add(data["obligation.ActionType"]);
                    values.Add(data["obligation.Responsibility.Name"]);
                    values.Add(data["obligation.IsFinancial"]);
                    values.Add(data["obligation.Phase.Name"]);
                    obligationSheet.AddDataRow(values, ++rowNo);
                }

                #endregion obligationSheet

                //#region child Obligation

                //var childObligationSheet = excel.CreateSheet(UlxConfig.Get($"Excels:{name}:Sheets:ChildObligations", "ChildObligations"));
                //rowNo = 0;
                //childObligationSheet.AddHeaderColumns(new List<string>
                //    {
                //        "Obligation ID",
                //        "Date",
                //        "Obligation Topic & Description",
                //        "Account Name",
                //        "Document Name",
                //        "Obligation Functional Group",
                //        "Obligation Owner",
                //        "Escalation Owner",
                //        "Frequency",
                //        "Triggered",
                //        "Obligation Management Type",
                //        "Responsibility",
                //        "Financial Impact",
                //        "Phase",
                //    }, rowNo);
                //foreach (var item in dump.ChildObligations)
                //{
                //    var data = ExtractColumns(item);
                //    data = ExtractColumns(item.Parent, data);
                //    data = ExtractColumns(account, data);

                //    var values = new List<String>();

                //    values.Add(data["childObligation.Id"]);
                //    values.Add(data["childObligation.Date"]);

                //    values.Add(data["obligation.Name"]);
                //    values.Add(data["account.Name"]);
                //    values.Add(data["document.Name"]);
                //    values.Add(data["obligation.Type.Name"]);
                //    values.Add(data["obligation.Owner.FullName"]);
                //    values.Add(data["obligation.Manager.FullName"]);
                //    values.Add(data["obligation.Frequency.Name"]);
                //    // values.Add(data["obligation.StartDate"]);
                //    values.Add(data["obligation.IsTrigger"]);
                //    values.Add(data["obligation.ActionType"]);
                //    // values.Add(data["obligation.SubType.Name"]);
                //    values.Add(data["obligation.Responsibility.Name"]);
                //    values.Add(data["obligation.IsFinancial"]);
                //    values.Add(data["obligation.Phase.Name"]);

                //    childObligationSheet.AddDataRow(values, ++rowNo);
                //}

                //#endregion child Obligation
            }
            catch (Exception ex)
            {
                log.Error($"{logContext}", ex);
            }

            log.Debug($"{logContext} Done");

            return new MessageAttachment
            {
                Path = excel.Save(),
                FileName = fileName
            };
        }

        public MessageAttachment Create(Account account, IDictionary<string, IList<Obligation>> obligationListMap, string name)
        {
            var logContext = $"[Create: obligationListMap]";
            log.Debug($"{logContext} Started");
            var fileName = UlxConfig.Get($"Excels:{name}:Name");
            var excel = excelProvider.CreateExcel(fileName);

            foreach (var sheetName in obligationListMap.Keys)
            {
                var obList = obligationListMap[sheetName];

                try
                {
                    var sheet = excel.CreateSheet(UlxConfig.Get($"Excels:{name}:Sheets:{sheetName}", sheetName));
                    sheet.AddMergeRegion(0, 1, 0, 14);
                    sheet.AddHeaderRow(account.Name);

                    var rowNo = 4;

                    IList<string> headers = new List<String> {
                        "Obligation ID",
                        "Obligation Topic & Description",
                        "Account Name",
                        "Document Name",
                        "Obligation Functional Group",
                        "Obligation Owner",
                        "Escalation Owner",
                        "Frequency",
                        "Initial Due Date",
                        "Triggered",
                        "Obligation Management Type",
                        // "Obligation Type",
                        "Responsibility",
                        "Financial Impact",
                        "Phase",
                    };

                    sheet.AddHeaderColumns(headers, rowNo);

                    foreach (var obligation in obList)
                    {
                        var values = new List<string>();
                        var data = ExtractColumns(obligation.Account);
                        data = ExtractColumns(obligation, data);

                        values.Add(data["obligation.Id"]);
                        values.Add(data["obligation.Name"]);
                        values.Add(data["account.Name"]);
                        values.Add(data["document.Name"]);
                        values.Add(data["obligation.Type.Name"]);
                        values.Add(data["obligation.Owner.FullName"]);
                        values.Add(data["obligation.Manager.FullName"]);
                        values.Add(data["obligation.Frequency.Name"]);
                        values.Add(data["obligation.StartDate"]);
                        values.Add(data["obligation.IsTrigger"]);
                        values.Add(data["obligation.ActionType"]);
                        // values.Add(data["obligation.SubType.Name"]);
                        values.Add(data["obligation.Responsibility.Name"]);
                        values.Add(data["obligation.IsFinancial"]);
                        values.Add(data["obligation.Phase.Name"]);
                        sheet.AddDataRow(values, ++rowNo);
                    }
                }
                catch (Exception ex)
                {
                    log.Error($"{logContext}", ex);
                }
            }

            log.Debug($"{logContext} Done");

            return new MessageAttachment
            {
                Path = excel.Save(),
                FileName = fileName
            };
        }

        public MessageAttachment Create(Dictionary<string, IList<ObligationTriggerLog>> maps, string name)
        {
            var logContext = $"[Create: obligationListMap]";
            log.Debug($"{logContext} Started");
            var fileName = UlxConfig.Get($"Excels:{name}:Name");
            var excel = excelProvider.CreateExcel(fileName);

            foreach (var sheetName in maps.Keys)
            {
                var items = maps[sheetName];

                try
                {
                    var sheet = excel.CreateSheet(UlxConfig.Get($"Excels:{name}:Sheets:{sheetName}", sheetName));
                    //sheet.AddMergeRegion(0, 1, 0, 14);
                    //sheet.AddHeaderRow();

                    var rowNo = 1;

                    var headers = new List<String>
                    {
                        "Obligation ID",
                        "Due Date",
                        "Obligation Topic & Description",
                        "Account Name",
                        "Document Name",
                        "Obligation Functional Group",
                        "Obligation Owner",
                        "Escalation Owner",
                        "Frequency",
                        // "Initial Due Date",
                        "Triggered",
                        "Obligation Management Type",
                        // "Obligation Type",
                        "Responsibility",
                        "Financial Impact",
                        "Phase",
                    };

                    sheet.AddHeaderColumns(headers, rowNo);
                    foreach (var item in items)
                    {
                        var data = ExtractColumns(item.ChildObligation);
                        data = ExtractColumns(item.ChildObligation.Parent, data);
                        data = ExtractColumns(item.ChildObligation.Parent.Account, data);

                        var values = new List<String>();

                        values.Add(data["childObligation.Id"]);
                        values.Add(data["childObligation.Date"]);

                        values.Add(data["obligation.Name"]);
                        values.Add(data["account.Name"]);
                        values.Add(data["document.Name"]);
                        values.Add(data["obligation.Type.Name"]);
                        values.Add(data["obligation.Owner.FullName"]);
                        values.Add(data["obligation.Manager.FullName"]);
                        values.Add(data["obligation.Frequency.Name"]);
                        // values.Add(data["obligation.StartDate"]);
                        values.Add(data["obligation.IsTrigger"]);
                        values.Add(data["obligation.ActionType"]);
                        // values.Add(data["obligation.SubType.Name"]);
                        values.Add(data["obligation.Responsibility.Name"]);
                        values.Add(data["obligation.IsFinancial"]);
                        values.Add(data["obligation.Phase.Name"]);

                        sheet.AddDataRow(values, ++rowNo);
                    }
                }
                catch (Exception ex)
                {
                    log.Error($"{logContext}", ex);
                }
            }

            log.Debug($"{logContext} Done");

            return new MessageAttachment
            {
                Path = excel.Save(),
                FileName = fileName
            };
        }

        public MessageAttachment Create(IDictionary<string, IList<ChildObligation>> maps, string name)
        {
            var logContext = $"[Create: obligationListMap]";
            log.Debug($"{logContext} Started");
            var fileName = UlxConfig.Get($"Excels:{name}:Name");
            var excel = excelProvider.CreateExcel(fileName);

            foreach (var sheetName in maps.Keys)
            {
                var items = maps[sheetName];

                try
                {
                    var sheet = excel.CreateSheet(UlxConfig.Get($"Excels:{name}:Sheets:{sheetName}", sheetName));

                    var rowNo = 1;

                    var headers = new List<String>
                    {
                        "Obligation ID",
                        "Due Date",
                        "Obligation Topic & Description",
                        "Account Name",
                        "Document Name",
                        "Obligation Functional Group",
                        "Obligation Owner",
                        "Escalation Owner",
                        "Frequency",
                        // "Initial Due Date",
                        "Triggered",
                        "Obligation Management Type",
                        // "Obligation Type",
                        "Responsibility",
                        "Financial Impact",
                        "Phase",
                    };

                    sheet.AddHeaderColumns(headers, rowNo);
                    foreach (var item in items)
                    {
                        var data = ExtractColumns(item);
                        data = ExtractColumns(item.Parent, data);
                        data = ExtractColumns(item.Parent.Account, data);

                        var values = new List<String>();

                        values.Add(data["childObligation.Id"]);
                        values.Add(data["childObligation.Date"]);

                        values.Add(data["obligation.Name"]);
                        values.Add(data["account.Name"]);
                        values.Add(data["document.Name"]);
                        values.Add(data["obligation.Type.Name"]);
                        values.Add(data["obligation.Owner.FullName"]);
                        values.Add(data["obligation.Manager.FullName"]);
                        values.Add(data["obligation.Frequency.Name"]);
                        // values.Add(data["obligation.StartDate"]);
                        values.Add(data["obligation.IsTrigger"]);
                        values.Add(data["obligation.ActionType"]);
                        // values.Add(data["obligation.SubType.Name"]);
                        values.Add(data["obligation.Responsibility.Name"]);
                        values.Add(data["obligation.IsFinancial"]);
                        values.Add(data["obligation.Phase.Name"]);

                        sheet.AddDataRow(values, ++rowNo);
                    }
                }
                catch (Exception ex)
                {
                    log.Error($"{logContext}", ex);
                }
            }

            log.Debug($"{logContext} Done");

            return new MessageAttachment
            {
                Path = excel.Save(),
                FileName = fileName
            };
        }

        public MessageAttachment Create(IDictionary<string, HeatMapReport> maps, string name)
        {
            var logContext = $"[Create: HeatMapReport]";
            log.Debug($"{logContext} Started");
            var fileName = UlxConfig.Get($"Excels:{name}:Name");
            var excel = excelProvider.CreateExcel(fileName);
            try
            {
                foreach (var sheetName in maps.Keys)
                {
                    var sheet = excel.CreateSheet(UlxConfig.Get($"Excels:{name}:Sheets:sheet1", "sheet1"));
                    var sName = excel.Workbook.GetSheetName(0);
                    excel.Workbook.SetSheetName(0, sheetName);
                    var heatMapReport = maps[sheetName];
                    var account = heatMapReport.Account;
                    var rowNo = 0;

                    sheet.AddHeaderColumn("Account Heat Map Report _ " + account.Name, rowNo, 1);
                    rowNo += 3;

                    sheet.AddHeaderColumn(DateHelper.ToDateTimeString(DateTime.Now), rowNo, 5);
                    rowNo += 2;

                    #region High Risk data

                    sheet.AddHeaderColumn("High Risk Items Description", rowNo, 1);
                    sheet.AddHeaderColumn("Penalty", rowNo, 4);
                    sheet.AddHeaderColumn("Due Date", rowNo, 5);
                    sheet.AddHeaderColumn("Days till Due", rowNo, 8);
                    sheet.AddHeaderColumn("Owner", rowNo, 11);

                    rowNo += 1;

                    foreach (var listItem in heatMapReport.HighRiskData)
                    {
                        var data = ExtractColumns(listItem);
                        sheet.AddHeaderColumn(data["High Risk Items Description"], rowNo, 1);
                        sheet.AddHeaderColumn(data["Penalty"], rowNo, 4);
                        sheet.AddHeaderColumn(data["Due Date"], rowNo, 5);
                        sheet.AddHeaderColumn(data["Days till Due"], rowNo, 8);
                        //sheet.AddHeaderColumn(data["Owner"], rowNo, 11);
                        //                ++rowNo;
                    }
                    rowNo += 2;

                    #endregion

                    #region TrendData

                    var TrendHeaders = new Dictionary<string, int>();
                    var index = 0;
                    foreach (var obj in heatMapReport.TrendData[0])
                    {
                        index += 1;
                        sheet.AddHeaderColumn(obj.Key, rowNo, index);
                        TrendHeaders.Add(obj.Key, index);
                        switch (index)
                        {
                            case 1:
                                index += 2;
                                break;
                            case 5:
                                index += 1;
                                break;
                            case 9:
                                index += 1;
                                break;
                            case 11:
                                index += 1;
                                break;
                        }
                    }
                    rowNo += 1;
                    foreach (var listItem in heatMapReport.TrendData)
                    {
                        //var values = new List<string>();

                        var data = ExtractColumns(listItem);
                        foreach (var item in TrendHeaders)
                        {
                            sheet.AddHeaderColumn(data[item.Key], rowNo, item.Value);
                        }

                        rowNo += 2;
                    }
                    #endregion

                    #region CSC List

                    var headers = new Dictionary<string, int>();
                    index = 0;
                    foreach (var obj in heatMapReport.CSCs[0])
                    {
                        index += 1;
                        sheet.AddHeaderColumn(obj.Key, rowNo, index);
                        headers.Add(obj.Key, index);
                        switch (index)
                        {
                            case 1:
                                index += 2;
                                break;
                            case 9:
                                index += 1;
                                break;
                            case 11:
                                index += 1;
                                break;
                        }
                    }
                    rowNo += 1;
                    foreach (var item in heatMapReport.CSCs)
                    {
                        var data = ExtractColumns(item);
                        foreach (var header in headers)
                        {
                            sheet.AddHeaderColumn(data[header.Key], rowNo, header.Value);
                        }
                        rowNo += 1;
                    }
                    rowNo += 3;
                    #endregion

                    #region Due in Next 30 Days

                    foreach (var item in heatMapReport.DueIn30Days)
                    {
                        sheet.AddHeaderColumn(item.Key, rowNo, 1);
                        sheet.AddHeaderColumn($"[{item.Value.Count}|{LinkHelper.GetHeatMapDataLink(item.Value)}]", rowNo, 4);
                        rowNo += 1;
                    }
                    rowNo += 4;
                    #endregion

                    #region Due in the last  90 days

                    foreach (var item in heatMapReport.DueIn90Days)
                    {
                        sheet.AddHeaderColumn(item.Key, rowNo, 1);
                        sheet.AddHeaderColumn($"[{item.Value.Count}|{LinkHelper.GetHeatMapDataLink(item.Value)}]", rowNo, 4);
                        rowNo += 1;
                    }
                    rowNo += 2;
                    #endregion

                    #region CSU List
                    var cusHeaders = new Dictionary<string, int>();
                    index = 0;
                    foreach (var obj in heatMapReport.CUSs[0])
                    {
                        index += 1;
                        sheet.AddHeaderColumn(obj.Key, rowNo, index);
                        cusHeaders.Add(obj.Key, index);
                        switch (index)
                        {
                            case 1:
                                index += 2;
                                break;
                            case 9:
                                index += 1;
                                break;
                            case 11:
                                index += 1;
                                break;
                        }
                    }
                    rowNo += 1;
                    foreach (var item in heatMapReport.CUSs)
                    {
                        var data = ExtractColumns(item);
                        foreach (var header in cusHeaders)
                        {
                            sheet.AddHeaderColumn(data[header.Key], rowNo, header.Value);
                        }
                        rowNo += 1;
                    }
                    rowNo += 2;
                    #endregion

                    #region Obligation Owner Status (Top Five)

                    foreach (var listItem in heatMapReport.Owners)
                    {
                        var ownerHeaders = new Dictionary<string, int>();
                        index = 0;
                        foreach (var obj in listItem[0])
                        {
                            index += 1;
                            sheet.AddHeaderColumn(obj.Key, rowNo, index);
                            ownerHeaders.Add(obj.Key, index);
                            switch (index)
                            {
                                case 1:
                                    index += 2;
                                    break;
                                case 9:
                                    index += 1;
                                    break;
                                case 11:
                                    index += 1;
                                    break;
                            }
                        }
                        rowNo += 1;
                        foreach (var item in listItem)
                        {
                            var data = ExtractColumns(item);
                            foreach (var header in ownerHeaders)
                            {
                                sheet.AddHeaderColumn(data[header.Key], rowNo, header.Value);
                            }
                            rowNo += 1;
                        }
                        rowNo += 1;
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                log.Error($"{logContext}", ex);
            }
            log.Debug($"{logContext} Done");

            return new MessageAttachment
            {
                Path = excel.Save(),
                FileName = fileName
            };
        }

        public MessageAttachment Create(IDictionary<string, AccountTrendReportFunctional> maps, string name)
        {
            var logContext = $"[Create: AccountTrendReportFunctional]";
            log.Debug($"{logContext} Started");
            var fileName = UlxConfig.Get($"Excels:{name}:Name");
            var excel = excelProvider.CreateExcel(fileName);
            try
            {
                foreach (var sheetName in maps.Keys)
                {
                    var sheet = excel.CreateSheet(UlxConfig.Get($"Excels:{name}:Sheets:{sheetName}", sheetName));

                    var trendReport = maps[sheetName];

                    sheet.AddMergeRegion(0, 1, 0, 14);
                    sheet.AddHeaderRow("Account Status Trend Report Functional");

                    sheet.AddDataRowColumn("Report Generated On:" + DateHelper.ToDateString(DateTime.Now), 4);

                    var rowNo = 5;

                    var accountData = ExtractColumns(trendReport.Account);

                    var headersAdded = false;
                    foreach (var obligationType in trendReport.List.Keys)
                    {
                        var monthlyData = trendReport.List[obligationType];

                        if (!headersAdded)
                        {
                            IList<string> headers = new List<string>();
                            headers.Add("Obligation Type");

                            foreach (var data in monthlyData)
                            {
                                headers.Add(DateHelper.ToMonthString(data.StartDate));
                            }

                            sheet.AddHeaderColumns(headers, rowNo);

                            headersAdded = true;
                        }

                        var values = new List<string>();
                        values.Add(obligationType.Name);

                        foreach (var data in monthlyData)
                        {
                            values.Add($"[{ data.BragRatio.ToString()}|style:{UlxConfig.Get($"StatusColor:{data.BragStatus}", "White")}BG]");
                        }

                        sheet.AddDataRow(values, ++rowNo);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error($"{logContext}", ex);
            }

            return new MessageAttachment
            {
                Path = excel.Save(),
                FileName = fileName
            };
        }

        public MessageAttachment Create(IDictionary<string, IList<AccountInformationData>> maps, string name, string LeadCCM)
        {
            var logContext = $"[Create: AccountInformationReport]";
            log.Debug($"{logContext} Started");
            var fileName = UlxConfig.Get($"Excels:{name}:Name");
            var excel = excelProvider.CreateExcel(fileName);
            IList<AccountInformationData> items = new List<AccountInformationData>();
            try
            {
                foreach (var keyValue in maps.Keys)
                {
                    var table = maps[keyValue];
                    foreach (var informationReport in table)
                    {
                        informationReport.LeadCCM = LeadCCM;
                        items.Add(informationReport);
                    }
                }
                var rowNo = 5;
                var headersAdded = false;
                var sheet = excel.CreateSheet(UlxConfig.Get($"Excels:{name}:Sheets:AccountsInformation", "AccountsInformation"));
                sheet.AddMergeRegion(0, 1, 0, 14);
                sheet.AddHeaderRow("Account Information Report");
                sheet.AddMergeRegion(2, 2, 0, 1);
                sheet.AddDataRowColumn("Report Generated On:" + DateTime.UtcNow.ToString("ddd, dd MMM yyy HH:mm:ss "), 2);
                foreach (var informationReport in items)
                {
                    if (!headersAdded)
                    {
                        IList<string> headers = new List<string>();
                        headers.Add("Account Name");
                        headers.Add("Account Legal Name");
                        headers.Add("Account Type");
                        headers.Add("Account Status");
                        headers.Add("Region");
                        headers.Add("Total Contract Value");
                        headers.Add("Lead Country");
                        headers.Add("Delivery Countries");
                        headers.Add("Account in Transition");
                        headers.Add("Account Address");
                        headers.Add("Active");
                        headers.Add("Creation Date");
                        headers.Add("Lead CCM");
                        headers.Add("Account Classification");
                        headers.Add("Conformed Till Date");
                        sheet.AddHeaderColumns(headers, rowNo);
                        headersAdded = true;
                    }
                    var values = new List<string>
                        {
                            informationReport.Name,
                            informationReport.LegelName,
                            informationReport.AccountType,
                            ($"[|style:{UlxConfig.Get($"StatusColor:{informationReport.Status}", "White")}BG]"),
                            informationReport.RegionName,
                            informationReport.AccountValue,
                            informationReport.LeadCountryName,
                            informationReport.DeliveryCountryName,
                            informationReport.InTransition,
                            informationReport.Address,
                            informationReport.IsActive,
                            informationReport.AccountDateCreated,
                            informationReport.LeadCCM,
                            informationReport.AccountClassification,
                            informationReport.AccountDateConfirmed
                        };
                    sheet.AddDataRow(values, ++rowNo);
                }
            }
            catch (Exception ex)
            {
                log.Error($"{logContext}", ex);
            }
            return new MessageAttachment
            {
                Path = excel.Save(),
                FileName = fileName
            };
        }
        public MessageAttachment Create(IDictionary<string, IList<AccountTrendReportCumulative>> maps, string name)
        {
            var logContext = $"[Create: AccountTrendReportFunctional]";
            log.Debug($"{logContext} Started");
            var fileName = UlxConfig.Get($"Excels:{name}:Name");
            var excel = excelProvider.CreateExcel(fileName);
            try
            {
                foreach (var sheetName in maps.Keys)
                {
                    var sheet = excel.CreateSheet(UlxConfig.Get($"Excels:{name}:Sheets:{sheetName}", sheetName));

                    var items = maps[sheetName];

                    sheet.AddMergeRegion(0, 1, 0, 14);
                    sheet.AddHeaderRow("Account Status Trend Report");

                    sheet.AddDataRowColumn("Report Generated On:" + DateHelper.ToDateString(DateTime.Now), 4);

                    var rowNo = 5;

                    var headersAdded = false;
                    foreach (var trendReport in items)
                    {
                        var monthlyData = trendReport.List;

                        if (!headersAdded)
                        {
                            IList<string> headers = new List<string>();
                            headers.Add("Account");

                            foreach (var data in monthlyData)//error
                            {
                                headers.Add(DateHelper.ToMonthString(data.StartDate));
                            }

                            headers.Add("Region");
                            headers.Add("Account Type");
                            headers.Add("Coverage");
                            headers.Add("Lead CCM");
                            headers.Add("SCCM");

                            sheet.AddHeaderColumns(headers, rowNo);

                            headersAdded = true;
                        }

                        var values = new List<string>();

                        var accountData = ExtractColumns(trendReport.Account);

                        values.Add(accountData["account.Name"]);
                        foreach (var data in monthlyData)
                        {
                            values.Add($"[{" "}|style:{UlxConfig.Get($"StatusColor:{data.BragStatus}", "White")}BG]");
                        }

                        values.Add(accountData["account.Region"]);
                        values.Add(accountData["account.Type"]);
                        values.Add(accountData["account.Coverage"]);
                        values.Add(accountData["account.LeadCCM"]);
                        values.Add(accountData["account.SCCM"]);

                        sheet.AddDataRow(values, ++rowNo);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error($"{logContext}", ex);
            }

            return new MessageAttachment
            {
                Path = excel.Save(),
                FileName = fileName
            };
        }


        private IDictionary<string, string> ExtractColumns(IDictionary<string, HeatMapData> item, IDictionary<string, string> values = null)
        {

            if (values == null)
            {
                values = new Dictionary<string, string>();
            }

            foreach (var key in item.Keys)
            {

                var data = item[key];
                if (!string.IsNullOrEmpty(data.Header))
                {
                    values.Add(key, data.Header);
                }
                else if (!data.Count.HasValue)
                {
                    values.Add(key, "");
                }
                else
                {

                    var value = data.Count;

                    var style = "WhiteBG";

                    // TODO:

                    if (value == 0)
                    {
                        style = "WhiteBG";
                    }
                    else if (value == 1)
                    {
                        style = "CreamBG";
                    }
                    else if (value <= 5)
                    {
                        style = "PinkBG";
                    }
                    else if (value <= 10)
                    {
                        style = "OrangeBG";
                    }
                    else if (value <= 20)
                    {
                        style = "CoralBG";
                    }
                    else // > 20
                    {
                        style = "RedBG";
                    }


                    values.Add(key, $"[{value}|{LinkHelper.GetHeatMapDataLink(data)}|style:{style}]");
                }
            }

            return values;


        }

        private IDictionary<string, string> ExtractColumns(Obligation obligation, IDictionary<string, string> values = null)
        {
            if (values == null)
            {
                values = new Dictionary<string, string>();
            }

            values["document.Name"] = GetDocumentName(obligation);

            values["obligation.Id"] = $"[{obligation.Id}M|{LinkHelper.GetObligationLink(obligation)}]";
            values["obligation.Name"] = obligation.Name;

            values["obligation.IsTrigger"] = obligation.IsTrigger ? "YES" : "NO";
            values["obligation.IsFinancial"] = obligation.IsFinancial ? "YES" : "NO";
            values["obligation.ActionType"] = obligationRepository.GetActionTypeName(obligation.ActionType);

            values["obligation.Owner.FullName"] = "Not Available";
            values["obligation.Manager.FullName"] = "Not Available";
            values["obligation.SharedWith.FullName"] = "Not Available";

            values["obligation.StartDate"] = "Not Available";

            values["obligation.Frequency.Name"] = "Not Available";
            values["obligation.Type.Name"] = "Not Available";
            values["obligation.SubType.Name"] = "Not Available";
            values["obligation.Phase.Name"] = "Not Available";
            values["obligation.Responsibility.Name"] = "Not Available";

            if (obligation.StartDate.HasValue)
            {
                values["obligation.StartDate"] = DateHelper.ToDateString(obligation.StartDate.Value);
            }

            if (obligation.OwnerUser != null)
            {
                values["obligation.Owner.FullName"] = $"{obligation.OwnerUser.FirstName} {obligation.OwnerUser.LastName}";
            }

            if (obligation.OwnerManager != null)
            {
                values["obligation.Manager.FullName"] = $"{obligation.OwnerManager.FirstName} {obligation.OwnerManager.LastName}";
            }

            if (obligation.SharedWith != null)
            {
                values["obligation.SharedWith.FullName"] = $"{obligation.SharedWith.FirstName} {obligation.SharedWith.LastName}";
            }

            if (obligation.Frequency != null)
            {
                values["obligation.Frequency.Name"] = obligation.Frequency.Name;
            }

            if (obligation.Type != null && !string.IsNullOrEmpty(obligation.Type.Name))
            {
                values["obligation.Type.Name"] = obligation.Type.Name;
            }

            if (obligation.SubType != null && !string.IsNullOrEmpty(obligation.SubType.Name))
            {
                values["obligation.SubType.Name"] = obligation.SubType.Name;
            }

            if (obligation.Phase != null && !string.IsNullOrEmpty(obligation.Phase.Name))
            {
                values["obligation.Phase.Name"] = obligation.Phase.Name;
            }

            if (obligation.Responsibility != null && !string.IsNullOrEmpty(obligation.Responsibility.Name))
            {
                values["obligation.Responsibility.Name"] = obligation.Responsibility.Name;
            }

            return values;
        }

        private IDictionary<string, string> ExtractColumns(ChildObligation childObligation, IDictionary<string, string> values = null)
        {
            if (values == null)
            {
                values = new Dictionary<string, string>();
            }

            values["childObligation.Id"] = $"[{childObligation.Id}C|{LinkHelper.GetChildObligationLink(childObligation)}]";
            values["childObligation.Date"] = DateHelper.ToDateString(childObligation.Date);

            return values;
        }

        public MessageAttachment Create(IDictionary<string, IList<UserAuditLog>> maps, string name)
        {
            var logContext = $"[Create: obligationListMap]";
            log.Debug($"{logContext} Started");
            var fileName = UlxConfig.Get($"Excels:{name}:Name");
            var excel = excelProvider.CreateExcel(fileName);

            foreach (var sheetName in maps.Keys)
            {
                var items = maps[sheetName];

                try
                {
                    var sheet = excel.CreateSheet(UlxConfig.Get($"Excels:{name}:Sheets:{sheetName}", sheetName));

                    var rowNo = 1;

                    var headers = new List<String>
                    {
                        "Date",
                        "Event",
                        "Document Type",
                        "Document Name",
                        "Document Id",
                        "User"
                    };

                    sheet.AddHeaderColumns(headers, rowNo);
                    foreach (var item in items)
                    {
                        var values = new List<String>();
                        values.Add(DateHelper.ToDateString(item.Date));
                        values.Add(item.Event.Name);
                        values.Add(item.GroupFor.ToString());
                        values.Add(item.GroupName.ToString());
                        values.Add(item.GroupId.ToString());
                        values.Add(item.User.FullName);
                        sheet.AddDataRow(values, ++rowNo);
                    }
                }
                catch (Exception ex)
                {
                    log.Error($"{logContext}", ex);
                }
            }

            log.Debug($"{logContext} Done");

            return new MessageAttachment
            {
                Path = excel.Save(),
                FileName = fileName
            };
        }
        private IDictionary<string, string> ExtractColumns(Account account, IDictionary<string, string> values = null)
        {
            if (values == null)
            {
                values = new Dictionary<string, string>();
            }

            values["account.Id"] = $"[{account.Id}|{LinkHelper.GetAccountLink(account)}]";
            values["account.Name"] = account.Name;
            values["account.Coverage"] = "Not Available";
            values["account.TimeZone"] = "Not Available";
            values["account.Type"] = "Not Available";
            values["account.Region"] = "Not Available";


            if (account.AccountCoverage != null)
            {
                values["account.Coverage"] = account.AccountCoverage.Name;
            }

            if (account.TimeZone != null)
            {
                values["account.TimeZone"] = account.TimeZone.Name;
            }

            if (account.AccountType != null)
            {
                values["account.Type"] = account.AccountType.Name;
            }

            if (account.Region != null)
            {
                values["account.Region"] = account.Region.Name;
            }

            if (account.Managers.Any())
            {
                foreach (var manager in account.Managers)
                {
                    if (manager.ManagerType == Convert.ToInt32(UlxConfig.Get("Managers:LeadCCM")))
                    {
                        values["account.LeadCCM"] = userRepository.Get(manager.Id)?.FullName;
                    }
                    if (manager.ManagerType == Convert.ToInt32(UlxConfig.Get("Managers:SCCM")))
                    {
                        values["account.SCCM"] = userRepository.Get(manager.Id)?.FullName;
                    }
                }
            }

            return values;
        }

        private IDictionary<string, string> ExtractColumns(Document document, IDictionary<string, string> values = null)
        {
            if (values == null)
            {
                values = new Dictionary<string, string>();
            }

            values["document.Id"] = document.Id.ToString();
            values["document.Name"] = document.Name;
            values["document.Type"] = document.Type.ToString();

            return values;
        }

        private string GetDocumentName(Obligation item)
        {
            if (item.GroupForId == 0)
            {
                return "";
            }

            switch (item.GroupFor)
            {
                case GroupFor.Account:
                    var account = accountRepository.Get(item.GroupForId);
                    if (account != null)
                    {
                        return account.Name;
                    }
                    else
                    {
                        return "";
                    }

                case GroupFor.SA:
                    var sa = saRepository.Get(item.GroupForId);
                    if (sa != null)
                    {
                        return sa.Name;
                    }
                    else
                    {
                        return "";
                    }

                case GroupFor.MSA:
                    var msa = msaRepository.Get(item.GroupForId);
                    if (msa != null)
                    {
                        return msa.Name;
                    }
                    else
                    {
                        return "";
                    }
                default:
                    return "";

            }
        }


    }
}