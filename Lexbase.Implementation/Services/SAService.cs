using Lexbase.Models;
using Lexbase.Repositories;
using Lexbase.Services;
using log4net;
using System.Collections.Generic;

namespace Lexbase.Implementation.Services
{
    public class SAService : ISAService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(SAService));

        private readonly ISARepository saRepository;
        private readonly IMSARepository msaRepository;

        private readonly IObligationRepository obligationRepository;
        private readonly IAccountRepository accountRepository;
        private readonly IUserService userService;
        private readonly IObligationService obligationService;
        private readonly IExcelService excelService;
        private readonly IEmailService emailService;

        public SAService(
            ISARepository saRepository,
            IMSARepository msaRepository,
            IObligationRepository obligationRepository,
            IAccountRepository accountRepository,

            IUserService userService,
            IObligationService obligationService,
            IExcelService excelService,
            IEmailService emailService
        )
        {
            this.saRepository = saRepository;
            this.msaRepository = msaRepository;
            this.obligationRepository = obligationRepository;
            this.accountRepository = accountRepository;
            this.userService = userService;
            this.obligationService = obligationService;
            this.excelService = excelService;
            this.emailService = emailService;
        }

        public void ActivateMSA(long id, Account account = null)
        {
            var logContext = $"[ActivateMSA:{id}]";
            log.Debug($"{logContext} Starting");
            var msa = msaRepository.Get(id);
            msa.Account = account ?? GetAccount(msa.Account.Id);

            msaRepository.StartActivation(id);
            var sendEmail = account == null; // if getting activated from parent don't send email
            ActivateChildren(id, GroupFor.MSA, ParentFor.MSA, msa.Account, new Document
            {
                Id = id,
                Type = GroupFor.MSA,
                Name = msa.Name
            }, sendEmail, logContext);
            msaRepository.CompleteActivation(id);

            log.Info($"{logContext} Done");
        }

        public void ActivateSA(long id, Account account = null)
        {
            var logContext = $"[ActivateSA:{id}]";
            log.Debug($"{logContext} Starting");

            var sa = saRepository.Get(id);
            sa.Account = account ?? GetAccount(sa.Account.Id);

            saRepository.StartActivation(id);
            var sendEmail = account == null; // if getting activated from parent don't send email
            ActivateChildren(id, GroupFor.SA, ParentFor.SA, sa.Account, new Document
            {
                Id = id,
                Type = GroupFor.SA,
                Name = sa.Name
            }, sendEmail, logContext);
            saRepository.CompleteActivation(id);

            log.Info($"{logContext} Done");
        }

        private Account GetAccount(long id)
        {
            var account = accountRepository.Get(id);
            account.Managers = userService.GetManagers(id);
            account.NonManagers = userService.GetUsers(id, account.Managers as List<User>);
            account.UserMap = MergeUsers(account.Managers, account.NonManagers);

            return account;
        }

        private void ActivateChildren(long id, GroupFor groupType, ParentFor parentType, Account account, Document document, bool sendEmail, string logContext)
        {
            foreach (var item in account.NonManagers)
            {
                if (!item.Active)
                {
                    userService.Activate(item);
                }
            }

            var childMSAs = msaRepository.Search(new Queries.MServiceAgreementQuery
            {
                ParentId = id,
                ParentType = parentType,
                InActive = true
            });

            log.Debug($"{logContext} {childMSAs.Count} child msa(s) found");

            foreach (var item in childMSAs)
            {
                ActivateMSA(item.Id, account);
            }

            var childSA = saRepository.Search(new Queries.ServiceAgreementQuery
            {
                ParentId = id,
                ParentType = parentType,
                InActive = true
            });

            log.Debug($"{logContext} {childMSAs.Count} child sa(s) found");

            foreach (var item in childSA)
            {
                ActivateSA(item.Id, account);
            }

            var obligations = obligationRepository.Search(new Queries.ObligationQuery
            {
                GroupId = id,
                GroupType = groupType,
                InActive = true
            });

            log.Info($"{logContext} {obligations.Count} obligation(s) needs activation");
            foreach (var item in obligations)
            {
                obligationService.Activate(item.Id);
                item.Account = account;
                if (item.OwnerUser != null && account.UserMap.ContainsKey(item.OwnerUser.Id))
                {
                    item.OwnerUser = account.UserMap[item.OwnerUser.Id];
                }

                if (item.SharedWith != null && account.UserMap.ContainsKey(item.SharedWith.Id))
                {
                    item.SharedWith = account.UserMap[item.SharedWith.Id];
                }

                if (item.OwnerManager != null && account.UserMap.ContainsKey(item.OwnerManager.Id))
                {
                    item.OwnerManager = account.UserMap[item.OwnerManager.Id];
                }
            }

            if (obligations.Count > 0 || sendEmail)
            {
                log.Debug($"{logContext} Sending Emails");
                SendObligationsToManagers(obligations as List<Obligation>, account, document);
                SendObligationsToUsers(obligations as List<Obligation>, account, document);
            }
        }

        private IDictionary<long, User> MergeUsers(IList<User> managers, IList<User> users)
        {
            var items = new Dictionary<long, User>();

            foreach (var item in users)
            {
                items[item.Id] = item;
            }

            foreach (var item in managers)
            {
                items[item.Id] = item;
            }

            return items;
        }

        private void SendObligationsToManagers(List<Obligation> obligations, Account account, Document document)
        {
            var logContext = $"[SendObligationsToManagers]";
            log.Debug($"{logContext} Started");

            if (account.Managers == null || account.Managers.Count == 0)
            {
                log.Info($"{logContext} no managers to send email");
                return;
            }

            if (obligations.Count == 0)
            {
                foreach (var user in account.Managers)
                {
                    emailService.Send(user, "Sub-AccountActivation-Manager-NoObligations", account);
                }
                log.Info($"{logContext} Done");
                return;
            }

            var excelSheetMap = new Dictionary<string, IList<Obligation>>();

            var reportObligations = obligations.FindAll(i => i.ActionType == "R");
            if (reportObligations.Count > 0)
            {
                excelSheetMap["Reported"] = reportObligations;
            }

            var triggerObligations = obligations.FindAll(i => i.IsTrigger);
            if (triggerObligations.Count > 0)
            {
                excelSheetMap["Triggered"] = triggerObligations;
            }

            var alertObligations = obligations.FindAll(i => i.ActionType == "A");
            if (alertObligations.Count > 0)
            {
                excelSheetMap["Alerted"] = alertObligations;
            }

            var monitoredObligations = obligations.FindAll(i => i.ActionType == "M");
            if (monitoredObligations.Count > 0)
            {
                excelSheetMap["Monitoried"] = monitoredObligations;
            }

            if (excelSheetMap.Count < 1)
            {
                log.Info($"{logContext} No Obligations to Send");
                return;
            }

            var attachment = excelService.Create(account, excelSheetMap, "ObligationsForManagers");
            log.Debug($"{logContext} Attachment Path {attachment}");

            foreach (var user in account.Managers)
            {
                emailService.Send(user, "Sub-AccountActivation-Manager", account, attachment, document);
            }
            log.Info($"{logContext} Done");
        }

        private void SendObligationsToUsers(List<Obligation> obligations, Account account, Document document)
        {
            var logContext = $"[SendObligationsToUsers:{account.Id}]";

            if (account.NonManagers == null || account.NonManagers.Count == 0)
            {
                log.Info($"{logContext} Aborting, Reason: No users to send email");
                return;
            }

            if (obligations.Count == 0)
            {
                log.Info($"{logContext} Aborting, Reason: No obligations to send email");
                return;
            }

            log.Debug($"{logContext} Started");

            foreach (var user in account.NonManagers)
            {
                log.Debug($"{logContext} Fetching ObligationsCombined");
                var items = obligations.FindAll(i =>
               {
                   if (i.OwnerUser != null && i.OwnerUser.Id == user.Id)
                   {
                       return true;
                   }

                   if (i.OwnerManager != null && i.OwnerManager.Id == user.Id)
                   {
                       return true;
                   }

                   if (i.SharedWith != null && i.SharedWith.Id == user.Id)
                   {
                       return true;
                   }

                   return false;
               });

                if (items.Count < 1)
                {
                    log.Info($"{logContext} No obligations assigned to {user.FullName}");
                    continue;
                }

                var excelSheetMap = new Dictionary<string, IList<Obligation>>();
                excelSheetMap["Assigned"] = items;
                var attachment = excelService.Create(account, excelSheetMap, "ObligationsForUsers");
                log.Debug($"{logContext} Attachment Path {attachment}");
                emailService.Send(user, "Sub-AccountActivation-Owner", account, attachment, document);
            }

            log.Info($"{logContext} Done");
        }
    }
}