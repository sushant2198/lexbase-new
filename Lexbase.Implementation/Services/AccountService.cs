﻿using Lexbase.Models;
using Lexbase.Queries;
using Lexbase.Repositories;
using Lexbase.Services;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Lexbase.Implementation.Services
{
    public class AccountService : IAccountService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(AccountService));
        private readonly IUserRepository userRepository;
        private readonly IAccountRepository accountRepository;
        private readonly IMSARepository msaRepository;
        private readonly ISARepository saRepository;
        private readonly IObligationRepository obligationRepository;
        private readonly IUserService userService;
        private readonly IObligationService obligationService;
        private readonly ISAService saService;
        private readonly IExcelService excelService;
        private readonly IEmailService emailService;
        private readonly IConfigPropertyRepository configPropertyRepository;
        private readonly IChildObligationRepository childObligationRepository;

        public AccountService(
            IUserRepository userRepository,
            IAccountRepository accountRepository,
            IMSARepository msaRepository,
            ISARepository saRepository,
            IObligationRepository obligationRepository,
            IConfigPropertyRepository configPropertyRepository,

            IUserService userService,
            IObligationService obligationService,
            ISAService saService,
            IExcelService excelService,
            IEmailService emailService,
            IChildObligationRepository childObligationRepository
        )
        {
            this.userRepository = userRepository;
            this.accountRepository = accountRepository;
            this.msaRepository = msaRepository;
            this.saRepository = saRepository;
            this.obligationRepository = obligationRepository;
            this.configPropertyRepository = configPropertyRepository;

            this.userService = userService;
            this.obligationService = obligationService;
            this.saService = saService;
            this.excelService = excelService;
            this.emailService = emailService;
            this.childObligationRepository = childObligationRepository;
        }

        public void Activate(long accountId)
        {
            var logContext = $"[Activate:{accountId}]";
            log.Info($"{logContext} Activating");
            accountRepository.StartActivation(accountId);
            var account = accountRepository.Get(accountId);

            account = accountRepository.Get(account.Id);
            account.Managers = userService.GetManagers(account.Id);
            account.NonManagers = userService.GetUsers(account.Id, account.Managers as List<User>);
            account.UserMap = MergeUsers(account.Managers, account.NonManagers);

            log.Info($"{logContext} {account.Managers.Count} manager(s) needs activation");
            foreach (var item in account.Managers)
            {
                if (!item.Active)
                {
                    userService.Activate(item);
                }
            }

            log.Info($"{logContext} {account.NonManagers.Count} users(s) needs activation");
            foreach (var item in account.NonManagers)
            {
                if (!item.Active)
                {
                    userService.Activate(item);
                }
            }

            var obligations = obligationRepository.Search(new ObligationQuery
            {
                AccountId = accountId,
                InActive = true
            });
            log.Info($"{logContext} {obligations.Count} obligation(s) needs activation");
            foreach (var item in obligations)
            {
                obligationService.Activate(item.Id);

                item.Account = account;
                if (item.OwnerUser != null && account.UserMap.ContainsKey(item.OwnerUser.Id))
                {
                    item.OwnerUser = account.UserMap[item.OwnerUser.Id];
                }

                if (item.SharedWith != null && account.UserMap.ContainsKey(item.SharedWith.Id))
                {
                    item.SharedWith = account.UserMap[item.SharedWith.Id];
                }

                if (item.OwnerManager != null && account.UserMap.ContainsKey(item.OwnerManager.Id))
                {
                    item.OwnerManager = account.UserMap[item.OwnerManager.Id];
                }
            }
            log.Debug($"{logContext} Sending Emails");
            SendObligationsToManagers(obligations as List<Obligation>, account);
            if (obligations.Count > 0)
            {
                SendObligationsToUsers(obligations as List<Obligation>, account);
            }

            ActivateChildren(account);

            accountRepository.CompleteActivation(accountId);
            log.Info($"{logContext} Done");
        }

        public AccountDump GetAccountDump(long accountId)
        {
            var account = accountRepository.Get(accountId);
            account.Managers = userService.GetManagers(accountId);
            account.NonManagers = userService.GetUsers(accountId, account.Managers as List<User>);

            var documents = new List<Document>();
            var msaServiceAgreements = msaRepository.Search(new Queries.MServiceAgreementQuery
            {
                IsActive = true,
                AccountId = accountId
            });

            foreach (var item in msaServiceAgreements)
            {
                documents.Add(new Document
                {
                    Id = item.Id,
                    Name = item.Name,
                    Type = GroupFor.MSA
                });
            }

            var saServiceAgreements = saRepository.Search(new Queries.ServiceAgreementQuery
            {
                IsActive = true,
                AccountId = accountId
            });

            foreach (var item in saServiceAgreements)
            {
                documents.Add(new Document
                {
                    Id = item.Id,
                    Name = item.Name,
                    Type = GroupFor.SA
                });
            }

            var obligations = obligationRepository.Search(new Queries.ObligationQuery
            {
                AccountId = accountId
            });

            //var childObligations = childObligationRepository.Search(new Queries.ChildObligationQuery
            //{
            //    AccountId = accountId
            //});

            return new AccountDump
            {
                Account = account,
                Documents = documents,
                Obligations = obligations
            };
        }

        private void ActivateChildren(Account account)
        {
            var logContext = $"[ActivateChildren:{account.Id}]";
            log.Debug($"{logContext} Starting");

            var msaServiceAgreements = msaRepository.Search(new MServiceAgreementQuery
            {
                ParentId = account.Id,
                ParentType = ParentFor.Account,
                InActive = true
            });
            log.Info($"{logContext} {msaServiceAgreements.Count} msa(s) needs activation");
            foreach (var item in msaServiceAgreements)
            {
                saService.ActivateMSA(item.Id, account);
            }

            var saServiceAgreements = saRepository.Search(new ServiceAgreementQuery
            {
                ParentId = account.Id,
                ParentType = ParentFor.Account,
                InActive = true
            });
            log.Info($"{logContext} {saServiceAgreements.Count} sa(s) needs activation");
            foreach (var item in saServiceAgreements)
            {
                saService.ActivateSA(item.Id, account);
            }
        }

        private IDictionary<long, User> MergeUsers(IList<User> managers, IList<User> users)
        {
            var items = new Dictionary<long, User>();

            foreach (var item in users)
            {
                items[item.Id] = item;
            }

            foreach (var item in managers)
            {
                items[item.Id] = item;
            }

            return items;
        }

        private void SendObligationsToManagers(List<Obligation> obligations, Account account)
        {
            var logContext = $"[SendObligationsToManagers]";
            log.Debug($"{logContext} Started");

            if (account.Managers == null || account.Managers.Count == 0)
            {
                log.Info($"{logContext} no managers to send email");
                return;
            }

            if (obligations.Count == 0)
            {
                foreach (var user in account.Managers)
                {
                    emailService.Send(user, "AccountActivation-Manager-NoObligations", account);
                }
                log.Info($"{logContext} Done");
                return;
            }

            var excelSheetMap = new Dictionary<string, IList<Obligation>>();

            var reportObligations = obligations.FindAll(i => i.ActionType == "R");
            if (reportObligations.Count > 0)
            {
                excelSheetMap["Reported"] = reportObligations;
            }

            var triggerObligations = obligations.FindAll(i => i.IsTrigger);
            if (triggerObligations.Count > 0)
            {
                excelSheetMap["Triggered"] = triggerObligations;
            }

            var alertObligations = obligations.FindAll(i => i.ActionType == "A");
            if (alertObligations.Count > 0)
            {
                excelSheetMap["Alerted"] = alertObligations;
            }

            var monitoredObligations = obligations.FindAll(i => i.ActionType == "M");
            if (monitoredObligations.Count > 0)
            {
                excelSheetMap["Monitored"] = monitoredObligations;
            }

            if (excelSheetMap.Count < 1)
            {
                log.Info($"{logContext} No Obligations to Send");
                return;
            }

            var attachment = excelService.Create(account, excelSheetMap, "ObligationsForManagers");
            log.Debug($"{logContext} Attachment Path {attachment}");

            var document = new Document
            {
                Id = account.Id,
                Name = account.Name,
                Type = GroupFor.Account
            };

            foreach (var user in account.Managers)
            {
                emailService.Send(user, "AccountActivation-Manager", account, attachment, document);
            }
            log.Info($"{logContext} Done");
        }

        private void SendObligationsToUsers(List<Obligation> obligations, Account account)
        {
            var logContext = $"[SendObligationsToUsers:{account.Id}]";

            if (account.NonManagers == null || account.NonManagers.Count == 0)
            {
                log.Info($"{logContext} Aborting, Reason: No users to send email");
                return;
            }

            log.Debug($"{logContext} Started");

            var document = new Document
            {
                Id = account.Id,
                Name = account.Name,
                Type = GroupFor.Account
            };

            foreach (var user in account.NonManagers)
            {
                log.Debug($"{logContext} Fetching ObligationsCombined");
                var items = obligations.FindAll(i =>
                {
                    if (i.OwnerUser != null && i.OwnerUser.Id == user.Id)
                    {
                        return true;
                    }

                    if (i.OwnerManager != null && i.OwnerManager.Id == user.Id)
                    {
                        return true;
                    }

                    if (i.SharedWith != null && i.SharedWith.Id == user.Id)
                    {
                        return true;
                    }

                    return false;
                });

                if (items.Count < 1)
                {
                    log.Info($"{logContext} No obligations assigned to {user.FullName}");
                    continue;
                }

                var excelSheetMap = new Dictionary<string, IList<Obligation>>();
                excelSheetMap["Assigned"] = items;
                var attachment = excelService.Create(account, excelSheetMap, "ObligationsForUsers");
                log.Debug($"{logContext} Attachment Path {attachment}");
                emailService.Send(user, "AccountActivation-Owner", account, attachment, document);
            }

            log.Info($"{logContext} Done");
        }

        private void SetManagersByType(Account account)
        {
            account.Managers = userService.GetManagers(account.Id);

            var logContext = $"[SetManagersByType:{account.Id}]";
            log.Debug($"{logContext} Started");
            var managerList = account.Managers;

            if (managerList == null || managerList.Count == 0)
            {
                return;
            }

            account.AccountManagerList = new List<User>();
            account.RegionalPOCList = new List<User>();
            foreach (var manager in managerList)
            {
                long managerType = manager.ManagerType;

                if (managerType == UlxConfig.AccountManagerType)
                {
                    account.AccountManagerList.Add(manager);
                }
                else if (managerType == UlxConfig.AccountExecutiveType)
                {
                    account.AccountExecutive = manager;
                }
                else if (managerType == UlxConfig.ServiceDeliveryType)
                {
                    account.ServiceDeliveryHead = manager;
                }
                else if (managerType == UlxConfig.FinanceManagerType)
                {
                    account.FinanceManager = manager;
                }
                else if (managerType == UlxConfig.HRManagerType)
                {
                    account.HRManager = manager;
                }
                else if (managerType == UlxConfig.PhaseManagerType)
                {
                    account.PhaseManager = manager;
                }
                else if (managerType == UlxConfig.AccountLeadManagerType)
                {
                    account.LeadManager = manager;
                }
                else if (managerType == UlxConfig.ContractAdministratorType)
                {
                    account.ContractAdministrator = manager;
                }
                else if (managerType == UlxConfig.ContractSpecialistType)
                {
                    account.ContractSpecialist = manager;
                }
                else if (managerType == UlxConfig.RegionalPOCType)
                {
                    account.RegionalPOCList.Add(manager);
                }
            }
        }

        private void BragStatus(AccountStatus accountStatus, bool forCurrentMonth)
        {
            int financialcurrentDayCobCount = 0;
            int nonFinancialCurrentDayCobCount = 0;
            var CObList = childObligationRepository.GetByAccount(accountStatus.Id);
            if (forCurrentMonth)
            {
                financialcurrentDayCobCount = CObList.Where(x => x.IsFinancial && x.Date == DateTime.Today && x.Status != "completed").Count();
                nonFinancialCurrentDayCobCount = CObList.Where(x => !x.IsFinancial && x.Date == DateTime.Today && x.Status != "completed").Count();
            }
            var financialCobList = CObList.Where(x => x.IsFinancial).ToList();
            var nonFinancialCobList = CObList.Where(x => !x.IsFinancial).ToList();

            ComputeAccountStatus(accountStatus, nonFinancialCobList, financialCobList, financialcurrentDayCobCount, nonFinancialCurrentDayCobCount);
        }

        private void ComputeAccountStatus(AccountStatus accountStatus, List<ChildObligation> nfCobList, List<ChildObligation> fCobList, int financialcurrentDayCobCount, int nonFinancialCurrentDayCobCount)
        {
            COBCountAccountStatus cobAS = new COBCountAccountStatus();

            CalculateCOBCount(nfCobList, cobAS, false, nonFinancialCurrentDayCobCount);
            CalculateCOBCount(fCobList, cobAS, true, financialcurrentDayCobCount);

            int status;

            /**
             * Formula for Account Brag Status Calculation based on CR-87
             */
            if ((cobAS.NfBeforeDDCount + cobAS.FBeforeDDCount) == (cobAS.FTotalCOBCount + cobAS.NfTotalCOBCount))
            {
                status = 1;
            }
            else if ((cobAS.NfBeforeDDCount + cobAS.FBeforeDDCount + cobAS.NfOnDDCount + cobAS.FOnDDCount) == (cobAS.FTotalCOBCount + cobAS.NfTotalCOBCount))
            {
                status = 2;
            }
            else if (((cobAS.NfBeforeDDCount + cobAS.FBeforeDDCount + cobAS.NfOnDDCount + cobAS.FOnDDCount) / (cobAS.FTotalCOBCount + cobAS.NfTotalCOBCount) >= 0.95) && (cobAS.FAfterDDCount == 0))
            {
                status = 3;
            }
            else
            {
                status = 4;
            }

            accountStatus.CurrentStatus = status;
            accountStatus.BragStatus = status;
            // Update account status written by Anurag
            accountRepository.UpdateInActiveAccountStatus(accountStatus, 0);

            /**
             * Formula for Financial Brag Status Calculation as per CR-87.
             * Financial Brag status is used for Account Health (obligations with Financial Impact) graph generation.
             * Only Obligation with Financial Impact are taken into Consideration
             */
            if (cobAS.FAfterDDCount > 0)
            {
                if (fCobList.Count > 0)
                {
                    var accountstatusData = new AccountStatusData
                    {
                        Id = accountStatus.Id,
                        StartDate = accountStatus.StartDate,
                        EndDate = accountStatus.EndDate,
                        ChildObligationIds = fCobList.Select(x => x.Id).ToList()
                    };
                    accountRepository.CreateAccountStatusData(accountstatusData);
                }
            }
        }

        private void CalculateCOBCount(List<ChildObligation> cobList, COBCountAccountStatus cobAs, bool isFinancial, int currentDateCOBCount)
        {
            int onDDCount = 0;
            int beforeDDCount = 0;
            int afterDDCount = 0;
            int totalCobCount = cobList.Count;

            foreach (var childOb in cobList)
            {
                DateTime completionDate = DateTime.MinValue;
                var todayDate = DateTime.Today.Date;
                if (childOb.DateCompleted != null)
                    completionDate = childOb.DateCompleted.Value;

                var delay = (!childOb.DueDate.HasValue || (completionDate == DateTime.MinValue)) ? 0 : ((completionDate - childOb.DueDate.Value.Date).TotalDays);
                if (completionDate == DateTime.MinValue)
                {
                    var diff = childOb.DueDate.HasValue ? (childOb.DueDate.Value.Date - todayDate).TotalDays : 0;
                    if (diff < 0)
                    {
                        if (isFinancial)
                            cobAs.PastDueCOBWithFinancialImpact.Add(childOb.Id);
                        afterDDCount++;
                    }
                    //if (diff == 0)
                    //{
                    //    onDDCount++;
                    //}
                }
                else if (delay == 0)
                {
                    onDDCount++;
                }
                else if (delay < 0)
                {
                    beforeDDCount++;
                }
                else
                {
                    if (isFinancial)
                        cobAs.PastDueCOBWithFinancialImpact.Add(childOb.Id);
                    afterDDCount++;
                }
            }
            if (isFinancial)
            {
                cobAs.FAfterDDCount = afterDDCount;
                cobAs.FBeforeDDCount = beforeDDCount;
                cobAs.FOnDDCount = onDDCount + currentDateCOBCount;
                cobAs.FTotalCOBCount = totalCobCount;
            }
            else
            {
                cobAs.NfAfterDDCount = afterDDCount;
                cobAs.NfBeforeDDCount = beforeDDCount;
                cobAs.NfOnDDCount = onDDCount + currentDateCOBCount;
                cobAs.NfTotalCOBCount = totalCobCount;
            }
        }

        private void SetCompletedRatio(AccountStatus accountStatus)
        {
            var now = DateTime.UtcNow;
            var CObList = childObligationRepository.GetByAccount(accountStatus.Id);
            var overAllCount = CObList.Where(x => x.ActionType == "M" && ((!x.ApprovalRequired && x.SubmissionDate <= x.DueDate) || (!x.ApprovalRequired && x.CustomerApprovalDate <= x.DueDate)) && x.DueDate >= accountStatus.StartDate && x.DueDate <= accountStatus.EndDate).Count();

            var completedCount = CObList.Where(x => x.Status == "completed" && x.ActionType == "M" && ((x.ApprovalRequired && x.SubmissionDate <= x.DueDate) || (x.ApprovalRequired && x.CustomerApprovalDate <= x.DueDate)) && x.DueDate >= accountStatus.StartDate && x.DueDate <= accountStatus.EndDate).Count();

            if (overAllCount > 0)
            {
                double bragRatio = (completedCount == 0 || overAllCount == 0) ? 0 : ((completedCount / overAllCount) * 100);
                accountStatus.BragRatio = bragRatio;
            }
        }

        private void SetCompletedRatioByFG(AccountStatus accountStatus)
        {
            var now = DateTime.UtcNow;
            var CObList = childObligationRepository.GetByAccount(accountStatus.Id);
            var overAllMap = CObList.Where(x => x.ActionType == "M" && ((!x.ApprovalRequired && x.SubmissionDate <= x.DueDate) || (x.ApprovalRequired && x.CustomerApprovalDate <= x.DueDate)) && x.DueDate >= accountStatus.StartDate && x.DueDate <= accountStatus.EndDate).GroupBy(x => x.ObligationType).Select(x => new { Type = x.Key, Total = x.Count() });

            var completedMap = CObList.Where(x => x.ActionType == "M" && x.Status == "completed" && ((!x.ApprovalRequired && x.SubmissionDate <= x.DueDate) || (x.ApprovalRequired && x.CustomerApprovalDate <= x.DueDate)) && x.DueDate >= accountStatus.StartDate && x.DueDate <= accountStatus.EndDate).GroupBy(x => x.ObligationType).Select(x => new { Type = x.Key, Total = x.Count() });

            var ratioMap = new Dictionary<long, double>();
            if (overAllMap.Any())
            {
                bool isDeleted = false;
                foreach (var entry in overAllMap)
                {
                    if (completedMap.Where(x => x.Type == entry.Type).Count() > 0)
                    {
                        var key = completedMap.Select(x => x.Type).FirstOrDefault();
                        double ratio = (key == 0 || entry.Total == 0) ? 0 : ((key / entry.Total) * 100);
                        ratioMap.Add(entry.Type, ratio);

                        var accountStatusData = new AccountStatusData
                        {
                            Id = accountStatus.Id,
                            ObligationType = entry.Type,
                            StartDate = accountStatus.StartDate,
                            EndDate = accountStatus.EndDate,
                            BragRatio = ratio
                        };
                        if (!isDeleted)
                        {
                            accountRepository.DeleteAccountStatusByFG(accountStatusData);
                            isDeleted = true;
                        }
                        accountRepository.CreateAccountStatusByFG(accountStatusData);
                    }
                    else
                        ratioMap.Add(entry.Type, new double());
                }
                accountStatus.RatioMap = ratioMap;
            }
        }

        public void UpdateAccountStatus(IList<Account> accountStaus)
        {
            var activeAccount = accountStaus.Where(v => v.Active == true).ToList();
            var inActiveAccount = accountStaus.Where(v => v.Active == false).ToList();

            var currentDate = DateTime.UtcNow.Date;
            var FirstDayOftheMonth = new DateTime(currentDate.Year, currentDate.Month, 1);
            var LastDayOftheMonth = new DateTime(currentDate.Year, currentDate.Month, DateTime.DaysInMonth(currentDate.Year, currentDate.Month));

            if (activeAccount != null)
            {
                int dateCompare = DateTime.Compare(currentDate, FirstDayOftheMonth);

                if (dateCompare == 0)
                {
                    var FirstDayofPrevMonth = FirstDayOftheMonth.AddMonths(-1);
                    var LastDayofPrevMonth = FirstDayOftheMonth.AddDays(-1);
                    var freezingStatus = this.accountRepository.GetAccountFreezingStatus(FirstDayofPrevMonth, LastDayofPrevMonth);

                    foreach (var account in activeAccount)
                    {
                        AccountStatus accountStatus = new AccountStatus();
                        var Freegaccount = freezingStatus.Where(f => f.Id == account.Id).FirstOrDefault();
                        if (Freegaccount != null)
                        {
                            accountStatus.Id = account.Id;
                            accountStatus.StartDate = FirstDayofPrevMonth;
                            accountStatus.EndDate = LastDayofPrevMonth;
                            accountStatus.Freezingtatus = true;
                            BragStatus(accountStatus, false);
                        }
                        else
                        {// this else not needed because the uper else will remove and run without else.
                            accountStatus.Id = account.Id;
                            accountStatus.StartDate = FirstDayOftheMonth;
                            accountStatus.EndDate = LastDayOftheMonth;
                            BragStatus(accountStatus, true);
                        }
                    }
                }
                else
                { //This Else part not needed - below code will run without else means running in main flow.
                    // to do calll amir
                    //FirstDayOftheMonth and LastDayOftheMonth as param
                    foreach (var account in activeAccount)
                    {
                        AccountStatus accountStatus = new AccountStatus();
                        accountStatus.Id = account.Id;
                        accountStatus.StartDate = FirstDayOftheMonth;
                        accountStatus.EndDate = LastDayOftheMonth;
                        BragStatus(accountStatus, true);
                    }
                }
            }

            foreach (var account in inActiveAccount)
            {
                AccountStatus accountStatus = new AccountStatus();

                accountStatus.Id = account.Id;

                if (account.IsArchive == 1)
                {
                    accountStatus.BragStatus = 5; //Gray
                }
                else
                {
                    accountStatus.BragStatus = 0; //Black
                }

                accountStatus.StartDate = FirstDayOftheMonth;
                accountStatus.EndDate = LastDayOftheMonth;

                this.accountRepository.UpdateInActiveAccountStatus(accountStatus, 1);
            }
        }

        private class COBCountAccountStatus
        {
            public double FOnDDCount = 0;
            public double FBeforeDDCount = 0;
            public double FAfterDDCount = 0;
            public double NfOnDDCount = 0;
            public double NfBeforeDDCount = 0;
            public double NfAfterDDCount = 0;
            public double FTotalCOBCount = 0;
            public double NfTotalCOBCount = 0;
            public List<long> PastDueCOBWithFinancialImpact = new List<long>();
        }
    }
}