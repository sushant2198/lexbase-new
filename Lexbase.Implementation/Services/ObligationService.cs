using Lexbase.Helpers;
using Lexbase.Models;
using Lexbase.Repositories;
using Lexbase.Services;
using log4net;
using System;

namespace Lexbase.Implementation.Services
{
    public class ObligationService : IObligationService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ObligationService));

        private readonly IObligationRepository obligationRepository;
        private readonly IChildObligationRepository childObligationRepository;
        private readonly IReminderFrequencyRepository reminderFrequencyRepository;
        private readonly IObligationTriggerLogRepository obligationTriggerLogRepository;
        private readonly IAccountRepository accountRepository;

        public ObligationService(
            IObligationRepository obligationRepository,
            IChildObligationRepository childObligationRepository,
            IReminderFrequencyRepository reminderFrequencyRepository,
            IObligationTriggerLogRepository obligationTriggerLogRepository,
            IAccountRepository accountRepository
        )
        {
            this.obligationRepository = obligationRepository;
            this.childObligationRepository = childObligationRepository;
            this.reminderFrequencyRepository = reminderFrequencyRepository;
            this.obligationTriggerLogRepository = obligationTriggerLogRepository;
            this.accountRepository = accountRepository;

        }

        public void Activate(long id)
        {
            var logContext = $"[Activate:{id}]";
            log.Debug($"{logContext} Activating");

            obligationRepository.CompleteActivation(id);
            log.Info($"{logContext} Complete");
        }

        public void Schedule(long id)
        {
            var logContext = $"[Schedule:{id}]";
            var item = obligationRepository.Get(id);
            item.Account = accountRepository.Get(item.Account.Id);

            if (item.InProcessing)
            {
                log.Info($"{logContext} Skipping. Reason: is alaready InProcessing");
                return;
            }

            log.Debug($"{logContext} Started");
            obligationRepository.StartProcessing(id);

            if (item.RulesUpdated > 0 && item.RulesUpdated != 3)
            {
                item.UptoDate = childObligationRepository.Clean(item.Id);

                if (DateHelper.IsFuture(item.StartDate.Value) || (item.UptoDate.HasValue && item.UptoDate == item.StartDate))
                {
                    item.UptoDate = null;
                }
            }

            if (item.ActionType == "A" || item.ActionType == "M")
            {
                CreateChildren(item);
            }
            obligationRepository.CompleteProcessing(item);
            log.Info($"{logContext} Complete");
        }

        public void MarkOverDue(ChildObligation item)
        {
            var logContext = $"[MarkOverDue:{item.Id}/{item.Status}]";
            log.Debug($"{logContext} Started");
            if (!DateHelper.IsFuture(item.DueDate.Value))
            {
                childObligationRepository.SetStatus(item, "overdue");
            }
            else if (item.Status == "overdue") // is due for future
            {
                childObligationRepository.SetStatus(item, "pending");

                item = childObligationRepository.Get(item.Id);
                item.Parent.Account = accountRepository.Get(item.Parent.Account.Id);
                CreateReminders(item.Parent, item);
            }
            log.Info($"{logContext} Done");
        }

        public void SetStatus(ChildObligation item, ObligationTriggerLog trigger)
        {
            var logContext = $"[SetStatus:{item.Id}/{item.Status}]";

            if (!string.IsNullOrEmpty(item.Status) && "overdue".Contains(item.Status.ToLower()))
            {
                // overdue cannot be changed
                log.Debug($"{logContext} not changing status");
                return;
            }

            log.Debug($"{logContext} Started");

            string newStatus = null;

            switch (trigger.Reminder.Status.ToLower())
            {
                case "escalated":
                    newStatus = "escalated";
                    break;

                case "alert":
                    if (item.ActionType == "A")
                    {
                        newStatus = "alerted";
                    }
                    break;
            }

            log.Debug($"{logContext} new status: {newStatus}");

            if (string.IsNullOrEmpty(newStatus) || newStatus.ToLower() == item.Status.ToLower())
            {
                log.Debug($"{logContext} not changing status");
                return;
            }

            childObligationRepository.SetStatus(item, newStatus);
            log.Info($"{logContext} Done");
        }

        private void CreateChildren(Obligation item)
        {
            var logContext = $"[CreateChildren:{item.Id}]";
            log.Debug($"{logContext} Started");

            var periodicityType = UlxConfig.Get($"Periodicity:{item.Frequency.Name}:Type");
            var periodicityValue = UlxConfig.GetInteger($"Periodicity:{item.Frequency.Name}:Value", 1);

            var fromDate = item.StartDate.Value;
            var tillDate = DateTime.Today.AddMonths(item.Frequency.AdvanceByMonths);

            if (!item.UptoDate.HasValue)
            {
                // first time advance by start date
                var initialTillDate = item.StartDate.Value.AddMonths(item.Frequency.AdvanceByMonths);
                if (initialTillDate > tillDate)
                {
                    tillDate = initialTillDate;
                }
            }
            else
            {
                fromDate = DateHelper.GetNext(item.UptoDate.Value, periodicityType, periodicityValue, false);
            }

            if (tillDate > item.CompletionDate.Value)
            {
                tillDate = item.CompletionDate.Value;
            }

            if (periodicityType == "Once")
            {
                item.NextCreationDate = item.CompletionDate.Value;
            }
            else
            {
                item.NextCreationDate = DateHelper.GetNext(item.StartDate.Value, periodicityType, periodicityValue);
            }

            if (DateHelper.IsPast(tillDate) || tillDate <= fromDate)
            {
                item.UptoDate = fromDate;
                return;
            }

            var dates = DateHelper.GetDates(fromDate, tillDate, periodicityType, periodicityValue);

            log.Debug($"{logContext} {dates.Count} child(ren) needs to be created");

            foreach (var date in dates)
            {
                var status = UlxConfig.Get($"COB_STATUS_FOR_{item.ActionType}_INITIAL");

                if (DateHelper.IsPast(date))
                {
                    status = UlxConfig.Get($"COB_STATUS_FOR_{item.ActionType}_PAST");
                }

                var child = childObligationRepository.Create(new ChildObligation
                {
                    Date = DateHelper.ToWeekDay(date),
                    Status = status,
                    Parent = item,
                    IsActive = true,
                    ActionType = item.ActionType
                });

                item.UptoDate = date;
                CreateReminders(item, child);
            }

            log.Debug($"{logContext} Done");
        }

        private void CreateReminders(Obligation item, ChildObligation child)
        {
            var logContext = $"[CreateReminders:{child.Id}/{item.Frequency.Name}]";
            log.Debug($"{logContext} Started");
            var reminders = reminderFrequencyRepository.Search(new Queries.ReminderFrequencyQuery
            {
                FrequencyId = item.Frequency.Id,
                Names = UlxConfig.Get($"REMINDER_FREQUENCY_NAMES_FOR_{item.ActionType}")
            });

            log.Debug($"{logContext} {reminders.Count} reminder(s) needs to be created");

            foreach (var reminder in reminders)
            {
                var date = child.Date.AddDays(-reminder.DaysBack);

                date = DateHelper.ToWeekDay(date);
                if (DateHelper.IsPast(date))
                {
                    log.Debug($"{logContext} Skipping for {DateHelper.ToDateString(date)}. Reason: Is Past");
                    continue;
                }

                log.Debug($"{logContext} Creating for {DateHelper.ToDateString(date)}");
                var newReminderLog = obligationTriggerLogRepository.Create(new ObligationTriggerLog
                {
                    ChildObligation = child,
                    When = DateHelper.ToBOD(date),
                    Reminder = reminder
                });
            }
        }
    }
}