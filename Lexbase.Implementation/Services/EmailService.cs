using Lexbase.Helpers;
using Lexbase.Models;
using Lexbase.Providers;
using Lexbase.Providers.Models;
using Lexbase.Services;
using log4net;
using System;
using System.Collections.Generic;

namespace Lexbase.Implementation.Services
{
    public class EmailService : IEmailService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(EmailService));
        private readonly IEmailProvider emailProvider;
        private readonly ITemplateProvider templateProvider;

        private IDictionary<EmailType, string> bodyTemplates = new Dictionary<EmailType, string>
        {
            { EmailType.OBLIGATION_TRIGGER_TO_NO_TRIGGER, "ActivationEmail" },
            { EmailType.ACTIVATION_EMAIL, "ActivationEmail" },
            { EmailType.DE_ACTIVATION_EMAIL, "DeActivationEmail" }
        };

        class InjectModel
        {
            public class GroupForModel
            {
                public long Id { get; set; }
                public string Name { get; set; }
                public string Type { get; set; }
            }

            public class EmailsModel
            {
                public User COE { get; set; }
            }

            public class LinksModel
            {
                public string Application { get; set; }
                public string Account { get; set; }
                public string Reports { get; set; }
                public string Training { get; set; }
            }

            public string Date { get; set; }
            public User To { get; set; }
            public User From { get; set; }
            public GroupForModel GroupFor { get; set; }
            public Document Document { get; set; }
            public EmailsModel Emails { get; set; }
            public LinksModel Links { get; set; }
            public Account Account { get; set; }
            public MessageAttachment Attachment { get; set; }
            public ChangeRequest Request { get; set; }
            public string Subject { get; set; }
        }

        public EmailService(IEmailProvider emailProvider, ITemplateProvider templateProvider)
        {
            this.emailProvider = emailProvider;
            this.templateProvider = templateProvider;
        }

        public void Send(User to, string templateName, MessageAttachment attachment = null)
        {
            var template = templateProvider.GetEmailTemplate(templateName);

            var from = new User
            {
                Email = UlxConfig.Get("Emails:System:Email"),
                FirstName = UlxConfig.Get("Emails:System:FirstName"),
                LastName = UlxConfig.Get("Emails:System:LastName")
            };

            var inject = GetEmailData(to, from, attachment);
            var subject = template.GetSubject(inject);
            inject.Subject = subject;

            emailProvider.Send(new Message
            {
                Attachment = attachment,
                Subject = subject,
                Body = template.GetBody(inject)
            }, to, from);
        }

        public void Send(User to, string templateName, ChangeRequest item)
        {
            var template = templateProvider.GetEmailTemplate(templateName);

            var from = new User
            {
                Email = UlxConfig.Get("Emails:System:Email"),
                FirstName = UlxConfig.Get("Emails:System:FirstName"),
                LastName = UlxConfig.Get("Emails:System:LastName")
            };

            var inject = GetEmailData(to, from, null, item.Account, null, item);
            var subject = template.GetSubject(inject);
            inject.Subject = subject;

            emailProvider.Send(new Message
            {
                Subject = subject,
                Body = template.GetBody(inject)
            }, to, from);
        }

        public void Send(User to, string templateName, Account account, MessageAttachment attachment = null, Document document = null)
        {
            var template = templateProvider.GetEmailTemplate(templateName);

            var from = new User
            {
                Email = UlxConfig.Get("Emails:System:Email"),
                FirstName = UlxConfig.Get("Emails:System:FirstName"),
                LastName = UlxConfig.Get("Emails:System:LastName")
            };

            var inject = GetEmailData(to, from, attachment, account, document);
            var subject = template.GetSubject(inject);
            inject.Subject = subject;

            emailProvider.Send(new Message
            {
                Attachment = attachment,
                Subject = subject,
                Body = template.GetBody(inject)
            }, to, from);
        }

        private InjectModel GetEmailData(
            User to,
            User from,
            MessageAttachment attachment = null,
            Account account = null,
            Document document = null,
            ChangeRequest changeRequest = null
        )
        {
            var logContext = "[GetEmailData]";
            log.Debug($"{logContext} Started");

            var accountLink = "";
            if (account != null)
            {
                accountLink = LinkHelper.GetAccountLink(account);
            }

            return new InjectModel
            {
                Date = DateTime.Now.ToString(),
                To = to,
                From = from,
                GroupFor = new InjectModel.GroupForModel
                {
                    Id = account != null ? account.Id : 0,
                    Name = account != null ? account.Name : "",
                    Type = "Account"
                },
                Document = document,
                Emails = new InjectModel.EmailsModel
                {
                    COE = new User
                    {
                        Email = UlxConfig.Get("Emails:COE:Email"),
                        FirstName = UlxConfig.Get("Emails:COE:FirstName"),
                        LastName = UlxConfig.Get("Emails:COE:LastName")
                    }
                },
                Links = new InjectModel.LinksModel
                {
                    Application = LinkHelper.GetApplicationLink(),
                    Account = accountLink,
                    Reports = LinkHelper.GetReportsLink(),
                    Training = LinkHelper.GetTrainingsLink(),
                },
                Account = account,
                Attachment = attachment,
                Request = changeRequest,
                Subject = ""
            };
        }
    }
}