using Lexbase.Helpers;
using Lexbase.Models;
using Lexbase.Queries;
using Lexbase.Repositories;
using Lexbase.Services;
using log4net;
using System;
using System.Collections.Generic;

namespace Lexbase.Implementation.Services
{
    public class NotificationService : INotificationService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(NotificationService));
        private readonly ITimezoneRepository timezoneRepository;
        private readonly IAccountRepository accountRepository;
        private readonly IObligationTriggerLogRepository obligationTriggerLogRepository;
        private readonly IChangeRequestRepository changeRequestRepository;
        private readonly IUserRepository userRepository;
        private readonly IUserService userService;
        private readonly IObligationService obligationService;
        private readonly IExcelService excelService;
        private readonly IEmailService emailService;

        private enum NotificationGroup
        {
            Normal,
            DueToday,
            Alert,
            Overdue
        }

        public NotificationService(
            ITimezoneRepository timezoneRepository,
            IAccountRepository accountRepository,
            IObligationTriggerLogRepository obligationTriggerLogRepository,
            IChangeRequestRepository changeRequestRepository,
            IUserRepository userRepository,
            IUserService userService,
            IObligationService obligationService,
            IExcelService excelService,
            IEmailService emailService
        )
        {
            this.timezoneRepository = timezoneRepository;
            this.accountRepository = accountRepository;
            this.obligationTriggerLogRepository = obligationTriggerLogRepository;
            this.changeRequestRepository = changeRequestRepository;
            this.userRepository = userRepository;
            this.userService = userService;
            this.obligationService = obligationService;
            this.excelService = excelService;
            this.emailService = emailService;
        }

        public void SendChangeRequests(int timezoneId)
        {
            var logContext = $"[Send:{timezoneId}]";
            log.Debug($"{logContext} Started");


            log.Debug($"{logContext} getting accounts by time zone");

            List<Account> accounts = accountRepository.Search(new Queries.AccountQuery
            {
                TimezoneId = timezoneId
            }) as List<Account>;

            foreach (var account in accounts)
            {

                account.Managers = userService.GetManagers(account.Id);
                account.NonManagers = userService.GetUsers(account.Id, account.Managers as List<User>);

                var userIds = new List<long>();
                foreach (var user in account.NonManagers)
                {
                    userIds.Add(user.Id);
                }

                var changes = changeRequestRepository.Search(new UserQuery
                {
                    UserIds = userIds
                });

                foreach (var item in changes)
                {
                    item.Account = account;
                    item.Link = LinkHelper.GetChangeRequestLink(item);

                    foreach (var user in account.Managers)
                    {
                        emailService.Send(user, "Notification-CR", item);
                    }
                }
            }
        }

        public bool Send(int timezoneId)
        {
            var logContext = $"[Send:{timezoneId}]";
            log.Debug($"{logContext} Started");

            var timezone = timezoneRepository.Get(timezoneId);

            log.Debug($"{logContext} getting accounts by time zone");

            List<Account> accounts = accountRepository.Search(new Queries.AccountQuery
            {
                TimezoneId = timezoneId
            }) as List<Account>;

            var accountIds = new List<long>();
            foreach (var item in accounts)
            {
                accountIds.Add(item.Id);
            }
            var NotificationOffsetDaysValue = UlxConfig.GetInteger($"Timezones:{timezoneId}:NotificationOffsetDays", 0);

            var when = DateHelper.ToBOD(DateTime.Today.AddDays(NotificationOffsetDaysValue), timezone);

            var triggers = obligationTriggerLogRepository.Search(new Queries.ObligationTriggerLogQuery
            {
                AccountIds = accountIds,
                When = when
            });

            log.Debug($"{logContext} found {triggers.Count} trigger(s) for fetched accounts");

            if (triggers.Count == 0)
            {
                return false;
            }

            var managers = new Dictionary<long, IList<User>>();
            var notifications = new Dictionary<long, IDictionary<NotificationGroup, IList<ObligationTriggerLog>>>();

            log.Debug($"{logContext} organizing trigger logs");
            foreach (var item in triggers)
            {
                var account = accounts.FindLast(i => i.Id == item.ChildObligation.Parent.Account.Id);
                item.ChildObligation.Parent.Account = account;

                var group = NotificationGroup.Normal;
                if (DateHelper.IsPast(item.ChildObligation.Date))
                {
                    group = NotificationGroup.Overdue;
                }
                else if (item.Reminder.Status == "escalated" || DateHelper.IsToday(item.ChildObligation.Date))
                {
                    group = NotificationGroup.DueToday;
                }

                if (item.ChildObligation.ActionType == "A")
                {
                    group = NotificationGroup.Alert;
                }

                if (item.ChildObligation.Parent.OwnerUser != null)
                {
                    item.ChildObligation.Parent.OwnerUser = userRepository.Get(item.ChildObligation.Parent.OwnerUser.Id);
                    GroupNotification(notifications, item, item.ChildObligation.Parent.OwnerUser.Id, group);
                }

                if (item.ChildObligation.Parent.OwnerManager != null)
                {
                    item.ChildObligation.Parent.OwnerManager = userRepository.Get(item.ChildObligation.Parent.OwnerManager.Id);
                    if (group == NotificationGroup.Overdue || group == NotificationGroup.DueToday)
                    {
                        GroupNotification(notifications, item, item.ChildObligation.Parent.OwnerManager.Id, group);
                    }
                }

                if (group == NotificationGroup.Overdue)
                {
                    var accountManagers = GetManagers(managers, account.Id);
                    foreach (var manager in accountManagers)
                    {
                        GroupNotification(notifications, item, manager.Id, group);
                    }
                }

                obligationService.SetStatus(item.ChildObligation, item);
            }
            log.Debug($"{logContext} {notifications.Keys.Count} notification(s) to be sent");

            log.Debug($"{logContext} getting users for the notifications ");
            var userIds = new List<long>();
            foreach (var item in notifications.Keys)
            {
                userIds.Add(item);
            }

            var users = userRepository.Search(new Queries.UserQuery
            {
                UserIds = userIds
            }) as List<User>;

            foreach (var userId in notifications.Keys)
            {
                var user = users.FindLast(i => i.Id == userId);

                if (user == null)
                {
                    log.Debug($"{logContext} Not Sending Email. Reason: user id - {userId} does not exist");
                    continue;
                }

                log.Debug($"{logContext} creating excel for user: {userId}");
                var item = notifications[userId];

                var excel = new Dictionary<string, IList<ObligationTriggerLog>>();

                var templateLevel = "Normal";

                if (item.ContainsKey(NotificationGroup.Normal))
                {
                    excel["Notifications"] = item[NotificationGroup.Normal];
                    templateLevel = "Normal";
                }

                if (item.ContainsKey(NotificationGroup.Alert))
                {
                    excel["Alert"] = item[NotificationGroup.Alert];
                    templateLevel = "Alert";
                }

                if (item.ContainsKey(NotificationGroup.Overdue))
                {
                    excel["Overdue"] = item[NotificationGroup.Overdue];
                    templateLevel = "Overdue";
                }

                if (item.ContainsKey(NotificationGroup.DueToday))
                {
                    excel["DueToday"] = item[NotificationGroup.DueToday];
                    templateLevel = "DueToday";
                }

                var attachment = excelService.Create(excel, $"{templateLevel}Notification");

                emailService.Send(user, $"Notification-{templateLevel}", attachment);
            }

            foreach (var item in triggers)
            {
                obligationTriggerLogRepository.SetSent(item.Id, when);
            }
            log.Debug($"{logContext} Done");

            return true;
        }

        /// <summary>
        /// Lazy loaded managers for account
        /// </summary>
        /// <param name="managers"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        private IList<User> GetManagers(IDictionary<long, IList<User>> managers, long accountId)
        {
            if (!managers.ContainsKey(accountId))
            {
                managers[accountId] = accountRepository.GetManagers(accountId);
            }

            return managers[accountId];
        }

        private void GroupNotification(
            IDictionary<long, IDictionary<NotificationGroup, IList<ObligationTriggerLog>>> itemsMap,
            ObligationTriggerLog item,
            long userId,
            NotificationGroup group
            )
        {
            if (!itemsMap.ContainsKey(userId))
            {
                itemsMap[userId] = new Dictionary<NotificationGroup, IList<ObligationTriggerLog>>();
            }

            var userItems = itemsMap[userId];

            if (!userItems.ContainsKey(group))
            {
                userItems[group] = new List<ObligationTriggerLog>();
            }

            var groupedItems = userItems[group];

            groupedItems.Add(item);
        }
    }
}