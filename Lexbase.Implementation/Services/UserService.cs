using Lexbase.Models;
using Lexbase.Queries;
using Lexbase.Repositories;
using Lexbase.Services;
using log4net;
using System.Collections.Generic;

namespace Lexbase.Implementation.Services
{
    public class UserService : IUserService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(UserService));
        private readonly IUserRepository userRepository;
        private readonly IAccountRepository accountRepository;
        private readonly IObligationRepository obligationRepository;
        private readonly IEmailService emailService;

        public UserService(
            IUserRepository userRepository,
            IAccountRepository accountRepository,
            IObligationRepository obligationRepository,
            IEmailService emailService
        )
        {
            this.userRepository = userRepository;
            this.accountRepository = accountRepository;
            this.obligationRepository = obligationRepository;
            this.emailService = emailService;
        }

        public void Activate(User user)
        {
            var logContext = $"[Activate:{user.Id}]";

            if (user.Active)
            {
                log.Debug($"{logContext} Aborting. Reason:Already Active");
                return;
            }

            log.Debug($"{logContext} Activating");
            emailService.Send(user, "Welcome-User");
            userRepository.CompleteActivation(user.Id);
            user.Active = true;
            log.Info($"{logContext} Complete");
        }

        public IList<User> GetManagers(long accountId)
        {
            var managers = accountRepository.GetManagers(accountId);
            return Populate(managers as List<User>);
        }

        public IList<User> GetUsers(long accountId, List<User> exclude = null)
        {
            var users = obligationRepository.GetUsers(accountId);

            return Populate(users as List<User>, exclude);
        }

        private IList<User> Populate(List<User> items, List<User> exclude = null)
        {
            var userIds = new List<long>();

            if (exclude == null)
            {
                exclude = new List<User>();
            }

            foreach (var item in items)
            {
                if (exclude.FindLast(i => i.Id == item.Id) == null)
                {
                    userIds.Add(item.Id);
                }
            }

            if (userIds.Count == 0)
            {
                return new List<User>();
            }

            var users = userRepository.Search(new UserQuery
            {
                UserIds = userIds
            });

            foreach (var item in users)
            {
                var managerUser = items.FindLast(i => i.Id == item.Id);

                if (managerUser != null)
                {
                    item.ManagerType = managerUser.ManagerType;
                }
            }

            return users;
        }

        public IList<User> GetManagersForEmail(Account account, EmailType emailType)
        {
            var managerList = new List<User>();

            switch (emailType)
            {
                case EmailType.OBLIGATION_TRIGGER_TO_NO_TRIGGER:
                    managerList.AddRange(account.AccountManagerList);
                    managerList.Add(account.LeadManager);
                    managerList.Add(account.AccountExecutive);
                    break;

                case EmailType.ACTIVATION_EMAIL:
                    managerList.Add(account.LeadManager);
                    break;
            }

            return managerList;
        }
    }
}