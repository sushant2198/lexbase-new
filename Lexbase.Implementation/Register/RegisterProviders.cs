﻿using Lexbase.Implementation.Providers;
using Lexbase.Providers;
using Lexbase.Register;
using Microsoft.Extensions.DependencyInjection;

namespace Lexbase.Implementation.Register
{
    public class RegisterProviders : IRegister
    {
        public void Register(ServiceCollection collection)
        {
            collection.AddScoped<IExcelProvider, ExcelProvider>();
            collection.AddScoped<IFileProvider, FileProvider>();
            collection.AddScoped<ITemplateProvider, TemplateProvider>();
            collection.AddScoped<IEmailProvider, SMTPProvider>();
            collection.AddScoped<ICacheProvider, RedisCache>();
            collection.AddScoped<IIndexProvider, IndexProvider>();
            collection.AddDistributedRedisCache(options =>
            {
                options.Configuration = UlxConfig.GetConnectionString("REDIS");
            });
            collection.AddScoped<IBlobProvider, BlobProvider>();
        }
    }
}