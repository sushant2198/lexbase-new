using Lexbase.Implementation.Jobs;
using Lexbase.Jobs;
using Lexbase.Register;
using Microsoft.Extensions.DependencyInjection;

namespace Lexbase.Implementation.Register
{
    public class RegisterJobs : IRegister
    {
        public void Register(ServiceCollection collection)
        {
            collection.AddScoped<IAccountActivationProcessJob, ActivationProcessJob>();
            collection.AddScoped<INotificationJob, NotificationJob>();
            collection.AddScoped<ISchedulerJob, SchedulerJob>();
            collection.AddScoped<IAccountStatusProcessJob, AccountStatusProcessJob>();
            collection.AddScoped<IMarkOverDueJob, MarkOverDueJob>();
            collection.AddScoped<ICOBReportJob, COBReportJob>();
            collection.AddScoped<IHeatMapReportJob, HeatMapReportJob>();
            collection.AddScoped<IAccountDumpJob, AccountDumpJob>();
            collection.AddScoped<ITrendReportJob, TrendReportJob>();
            collection.AddScoped<IInformationReportJob, InformationReportJob>();
            collection.AddScoped<IChangeRequestNotificationJob, ChangeRequestNotificationJob>();
            collection.AddScoped<IReIndexJob, ReIndexJob>();
            collection.AddScoped<IScheduledReportJob, ScheduledReportJob>();
            collection.AddScoped<IUserAuditLogJob, UserAuditLogJob>();
            collection.AddScoped<IDeleteTempBlobJob, DeleteTempBlobJob>();

        }
    }
}