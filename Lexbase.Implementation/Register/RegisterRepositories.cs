using Lexbase.Implementation.Repositories;
using Lexbase.Register;
using Lexbase.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Lexbase.Implementation.Register
{
    public class RegisterRepositories : IRegister
    {
        public void Register(ServiceCollection collection)
        {
            collection.AddScoped<IUserRepository, UserRepository>();
            collection.AddScoped<IAccountRepository, AccountRepository>();
            collection.AddScoped<IAccountReportRepository, AccountReportRepository>();

            collection.AddScoped<IObligationRepository, ObligationRepository>();
            collection.AddScoped<IChildObligationRepository, ChildObligationRepository>();
            collection.AddScoped<IReminderFrequencyRepository, ReminderFrequencyRepository>();
            collection.AddScoped<IObligationTriggerLogRepository, ObligationTriggerLogRepository>();

            collection.AddScoped<IConfigPropertyRepository, ConfigPropertyRepository>();

            collection.AddScoped<ISARepository, SARepository>();
            collection.AddScoped<IMSARepository, MSARepository>();

            collection.AddScoped<IAccountTypeRepository, AccountTypeRepository>();
            collection.AddScoped<IAccountCoverageRepository, AccountCoverageRepository>();
            collection.AddScoped<IAccountCurrencyRepository, AccountCurrencyRepository>();
            collection.AddScoped<IBusinessUnitRepository, BusinessUnitRepository>();
            collection.AddScoped<IDeliveryCountryRepository, DeliveryCountryRepository>();
            collection.AddScoped<IVerticalRepository, VerticalRepository>();
            collection.AddScoped<ITimezoneRepository, TimezoneRepository>();
            collection.AddScoped<IChangeRequestRepository, ChangeRequestRepository>();
            collection.AddScoped<IScheduledReportRepository, ScheduledReportRepository>();
            collection.AddScoped<IUserAuditLogRepository, UserAuditLogRepository>();
        }
    }
}