using Lexbase.Implementation.Services;
using Lexbase.Register;
using Lexbase.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Lexbase.Implementation.Register
{
    public class RegisterServices : IRegister
    {
        public void Register(ServiceCollection collection)
        {
            collection.AddScoped<IObligationService, ObligationService>();
            collection.AddScoped<IUserService, UserService>();
            collection.AddScoped<IAccountService, AccountService>();
            collection.AddScoped<ISAService, SAService>();
            collection.AddScoped<IExcelService, ExcelService>();
            collection.AddScoped<IEmailService, EmailService>();
            collection.AddScoped<INotificationService, NotificationService>();

            collection.AddScoped<IAccountReportService, AccountReportService>();
            collection.AddScoped<IIndexingService, IndexingService>();
        }
    }
}