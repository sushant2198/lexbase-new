using Lexbase.Models;
using Lexbase.Queries;
using Lexbase.Repositories;
using log4net;
using System.Collections.Generic;

namespace Lexbase.Implementation.Repositories
{
    public class BusinessUnitRepository : RepositoryBase, IBusinessUnitRepository
    {
        private readonly ILog log = LogManager.GetLogger(typeof(BusinessUnitRepository));

        public BusinessUnitRepository() : base("LBACCOUNTS")
        {
        }

        public BusinessUnit Get(long id)
        {
            return FindOne("USP_JOBS_GET_ACCOUNT_BUSINESS_UNIT_BY_ID", new Dictionary<string, object> { { "ACCOUNT_BUSINESS_UNIT_ID", id } }, row =>
            {
                return new BusinessUnit
                {
                    Id = (long)row.ID,
                    Name = row.NAME
                };
            });
        }

        public IList<BusinessUnit> Search(BusinessUnitQuery query)
        {
            return Find("USP_JOBS_GET_ACCOUNT_BUSINESS_UNITS_BY_ACCOUNT_ID", new Dictionary<string, object> { { "ACCOUNT_ID", query.AccountId } }, row =>
            {
                return new BusinessUnit
                {
                    Id = (long)row.ID,
                    Name = row.NAME
                };
            });
        }
    }
}