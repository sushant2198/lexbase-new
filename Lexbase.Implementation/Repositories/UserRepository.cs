using Lexbase.Models;
using Lexbase.Queries;
using Lexbase.Repositories;
using log4net;
using System.Collections.Generic;

namespace Lexbase.Implementation.Repositories
{
    public class UserRepository : RepositoryBase, IUserRepository
    {
        private readonly ILog log = LogManager.GetLogger(typeof(UserRepository));
        private readonly IDictionary<long, User> cache = new Dictionary<long, User>();

        public UserRepository() : base("LBUSER")
        {
        }

        public User Get(long id)
        {
            if (!cache.ContainsKey(id))
            {
                cache[id] = FindOne("USP_JOBS_GET_USER_BY_ID", new Dictionary<string, object> { { "USER_ID", id } }, Mapper);
            }
            return cache[id];
        }

        public IList<User> Search(UserQuery query)
        {
            var procedure = "USP_JOBS_GET_USERS";
            var parameters = new Dictionary<string, object>();
            if (query.UserIds != null)
            {
                if (query.UserIds.Count == 0)
                {
                    return new List<User>();
                }

                procedure = "USP_JOBS_GET_USERS_BY_IDS";
                parameters["IDS"] = string.Join(',', query.UserIds);
            }

            return Find(procedure, parameters, Mapper);
        }

        public void DeActivate(long id)
        {
            // Execute("USP_JOBS_COMPLETE_USER_DEACTIVATE_BY_ID", new Dictionary<string, object> { { "USER_ID", id } });
        }

        public void StartActivation(long id)
        {
            // Execute("USP_JOBS_START_USER_ACTIVATION_BY_ID", new Dictionary<string, object> { { "USER_ID", id } });
        }

        public void CompleteActivation(long id)
        {
            Execute("USP_JOBS_COMPLETE_USER_ACTIVATION_BY_ID", new Dictionary<string, object> { { "USER_ID", id } });
        }

        private User Mapper(dynamic row)
        {
            var item = new User
            {
                Id = (long)row.ID,
                FirstName = row.FIRST_NAME,
                MiddleName = row.MIDDLE_NAME,
                LastName = row.LAST_NAME,
                SensitiveAccess = (row.SENSITIVE_ACCESS ?? 0) != 0,
                Email = row.EMAIL,
                Active = (row.IS_ACTIVE ?? 0) != 0
            };

            item.FullName = $"{item.FirstName} {item.LastName}";

            if (!item.Active)
            {
                log.Info($"User Id: {item.Id}, Name: {item.FirstName} {item.LastName} needs to be activated");
            }

            return item;
        }
    }
}