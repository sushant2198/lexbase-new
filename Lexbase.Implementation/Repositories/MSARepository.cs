using Lexbase.Models;
using Lexbase.Queries;
using Lexbase.Repositories;
using log4net;
using System.Collections.Generic;

namespace Lexbase.Implementation.Repositories
{
    public class MSARepository : RepositoryBase, IMSARepository
    {
        private readonly ILog log = LogManager.GetLogger(typeof(MSARepository));

        private readonly IDictionary<long, MServiceAgreement> cache = new Dictionary<long, MServiceAgreement>();

        public MSARepository() : base("LBMSVCAGREEMENTS")
        {
        }

        public void CompleteActivation(long id)
        {
            Execute("USP_JOBS_COMPLETE_MSA_ACTIVATION_BY_ID", new Dictionary<string, object> { { "MSA_ID", id } });
        }

        public void StartActivation(long id)
        {
            // Execute("USP_JOBS_START_MSA_ACTIVATION_BY_ID", new Dictionary<string, object> { { "MSA_ID", id } });
        }

        public MServiceAgreement Get(long id)
        {
            if (!cache.ContainsKey(id))
            {
                cache[id] = FindOne<MServiceAgreement>("USP_JOBS_GET_MSA_BY_ID", new Dictionary<string, object> { { "MSA_ID", id } }, Mapper);
            }
            return cache[id];
        }

        public IList<MServiceAgreement> Search(MServiceAgreementQuery query)
        {
            var procedure = "USP_JOBS_GET_MSAS";
            var parameters = new Dictionary<string, object>();

            if (query.NeedActivation)
            {
                procedure = "USP_JOBS_GET_MSAS_NEEDS_ACTIVATION";
            }

            if (query.ParentId.HasValue)
            {
                procedure = "USP_JOBS_GET_MSAS_BY_PARENTID";
                parameters["PARENTID"] = query.ParentId.Value;
                parameters["PARENTTYPE"] = (long)query.ParentType.Value;
            }

            if (query.AccountId.HasValue && !query.IsActive)
            {
                procedure = "USP_JOBS_GET_MSAS_BY_ACCOUNT_ID";
                parameters["ACCOUNT_ID"] = query.AccountId.Value;
            }

            if (query.AccountId.HasValue && query.IsActive)
            {
                procedure = "USP_JOBS_GET_MSAS";
                parameters["ACCOUNTID"] = query.AccountId.Value;
            }

            return Find<MServiceAgreement>(procedure, parameters, Mapper);
        }

        private MServiceAgreement Mapper(dynamic row)
        {
            var item = new MServiceAgreement
            {
                Id = (long)row.ID,
                Name = row.NAME,
                IsActive = (row.IS_ACTIVE ?? 0) == 0 ? false : true,
                Description = row.NAME,
                Status = row.STATUS,
                Account = new Account { Id = (long)(row.ACCOUNT_ID ?? 0) }
                // TODO: Populate
            };
            return item;
        }
    }
}