using Lexbase.Models;
using Lexbase.Queries;
using Lexbase.Repositories;
using System.Collections.Generic;

namespace Lexbase.Implementation.Repositories
{
    public class TimezoneRepository : RepositoryBase, ITimezoneRepository
    {
        public TimezoneRepository() : base("LBACCOUNTS")
        {
        }

        public Timezone Get(long id)
        {
            return FindOne("USP_JOBS_GET_TIMEZONE_BY_ID", new Dictionary<string, object> { { "TIMEZONEID", id } }, Mapper);
        }

        public IList<Timezone> Search(TimezoneQuery query)
        {
            return Find("USP_JOBS_GET_TIMEZONES", null, Mapper);
        }

        private Timezone Mapper(dynamic row)
        {
            return new Timezone
            {
                Id = (long)row.ID,
                Name = row.NAME,
                Description = row.DESCRIPTION,
                Value = row.VALUE
            };
        }
    }
}