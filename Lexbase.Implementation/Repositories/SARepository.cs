using Lexbase.Models;
using Lexbase.Queries;
using Lexbase.Repositories;
using log4net;
using System.Collections.Generic;

namespace Lexbase.Implementation.Repositories
{
    public class SARepository : RepositoryBase, ISARepository
    {
        private readonly ILog log = LogManager.GetLogger(typeof(SARepository));
        private readonly IDictionary<long, ServiceAgreement> cache = new Dictionary<long, ServiceAgreement>();

        public SARepository() : base("LBSVCAGREEMENTS")
        {
        }

        public void CompleteActivation(long id)
        {
            Execute("USP_JOBS_COMPLETE_SA_ACTIVATION_BY_ID", new Dictionary<string, object> { { "SA_ID", id } });
        }

        public void StartActivation(long id)
        {
            // Execute("USP_JOBS_START_SA_ACTIVATION_BY_ID", new Dictionary<string, object> { { "SA_ID", id } });
        }

        public ServiceAgreement Get(long id)
        {

            if (!cache.ContainsKey(id))
            {
                cache[id] = FindOne<ServiceAgreement>("USP_JOBS_GET_SA_BY_ID",
                new Dictionary<string, object> { { "SA_ID", id } }, Mapper);
            }
            return cache[id];

        }

        public IList<ServiceAgreement> Search(ServiceAgreementQuery query)
        {
            var procedure = "USP_JOBS_GET_SAS";
            var parameters = new Dictionary<string, object>();

            if (query.NeedActivation)
            {
                procedure = "USP_JOBS_GET_SAS_NEEDS_ACTIVATION";
            }

            if (query.ParentId.HasValue)
            {
                procedure = "USP_JOBS_GET_SAS_BY_PARENTID";
                parameters["PARENTID"] = query.ParentId.Value;
                parameters["PARENTTYPE"] = (long)query.ParentType.Value;
            }

            if (query.AccountId.HasValue && !query.IsActive)
            {
                procedure = "USP_JOBS_GET_SAS_BY_ACCOUNT_ID";
                parameters["ACCOUNT_ID"] = query.AccountId.Value;
            }

            if (query.AccountId.HasValue && query.IsActive)
            {
                procedure = "USP_JOBS_GET_SAS";
                parameters["ACCOUNTID"] = query.AccountId.Value;
            }

            return Find<ServiceAgreement>(procedure, parameters, Mapper);
        }

        private ServiceAgreement Mapper(dynamic row)
        {
            var item = new ServiceAgreement
            {
                Id = (long)row.ID,
                Name = row.NAME,
                Account = new Account { Id = (long)(row.ACCOUNT_ID ?? 0) },
                IsActive = (row.IS_ACTIVE ?? 0) == 0 ? false : true,
                // TODO: Populate
            };
            return item;
        }
    }
}