﻿using Lexbase.Models;
using Lexbase.Queries;
using Lexbase.Repositories;
using log4net;
using System.Collections.Generic;

namespace Lexbase.Implementation.Repositories
{
    public class ScheduledReportRepository : RepositoryBase, IScheduledReportRepository
    {
        private readonly ILog log = LogManager.GetLogger(typeof(UserRepository));
        private readonly IDictionary<long, User> cache = new Dictionary<long, User>();

        public ScheduledReportRepository() : base("LBUSER")
        {
        }

        public void Clean()
        {
            Execute("USP_JOBS_CLEAN_SCHEDULEDREPORTS", new Dictionary<string, object> { });
        }

        public void StartProcessing(long id)
        {
            Execute("USP_JOBS_START_SCHEDULEDREPORT_BY_ID", new Dictionary<string, object> {
                { "scheduleId", id }
            });
        }

        public void CompleteProcessing(long id, string error)
        {
            Execute("USP_JOBS_COMPLETE_SCHEDULEDREPORT_BY_ID", new Dictionary<string, object> {
                { "scheduleId", id },
                { "error", error },
            });
        }

        public ScheduledReport Get(long id)
        {
            throw new System.NotImplementedException();
        }


        public IList<ScheduledReport> Search(ScheduledReportQuery query)
        {
            var procedure = "USP_JOBS_GET_SCHEDULEDREPORTS";
            var parameters = new Dictionary<string, object>();

            return Find(procedure, parameters, Mapper);
        }

        private ScheduledReport Mapper(dynamic row)
        {
            var item = new ScheduledReport
            {
                Id = (long)row.ID,
                JobName = row.JOBNAME,
                JobParam = row.JOBPARAM,
                Error = row.ERROR,
                StartedOn = row.STARTED_DATE,
                CompletedOn = row.COMPLETED_DATE
            };

            return item;
        }
    }
}