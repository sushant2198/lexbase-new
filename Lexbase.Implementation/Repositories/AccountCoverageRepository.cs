using Lexbase.Models;
using Lexbase.Queries;
using Lexbase.Repositories;
using log4net;
using System.Collections.Generic;

namespace Lexbase.Implementation.Repositories
{
    public class AccountCoverageRepository : RepositoryBase, IAccountCoverageRepository
    {
        private readonly ILog log = LogManager.GetLogger(typeof(AccountCoverageRepository));

        public AccountCoverageRepository() : base("LBACCOUNTS")
        {
        }

        public AccountCoverage Get(long id)
        {
            return FindOne("USP_JOBS_GET_ACCOUNT_COVERAGE_BY_ID", new Dictionary<string, object> { { "ACCOUNT_COVERAGE_ID", id } }, row =>
            {
                return new AccountCoverage
                {
                    Id = (long)row.ID,
                    Name = row.NAME
                };
            });
        }

        public IList<AccountCoverage> Search(AccountCoverageQuery query)
        {
            return Find("USP_JOBS_GET_ACCOUNT_COVERAGES_BY_ACCOUNT_ID", new Dictionary<string, object> { { "ACCOUNT_ID", query.AccountId.Value } }, row =>
            {
                return new AccountCoverage
                {
                    Id = (long)row.ID,
                    Name = row.NAME
                };
            });
        }
    }
}