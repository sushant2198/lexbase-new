using Lexbase.Models;
using Lexbase.Queries;
using Lexbase.Repositories;
using log4net;
using System.Collections.Generic;

namespace Lexbase.Implementation.Repositories
{
    public class DeliveryCountryRepository : RepositoryBase, IDeliveryCountryRepository
    {
        private readonly ILog log = LogManager.GetLogger(typeof(DeliveryCountryRepository));

        public DeliveryCountryRepository() : base("LBACCOUNTS")
        {
        }

        public DeliveryCountry Get(long id)
        {
            return FindOne("USP_JOBS_GET_DELIVERY_COUNTRY_BY_ID", new Dictionary<string, object> { { "DELIVERY_COUNTRY_ID", id } }, row =>
            {
                return new DeliveryCountry
                {
                    CountryId = (long)row.COUNTRY_ID,
                    CountryName = row.COUNTRY_NAME
                };
            });
        }

        public IList<DeliveryCountry> Search(DeliveryCountryQuery query)
        {
            return Find("USP_JOBS_GET_DELIVERY_COUNTRIES_BY_ACCOUNT_ID", new Dictionary<string, object> { { "ACCOUNT_ID", query.AccountId } }, row =>
            {
                return new DeliveryCountry
                {
                    CountryId = (long)(row.COUNTRY_ID ?? 0),
                    CountryName = row.COUNTRY_NAME
                };
            });
        }
    }
}