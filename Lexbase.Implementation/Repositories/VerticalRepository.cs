using Lexbase.Models;
using Lexbase.Queries;
using Lexbase.Repositories;
using log4net;
using System.Collections.Generic;

namespace Lexbase.Implementation.Repositories
{
    public class VerticalRepository : RepositoryBase, IVerticalRepository
    {
        private readonly ILog log = LogManager.GetLogger(typeof(VerticalRepository));

        public VerticalRepository() : base("LBACCOUNTS")
        {
        }

        public Vertical Get(long id)
        {
            return FindOne("USP_JOBS_GET_VERTICAL_BY_ID", new Dictionary<string, object> { { "VERTICAL_ID", id } }, row =>
            {
                return new Vertical
                {
                    Id = (long)row.ID,
                    Name = row.NAME
                };
            });
        }

        public IList<Vertical> Search(VerticalQuery query)
        {
            return Find("USP_JOBS_GET_VERTICALS_BY_ACCOUNT_ID", new Dictionary<string, object> { { "ACCOUNT_ID", query.AccountId } }, row =>
            {
                return new Vertical
                {
                    Id = (long)row.ID,
                    Name = row.NAME
                };
            });
        }
    }
}