﻿using Lexbase.Models;
using Lexbase.Queries;
using Lexbase.Repositories;
using log4net;
using System;
using System.Collections.Generic;

namespace Lexbase.Implementation.Repositories
{
    public class ObligationRepository : RepositoryBase, IObligationRepository
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ObligationRepository));
        private readonly IUserRepository userRepository;

        public ObligationRepository(
            IUserRepository userRepository
        ) : base("LBOBLIGATIONS")
        {
            this.userRepository = userRepository;
        }

        public void CompleteActivation(long id)
        {
            Execute("USP_JOBS_COMPLETE_OBLIGATIONS_ACTIVATION_BY_ID", new Dictionary<string, object> { { "OBLIGATION_ID", id } });
        }

        public void StartActivation(long id)
        {
            // Execute("USP_JOBS_START_OBLIGATIONS_ACTIVATION_BY_ID", new Dictionary<string, object> { { "OBLIGATION_ID", id } });
        }

        public IList<User> GetUsers(long accountId)
        {
            return Find("USP_JOBS_GET_USERS_BY_ACCOUNT_ID", new Dictionary<string, object> { { "ACCOUNTID", accountId } }, row =>
            {
                if (row.ID == null)
                {
                    return null;
                }
                return new User
                {
                    Id = (long)(row.ID ?? 0)
                };
            });
        }

        public string GetTimeZoneForObligation(Obligation item)
        {
            throw new NotImplementedException();
        }

        public Obligation Get(long id)
        {
            return FindOne<Obligation>("USP_JOBS_GET_OBLIGATION_BY_ID", new Dictionary<string, object> { { "OBLIGATION_ID", id } }, row =>
            {
                return Map(row);
            });
        }

        private Obligation Map(dynamic row)
        {
            var item = new Obligation
            {
                Id = (long)row.ID,
                Name = row.NAME,
                IsActive = (row.IS_ACTIVE ?? 0) == 0 ? false : true,
                InProcessing = (row.IN_PROCESSING ?? 0) == 0 ? false : true,

                ActionType = row.ACTION_TYPE,
                IsTrigger = (row.IS_TRIGGER ?? 0) == 0 ? false : true,
                IsFinancial = (row.FINANCIAL ?? 0) == 0 ? false : true,
                Status = row.STATUS,

                StartDate = row.START_DATE,
                CompletionDate = row.COMPLETION_DATE,

                UptoDate = row.UPTO_DATE,
                NextCreationDate = row.NEXT_CREATION_DATE,

                NextDueDate = row.NEXT_DUE_DATE,

                RulesUpdated = (int)(row.RULES_UPDATED ?? 0),

                DateCreated = row.DATE_CREATED,
                DateModified = row.DATA_MODIFIED,
                //IsDueDateSet = row.IS_DUE_DATE_SET,

                //DataXML = row.DATAXML,
                //NextDueChildId = row.NEXT_DUE_CHILD_ID,
                //LastCompletedChildId = row.LAST_COMPLETED_CHILD_ID,
                //DataHistory = row.DATA_HISTORY,
                //GlActive = row.GL_ACTIVE,
                //CurrentOccurence = row.CURRENT_OCCURENCE,
                //Delegated = row.DELEGATED,
                //DelegateTo = row.DELEGATE_TO,
                //ResetDate = row.RESET_DATE,
                //FinancialPenalty = row.FINANCIAL_PENALTY,
                //ApprovalRequired = row.APPROVAL_REQUIRED,
                //ActionTypeManual = row.ACTION_TYPE_MANUAL,

                //DateShared = row.DATE_SHARED,
                //SharedBy = row.SHARED_BY,

                //IsTransitionMilestone = row.IS_TRANSITION_MILESTONE,
                //AlternateCompletionGroup = row.ALTERNATE_COMPLETION_GROUP,
                //CompletionGroup = row.COMPLETION_GROUP,
                //Extract = row.EXTRACT,
                //DocumentNumber = row.DOCUMENT_NUMBER,
                //Document_Type = row.DOCUMENT_TYPE,
                //ContractName = row.CONTRACT_NAME,
                //AccessEmpty = row.ACCESS_EMPTY,
                //GlAccessEmpty = row.GL_ACCESS_EMPTY,
                //ReferenceSection = row.REFERENCE_SECTION,
                //TriggerText = row.TRIGGER_TEXT,
                //NewAccess = row.NEW_ACCESS,
                //CrInprogress = row.CR_INPROGRESS,
                //Sla = row.SLA,
                //CrossReferenced = row.CROSS_REFERENCED,
                //TriggeringEvent = row.TRIGGERING_EVENT,
                //FromMaster = row.FROM_MASTER,
            };

            if (row.FREQUENCY_ID != null)
            {
                item.Frequency = new Frequency
                {
                    Id = (long)(row.FREQUENCY_ID ?? 0),
                    Name = row.FREQUENCY_NAME,
                    AdvanceByMonths = (int)(row.FREQUENCY_ADVANCE_BY_MONTHS ?? 0)
                };
            }

            if (row.MANAGER_ID != null)
            {
                item.OwnerManager = new User
                {
                    Id = (long)(row.MANAGER_ID ?? 0)
                };
            }

            if (row.SHARED_WITH_ID != null)
            {
                item.SharedWith = new User
                {
                    Id = (long)(row.SHARED_WITH_ID ?? 0)
                };
            }

            if (row.OWNER_ID != null)
            {
                item.OwnerUser = new User
                {
                    Id = (long)(row.OWNER_ID ?? 0)
                };
            }

            if (row.ACCOUNT_ID != null)
            {
                item.Account = new Account
                {
                    Id = (long)(row.ACCOUNT_ID ?? 0)
                };
            }

            if (row.GROUP_FOR_ID != null)
            {
                item.GroupForId = (long)(row.GROUP_FOR_ID ?? 0);

                if (row.GROUP_FOR != null)
                {
                    item.GroupFor = (GroupFor)row.GROUP_FOR;
                }
            }

            if (row.PHASE_NAME != null || row.PHASE_ID != null)
            {
                item.Phase = new Phase
                {
                    Id = (long)(row.PHASE_ID ?? 0),
                    Name = row.PHASE_NAME
                };
            }


            if (row.TYPE_ID != null || row.TYPE_NAME != null)
            {
                item.Type = new ObligationType
                {
                    Id = (long)(row.TYPE_ID ?? 0),
                    Name = row.TYPE_NAME
                };
            }

            if (row.SUB_TYPE_ID != null || row.SUB_TYPE_NAME != null)
            {
                item.SubType = new ObligationSubType
                {
                    Id = (long)(row.SUB_TYPE_ID ?? 0),
                    Name = row.SUB_TYPE_NAME
                };
            }

            if (row.STATE_ID != null || row.STATE_NAME != null)
            {
                item.State = new ObligationState
                {
                    Id = (long)(row.STATE_ID ?? 0),
                    Name = row.STATE_NAME
                };
            }


            if (row.RESPONSIBILITY_ID != null || row.RESPONSIBILITY_NAME != null)
            {
                item.Responsibility = new Responsibility
                {
                    Id = (long)(row.RESPONSIBILITY_ID ?? 0),
                    Name = row.RESPONSIBILITY_NAME
                };
            }

            return item;
        }

        public IList<Obligation> Search(ObligationQuery query)
        {
            var parameters = new Dictionary<string, object>();
            var procedure = "USP_JOBS_GET_OBLIGATIONS_BY_ACCOUNT_ID";
            if (query.AccountId.HasValue)
            {
                parameters["ACCOUNTID"] = query.AccountId.Value;
            }

            if (query.ForScheduler)
            {
                procedure = "USP_JOBS_GET_OBLIGATIONS_FOR_SCHEDULER";
            }

            if (query.GroupId.HasValue)
            {
                procedure = "USP_JOBS_GET_OBLIGATIONS_BY_GROUPID";
                parameters["GROUPID"] = query.GroupId.Value;
                parameters["GROUPTYPE"] = (long)query.GroupType.Value;
            }

            if (query.OwnerId.HasValue)
            {
                procedure = "USP_JOBS_GET_OBLIGATIONS_BY_OWNER_ID";
                parameters["OWNERID"] = query.OwnerId;
            }
            else if (!string.IsNullOrEmpty(query.ActionType))
            {
                procedure = "USP_JOBS_GET_OBLIGATIONS_BY_ACTION_TYPE";
                parameters["ACTIONTYPE"] = query.ActionType;
            }
            else if (query.IsTrigger.HasValue)
            {
                procedure = "USP_JOBS_GET_OBLIGATIONS_BY_IS_TRIGGER";
                parameters["ISTRIGGER"] = query.IsTrigger.Value ? 1 : 0;
            }

            return Find<Obligation>(procedure, parameters, row =>
            {
                return Map(row);
            });
        }

        public void StartProcessing(long id)
        {
            Execute("USP_JOBS_START_OBLIGATION_SCHEDULE_BY_ID", new Dictionary<string, object> {
                { "OBLIGATIONID", id }
            });
        }

        public void CompleteProcessing(Obligation item)
        {
            Execute("USP_JOBS_COMPLETE_OBLIGATION_SCHEDULE_BY_ID", new Dictionary<string, object> {
                { "OBLIGATIONID", item.Id },
                { "UPTODATE", item.UptoDate },
                { "NEXTCREATIONDATE", item.NextCreationDate }
            });
        }

        public string GetActionTypeName(string code)
        {
            if (string.IsNullOrEmpty(code))
            {
                return "Not Available";
            }
            switch (code.ToUpper())
            {
                case "A":
                    return "Alerted";

                case "M":
                    return "Monitored";

                case "R":
                    return "Reported";

                case "N":
                    return "Not Available";
            }

            return "Not Available";
        }

        public IList<ObligationType> GetObligationTypes()
        {
            var parameters = new Dictionary<string, object>();

            return Find("USP_JOBS_GET_OBLIGATION_TYPES", parameters, row =>
            {
                return new ObligationType
                {
                    Id = (long)row.ID,
                    Name = row.NAME,
                };
            });
        }

    }
}