﻿using Lexbase.Enum;
using Lexbase.Models;
using Lexbase.Queries;
using Lexbase.Repositories;
using System;
using System.Collections.Generic;

namespace Lexbase.Implementation.Repositories
{
    public class AccountReportRepository : RepositoryBase, IAccountReportRepository
    {
        public AccountReportRepository(
       ) : base("LBOBLIGATIONS")
        {
        }

        public void GenerateHeatMap(HeatMapQuery query)
        {
            var parameters = new Dictionary<string, object>();
            var procedure = "USP_JOBS_GENERATE_HEAT_MAP";

            parameters["ACCOUNTID"] = query.AccountId;

            Execute(procedure, parameters);
        }
        public List<HeatMapData> GetHeatMapReport(HeatMapQuery query)
        {
            var parameters = new Dictionary<string, object>();
            var procedure = "USP_JOBS_GET_FULL_HEAT_MAP";

            parameters["ACCOUNTID"] = query.AccountId;

            return Find(procedure, parameters, row =>
            {
                var model = new HeatMapData
                {
                    Id = row.ID.ToString(),
                    ReportGroup = EnumUntill.ParseEnum<HeatMapReportGroup>(row.REPORT_GROUP.ToString()),
                    Type = EnumUntill.ParseEnum<HeatMapReportTypes>(row.TYPE.ToString()),
                    Period = row.PERIOD.ToString(),
                    Count = Convert.ToInt32(row.COUNT)
                };

                if (row.OWNER_ID != null)
                {
                    model.Owner = new User
                    {
                        Id = Convert.ToInt64(row.OWNER_ID)
                    };
                }


                return model;
            });
        }


        public List<HeatMapData> GetHighRiskForHeatMap(long accountId)
        {
            var parameters = new Dictionary<string, object>();
            var procedure = "USP_JOBS_GET_HIGH_RISK_BY_ACCOUNT_ID";
            parameters["ACCOUNTID"] = accountId;

            return Find(procedure, parameters, row =>
            {
                var model = new HeatMapData
                {
                    HighriskId = row.HIGHRISK_ID,
                    Description = row.DESCRIPTION,
                    Penalty = row.PENALTY,
                    Duedate = row.DUE_DATE,
                    DaystillDue = row.DAYSTILL_DUE
                };
                if (row.OWNER_ID != null)
                {
                    model.Owner = new User
                    {
                        Id = Convert.ToInt64(row.OWNER_ID)
                    };
                }
                return model;
            });
        }
        public List<HeatMapData> GetHeatMapChartData(HeatMapQuery query)
        {
            var parameters = new Dictionary<string, object>();
            var procedure = "USP_JOBS_GET_HEAT_MAP_30_CHART_DATA";

            parameters["ACCOUNTID"] = query.AccountId;

            if (query.FrequencyType.HasValue)
            {
                parameters["FRQTYPE"] = query.FrequencyType.Value;
            }

            return Find(procedure, parameters, row =>
            {
                return new HeatMapData { Id = Convert.ToString(row.ID) };
            });
        }

        public List<HeatMapData> GetHeatMap90ChartData(HeatMapQuery query)
        {
            var parameters = new Dictionary<string, object>();
            var procedure = "USP_JOBS_GET_HEAT_MAP_90_CHART_DATA";
            parameters["ACCOUNTID"] = query.AccountId;

            if (query.StatusType.HasValue)
            {
                parameters["STATUSTYPE"] = query.StatusType.Value;
            }

            return Find(procedure, parameters, row =>
            {
                return new HeatMapData { Id = Convert.ToString(row.ID) };
            });
        }


    }
}