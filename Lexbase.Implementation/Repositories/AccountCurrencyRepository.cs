using Lexbase.Models;
using Lexbase.Queries;
using Lexbase.Repositories;
using log4net;
using System.Collections.Generic;

namespace Lexbase.Implementation.Repositories
{
    public class AccountCurrencyRepository : RepositoryBase, IAccountCurrencyRepository
    {
        private readonly ILog log = LogManager.GetLogger(typeof(AccountCurrencyRepository));

        public AccountCurrencyRepository() : base("LBACCOUNTS")
        {
        }

        public AccountCurrency Get(long id)
        {
            return FindOne("USP_JOBS_GET_ACCOUNT_CURRENCY_BY_ID", new Dictionary<string, object> { { "ACCOUNT_CURRENCY_ID", id } }, row =>
            {
                return new AccountCurrency
                {
                    AccountId = (long)(row.ACCOUNT_ID ?? 0),
                    ContractCurrency = (long)(row.CONTRACT_CURRENCY ?? 0)
                };
            });
        }

        public IList<AccountCurrency> Search(AccountCurrencyQuery query)
        {
            return Find("USP_JOBS_GET_ACCOUNT_CURRENCIES_BY_ACCOUNT_ID", new Dictionary<string, object> { { "ACCOUNT_ID", query.AccountId } }, row =>
            {
                return new AccountCurrency
                {
                    AccountId = (long)(row.ACCOUNT_ID ?? 0),
                    ContractCurrency = (long)(row.CONTRACT_CURRENCY ?? 0)
                };
            });
        }
    }
}