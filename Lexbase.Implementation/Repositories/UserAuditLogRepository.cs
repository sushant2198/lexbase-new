﻿using Lexbase.Models;
using Lexbase.Queries;
using Lexbase.Repositories;
using log4net;
using System.Collections.Generic;
using System.Linq;

namespace Lexbase.Implementation.Repositories
{
    public class UserAuditLogRepository : RepositoryBase, IUserAuditLogRepository
    {
        private readonly ILog log = LogManager.GetLogger(typeof(UserAuditLogRepository));
        private readonly IDictionary<long, UserAuditLog> cache = new Dictionary<long, UserAuditLog>();

        public UserAuditLogRepository() : base("LBAUDITLOGS")
        {
        }

        public UserAuditLog Get(long id)
        {
            throw new System.NotImplementedException();
        }
        public IList<UserAuditLog> Search(UserAuditLogQuery query)
        {
            var parameters = new Dictionary<string, object>();
            var procedure = "USP_JOBS_GET_AUDIT_LOGS";

            if (query.NoOfDays > 0)
            {
                parameters["noOfDays"] = query.NoOfDays;
            }

            return Find<UserAuditLog>(procedure, parameters, Mapper);
        }
        private UserAuditLog Mapper(dynamic row)
        {
            var groupfrName = System.Enum.GetValues(typeof(GroupFor)).Cast<GroupFor>();
            var item = new UserAuditLog
            {
                Id = (long)row.USER_ID,
                Date = row.DATE_CREATED
            };
            foreach (var group in groupfrName)
            {
                if (row.GROUP_FOR == group.ToString())
                {
                    item.GroupFor = group;
                }
            }
            item.GroupId = (long)row.GROUP_FOR_ID;
            item.Event = new UserAuditLogEvent
            {
                Id = (long)row.EVENT_ID,
                Name = row.EVENT_NAME,
                Code = row.EVENT_CODE
            };
            item.User = new User
            {
                Id = (long)row.USER_ID
            };
            return item;
        }

    }
}