﻿using Lexbase.Models;
using Lexbase.Queries;
using Lexbase.Repositories;
using System;
using System.Collections.Generic;

namespace Lexbase.Implementation.Repositories
{
    public class ReminderFrequencyRepository : RepositoryBase, IReminderFrequencyRepository
    {
        public ReminderFrequencyRepository() : base("LBOBLIGATIONS")
        {
        }

        public ReminderFrequency Get(long id)
        {
            throw new NotImplementedException();
        }

        public IList<ReminderFrequency> Search(ReminderFrequencyQuery query)
        {
            return Find("USP_JOBS_GET_REMINDER_FREQUENCIES_BY_FREQUENCY_ID", new Dictionary<string, object> { { "FREQUENCY_ID", query.FrequencyId.Value } }, row =>
            {
                var item = new ReminderFrequency
                {
                    Id = (long)row.ID,
                    Name = row.NAME,
                    DaysBack = (int)(row.DAYS_BACK ?? 0)
                };

                if (!string.IsNullOrEmpty(query.Names) && !query.Names.Contains(item.Name))
                {
                    return null;
                }

                if (row.FREQUENCY_ID != null)
                {
                    item.Frequency = new Frequency
                    {
                        Id = (long)(row.FREQUENCY_ID ?? 0),
                        Name = row.FREQUENCY_NAME
                    };
                }

                return item;
            });
        }
    }
}