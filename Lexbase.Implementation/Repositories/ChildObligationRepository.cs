﻿using Lexbase.Helpers;
using Lexbase.Models;
using Lexbase.Queries;
using Lexbase.Repositories;
using log4net;
using System;
using System.Collections.Generic;

namespace Lexbase.Implementation.Repositories
{
    public class ChildObligationRepository : RepositoryBase, IChildObligationRepository
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ChildObligationRepository));

        public ChildObligationRepository() : base("LBOBLIGATIONS")
        {
        }

        public void SetStatus(ChildObligation item, string status)
        {
            var history = JsonHelper.ToJsonString(item.StatusHistory, new Dictionary<string, string>
            {
                { "Status", status },
                { "Date", DateHelper.ToDateString(DateTime.Today) }
            });

            Execute("USP_JOBS_SET_CHILD_OBLIGATION_STATUS_BY_ID", new Dictionary<string, object>
            {
                { "COBID", item.Id },
                { "COBSTATUS",  status },
                { "COBSTATUSHISTORY", history }
            });
        }

        public ChildObligation Create(ChildObligation item)
        {
            var logContext = $"Create";

            log.Debug($"{logContext} Started");

            var history = JsonHelper.ToJsonString(new List<IDictionary<string, string>>
            {
                new Dictionary<string, string>
                {
                    { "Status", item.Status },
                    { "Date", DateHelper.ToDateString(item.Date) }
                }
            });

            var id = Create("USP_JOBS_CREATE_CHILD_OBLIGATION", new Dictionary<string, object> {
                { "OBLIGATIONID", item.Parent.Id },
                { "OBLIGATIONDATE", item.Date },
                { "ISACTIVE", item.IsActive ? 1 : 0 },
                { "ACTIONTYPE", item.ActionType },
                { "STATUS",  item.Status },
                { "STATUSHISTORY", history },
                { "STATEID", "1" },
            });

            log.Info($"{logContext} Id: {id}");

            item.Id = id;

            return item;
        }

        public DateTime Clean(long obligationId)
        {
            return ExecuteWithDate("USP_JOBS_CLEAN_CHILD_OBLIGATIONS_BY_OBLIGATIONID", new Dictionary<string, object> { { "OBLIGATIONID", obligationId } });
        }

        public ChildObligation Get(long id)
        {
            return FindOne<ChildObligation>("USP_JOBS_GET_CHILD_OBLIGATION_BY_COBID", new Dictionary<string, object> { { "COBID", id } }, Map);
        }

        public IList<ChildObligation> GetByAccount(long accountId)
        {
            return Find<ChildObligation>("USP_JOBS_GET_CHILD_OBLIGATION_BY_ACCOUNTID", new Dictionary<string, object> { { "ACCOUNTID", accountId } }, row =>
            {
                return Map(row);
            });
        }

        public IList<ChildObligation> Search(ChildObligationQuery query)
        {
            var parameters = new Dictionary<string, object>();
            var procedure = "USP_JOBS_GET_CHILD_OBLIGATIONS";

            if (!String.IsNullOrEmpty(query.Status))
            {
                parameters["cobStatus"] = query.Status;
            }

            if (query.ReportPeriod.HasValue)
            {
                parameters["reportPeriod"] = query.ReportPeriod.Value;
            }

            if (query.AccountId.HasValue)
            {
                parameters["accountId"] = query.AccountId.Value;
            }

            if (query.AccountIds != null)
            {
                parameters["ACCOUNTIDS"] = string.Join(',', query.AccountIds);
            }

            if (query.Date.HasValue)
            {
                parameters["cobDate"] = query.DueDate.Value;
            }

            if (query.DueDate.HasValue)
            {
                procedure = "USP_JOBS_GET_CHILD_OBLIGATIONS_OVERDUE";
                parameters["DUEDATE"] = query.DueDate.Value;
            }

            return Find<ChildObligation>(procedure, parameters, Map);
        }

        private ChildObligation Map(dynamic row)
        {
            var item = new ChildObligation
            {
                Id = (long)row.ID,
                Date = row.OBLIGATION_DATE,
                IsActive = true, //Convert.ToBoolean(row.IS_ACTIVE),
                ActionType = row.ACTION_TYPE,
                ObligationType = Convert.ToInt32(row.OBLIGATION_TYPE),
                Status = row.STATUS,
                StatusHistory = row.STATUS_HISTORY,
                FunctionalGroup = row.FUNCTIONAL_GROUP,
                IsFinancial = Convert.ToBoolean(row.FINANCIAL),
                DateCompleted = row.SUBMISSION_DATE,
                DueDate = row.OBLIGATION_DATE,
                ApprovalRequired = Convert.ToBoolean(row.APPROVAL_REQUIRED),
                CustomerApprovalDate = row.CUSTOMER_APPROVAL_DATE
            };

            if (row.PARENT_ID == null)
            {
                return item;
            }

            var obligation = new Obligation
            {
                Id = (long)row.PARENT_ID,
                Name = row.PARENT_NAME,
                IsActive = (row.PARENT_IS_ACTIVE ?? 0) == 0 ? false : true,
                InProcessing = (row.PARENT_IN_PROCESSING ?? 0) == 0 ? false : true,

                ActionType = row.PARENT_ACTION_TYPE,
                IsTrigger = (row.PARENT_IS_TRIGGER ?? 0) == 0 ? false : true,
                IsFinancial = (row.PARENT_FINANCIAL ?? 0) == 0 ? false : true,
                Status = row.PARENT_STATUS,

                StartDate = row.PARENT_START_DATE,
                CompletionDate = row.PARENT_COMPLETION_DATE,

                UptoDate = row.PARENT_UPTO_DATE,
                NextCreationDate = row.PARENT_NEXT_CREATION_DATE,

                NextDueDate = row.PARENT_NEXT_DUE_DATE,

                RulesUpdated = (int)(row.PARENT_RULES_UPDATED ?? 0),

                DateCreated = row.PARENT_DATE_CREATED,
                DateModified = row.PARENT_DATA_MODIFIED,
            };

            if (row.PARENT_FREQUENCY_ID != null)
            {
                obligation.Frequency = new Frequency
                {
                    Id = (long)(row.PARENT_FREQUENCY_ID ?? 0),
                    Name = row.PARENT_FREQUENCY_NAME,
                    AdvanceByMonths = (int)(row.PARENT_FREQUENCY_ADVANCE_BY_MONTHS ?? 0)
                };
            }

            if (row.PARENT_MANAGER_ID != null)
            {
                obligation.OwnerManager = new User
                {
                    Id = (long)(row.PARENT_MANAGER_ID ?? 0)
                };
            }

            if (row.PARENT_OWNER_ID != null)
            {
                obligation.OwnerUser = new User
                {
                    Id = (long)(row.PARENT_OWNER_ID ?? 0)
                };
            }

            if (row.PARENT_SHARED_WITH_ID != null)
            {
                obligation.SharedWith = new User
                {
                    Id = (long)(row.PARENT_SHARED_WITH_ID ?? 0)
                };
            }

            if (row.PARENT_ACCOUNT_ID != null)
            {
                obligation.Account = new Account
                {
                    Id = (long)(row.PARENT_ACCOUNT_ID ?? 0)
                };
            }

            if (row.PARENT_GROUP_FOR_ID != null)
            {
                obligation.GroupForId = (long)(row.PARENT_GROUP_FOR_ID ?? 0);

                if (row.PARENT_GROUP_FOR != null)
                {
                    obligation.GroupFor = (GroupFor)row.PARENT_GROUP_FOR;
                }
            }

            if (row.PARENT_PHASE_NAME != null || row.PARENT_PHASE_ID != null)
            {
                obligation.Phase = new Phase
                {
                    Id = (long)(row.PARENT_PHASE_ID ?? 0),
                    Name = row.PARENT_PHASE_NAME
                };
            }


            if (row.PARENT_TYPE_ID != null || row.PARENT_TYPE_NAME != null)
            {
                obligation.Type = new ObligationType
                {
                    Id = (long)(row.PARENT_TYPE_ID ?? 0),
                    Name = row.PARENT_TYPE_NAME
                };
            }

            if (row.PARENT_SUB_TYPE_ID != null || row.PARENT_SUB_TYPE_NAME != null)
            {
                obligation.SubType = new ObligationSubType
                {
                    Id = (long)(row.PARENT_SUB_TYPE_ID ?? 0),
                    Name = row.PARENT_SUB_TYPE_NAME
                };
            }

            if (row.PARENT_STATE_ID != null || row.PARENT_STATE_NAME != null)
            {
                obligation.State = new ObligationState
                {
                    Id = (long)(row.PARENT_STATE_ID ?? 0),
                    Name = row.PARENT_STATE_NAME
                };
            }

            if (row.PARENT_RESPONSIBILITY_ID != null || row.PARENT_RESPONSIBILITY_NAME != null)
            {
                obligation.Responsibility = new Responsibility
                {
                    Id = (long)(row.PARENT_RESPONSIBILITY_ID ?? 0),
                    Name = row.PARENT_RESPONSIBILITY_NAME
                };
            }

            item.Parent = obligation;

            return item;
        }
    }
}