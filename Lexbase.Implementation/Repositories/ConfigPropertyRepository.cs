using Lexbase.Repositories;
using log4net;
using System;
using System.Collections.Generic;

namespace Lexbase.Implementation.Repositories
{
    public class ConfigPropertyRepository : RepositoryBase, IConfigPropertyRepository
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ConfigPropertyRepository));

        public ConfigPropertyRepository() : base("LBACCOUNTS")
        {
        }

        public IDictionary<string, object> GetPropertiesValue(string[] keys)
        {
            throw new NotImplementedException();
        }
    }
}