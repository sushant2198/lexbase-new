using Lexbase.Models;
using Lexbase.Queries;
using Lexbase.Repositories;
using log4net;
using System;
using System.Collections.Generic;

namespace Lexbase.Implementation.Repositories
{
    public class AccountRepository : RepositoryBase, IAccountRepository
    {
        private readonly ILog log = LogManager.GetLogger(typeof(AccountRepository));
        private readonly IAccountCoverageRepository accountCoverageRepository;
        private readonly IAccountTypeRepository accountTypeRepository;
        private readonly IBusinessUnitRepository businessUnitRepository;
        private readonly IDeliveryCountryRepository deliveryCountryRepository;
        private readonly IAccountCurrencyRepository accountCurrencyRepository;
        private readonly IVerticalRepository verticalRepository;

        private readonly IDictionary<long, Account> cache = new Dictionary<long, Account>();

        public AccountRepository(
            IAccountCoverageRepository accountCoverageRepository,
            IAccountTypeRepository accountTypeRepository,
            IBusinessUnitRepository businessUnitRepository,
            IDeliveryCountryRepository deliveryCountryRepository,
            IAccountCurrencyRepository accountCurrencyRepository,
            IVerticalRepository verticalRepository
        ) : base("LBACCOUNTS")
        {
            this.accountCoverageRepository = accountCoverageRepository;
            this.accountTypeRepository = accountTypeRepository;
            this.businessUnitRepository = businessUnitRepository;
            this.deliveryCountryRepository = deliveryCountryRepository;
            this.accountCurrencyRepository = accountCurrencyRepository;
            this.verticalRepository = verticalRepository;
        }

        public IList<User> GetManagers(long accountId)
        {
            return Find("USP_JOBS_GET_ACCOUNT_MANAGERS_BY_ACCOUNT_ID", new Dictionary<string, object> { { "ACCOUNTID", accountId } }, row =>
            {
                if (row.MANAGER_ID == null)
                {
                    return null;
                }

                return new User
                {
                    Id = (long)(row.MANAGER_ID ?? 0),
                    ManagerType = (long)(row.MANAGER_TYPE ?? 0)
                };
            });
        }

        public Account Get(long accountId)
        {
            if (!cache.ContainsKey(accountId))
            {
                cache[accountId] = FindOne<Account>("USP_JOBS_GET_ACCOUNT_BY_ID", new Dictionary<string, object> { { "ACCOUNTID", accountId } }, Mapper);
            }
            return cache[accountId];
        }

        public IList<Account> Search(AccountQuery query)
        {
            var parameters = new Dictionary<string, object>();
            var procedure = "USP_JOBS_GET_ACCOUNTS";

            if (query.IsActive.HasValue)
            {
                parameters["isActive"] = query.IsActive.Value ? 1 : 0;
            }

            if (query.NeedActivation)
            {
                procedure = "USP_JOBS_GET_ACCOUNTS_NEEDS_ACTIVATION";
            }
            if (query.TimezoneId.HasValue)
            {
                procedure = "USP_JOBS_GET_ACCOUNTS_BY_TIMEZONE";
                parameters["TIMEZONEID"] = query.TimezoneId;
            }

            return Find<Account>(procedure, parameters, Mapper);
        }

        public void StartActivation(long id)
        {
            // Execute("USP_JOBS_START_ACCOUNT_ACTIVATION_BY_ID", new Dictionary<string, object> { { "ACCOUNT_ID", id } });
        }

        public void CompleteActivation(long id)
        {
            Execute("USP_JOBS_COMPLETE_ACCOUNT_ACTIVATION_BY_ID", new Dictionary<string, object> { { "ACCOUNT_ID", id } });
        }

        public IList<Account> GetAccountStatus()
        {
            return Find("USP_JOBS_GET_ACCOUNT_FOR_STATUS", null, row =>
            {
                var item = new Account
                {
                    Id = (long)row.ID,
                    Name = row.NAME,
                    Active = (row.IS_ACTIVE ?? 0) == 0 ? false : true,
                    BragStatus = (int)(row.BRAG_STATUS ?? 0),
                    IsArchive = (int)(row.IS_ARCHIVE ?? 0)
                };

                if (row.TIMEZONE_ID != null)
                {
                    item.TimeZone = new Timezone
                    {
                        Id = (long)row.TIMEZONE_ID,
                        Name = row.TIMEZONE_NAME,
                        Description = row.TIMEZONE_DESCRIPTION,
                        Value = row.TIMEZONE_VALUE
                    };
                }
                return item;
            });
        }

        public void CreateAccountStatusByFG(AccountStatusData item)
        {
            Execute("USP_JOBS_CREATE_ACCOUNT_STATUS_BY_FG", new Dictionary<string, object> {
                { "ACCOUNTID", item.Id },
                { "OBLIGATIONTYPE", item.ObligationType },
                { "STARTDATE", item.StartDate },
                { "ENDDATE", item.EndDate },
                { "BRAGRATIO",  item.BragRatio }
            });
        }

        public void CreateAccountStatusData(AccountStatusData item)
        {
            Execute("USP_JOBS_CREATE_ACCOUNT_STATUS_DATA", new Dictionary<string, object> {
                { "ACCOUNTID", item.Id },
                { "STARTDATE", item.StartDate },
                { "ENDDATE", item.EndDate },
                { "CHILDOBLIGATIONIDS",  string.Join(',', item.ChildObligationIds) }
            });
        }

        public void DeleteAccountStatusByFG(AccountStatusData item)
        {
            Execute("USP_JOBS_DELETE_ACCOUNT_STATUS_BY_FG", new Dictionary<string, object> {
                { "ACCOUNTID", item.Id },
                { "STARTDATE", item.StartDate },
                { "ENDDATE", item.EndDate }
            });
        }

        public IList<AccountFreezingStatus> GetAccountFreezingStatus(DateTime StartDate, DateTime EndDate)
        {
            return Find("USP_JOBS_GET_ACCOUNT_FREEZING_STATUS", new Dictionary<string, object> { { "STARTDATE", StartDate }, { "ENDDATE", EndDate } }, row =>
            {
                var item = new AccountFreezingStatus
                {
                    Id = (long)row.ID,
                    FreezingStatus = (row.FREEZING_STATUS ?? 0) == 0 ? false : true
                };

                return item;
            });
        }

        public void UpdateInActiveAccountStatus(AccountStatus accountStatus, int isActive)
        {
            Execute("USP_JOBS_UPDATE_ACCOUNT_STATUS_INACTIVE", new Dictionary<string, object> {
                { "ACCOUNTID", accountStatus.Id },
                { "BRAGSTATUS", accountStatus.BragStatus },
                { "STARTDATE", accountStatus.StartDate },
                { "ENDDATE", accountStatus.EndDate },
                { "BRAGRATIO", accountStatus.BragRatio },
                { "FINANCIALCOBCOUNT", accountStatus.CurrentStatus },
                { "FREEZINGSTATUS", Convert.ToInt16(accountStatus.Freezingtatus)},
                { "ISINACTIVE", isActive },
            });
        }

        public void CreateAccountStatusByFG(AccountStatus item)
        {
            Create("USP_JOBS_CREATE_ACCOUNT_STATUS_BY_FG", new Dictionary<string, object> {
                { "ACCOUNTID", item.Id },
                { "OBLIGATIONTYPE", item.ObligationType },
                { "STARTDATE", item.StartDate },
                { "ENDDATE", item.EndDate },
                { "BRAGRATIO",  item.BragRatio }
            });
        }

        public List<AccountTrendData> GetAccountTrendData(long accountId)
        {
            var parameters = new Dictionary<string, object>();
            var procedure = "USP_JOBS_GET_TREND_REPORT_BY_ACCOUNT_ID";
            parameters["ACCOUNTID"] = accountId;

            return Find(procedure, parameters, row =>
            {
                return new AccountTrendData
                {
                    ObligationTypeId = (long)row.OBLIGATIONTYPE,
                    BragRatio = row.BRAGRATIO,
                    StartDate = row.STARTDATE,
                    BragStatus = row.BRAGSTATUS
                };
            });
        }

        public List<AccountInformationData> GetAccountInformationReport()
        {
            return Find("USP_JOBS_GET_INFORMATION_REPORT_BY_ACCOUNT_ID", new Dictionary<string, object>(), row =>
            {
                return new AccountInformationData
                {
                    AccountId = (long)row.ACCOUNTID,
                    Name = Convert.ToString(row.NAME),
                    AccountType = Convert.ToString(row.ACCOUNTTYPE),
                    Status = Convert.ToString(row.STATUS),
                    RegionName = Convert.ToString(row.REGIONNAME),
                    AccountValue = Convert.ToString(row.ACCOUNTVALUE),
                    CurrencyType = Convert.ToString(row.CURRENCYTYPE),
                    AccountDateCreated = Convert.ToString(row.ACCOUNTDATECREATED),
                    AccountDateModified = Convert.ToString(row.ACCOUNTDATEMODIFIED),
                    IsActive = Convert.ToString(row.ISACTIVE),
                    TimeZone = Convert.ToString(row.TIMEZONE),
                    Sensitive = Convert.ToString(row.SENSITIVE),
                    LegelName = Convert.ToString(row.LEGELNAME),
                    AccountClassification = Convert.ToString(row.ACCOUNTCLASSIFICATION),
                    AccountSize = Convert.ToString(row.ACCOUNTSIZE),
                    Alias = Convert.ToString(row.ALIAS),
                    Address = Convert.ToString(row.ADDRESS),
                    InTransition = Convert.ToString(row.INTRANSITION),
                    SubmissionDate = Convert.ToString(row.SUBMISSIONDATE),
                    LeadCountryName = Convert.ToString(row.LEADCOUNTRYNAME),
                    AccountDateConfirmed = Convert.ToString(row.ACCOUNTDATECONFIRMED),
                    DeliveryCountryName = Convert.ToString(row.DELIVERYCOUNTRYNAME)
                };
            });
        }

        private Account Mapper(dynamic row)
        {
            var item = new Account
            {
                Id = (long)row.ID,
                Name = row.NAME,
                Active = (row.IS_ACTIVE ?? 0) == 0 ? false : true

                // BragStatus = (int)(row.BRAG_STATUS ?? 0),
                // IsArchive = (int)(row.IS_ARCHIVE ?? 0),
                // AccountAmendmentStatus = (int)(row.IS_AMENDED ?? 0),
                // AmendmentBannerDate = row.AMENDMENT_BANNER_DATE,
                // GlActive = row.GL_ACTIVE ?? false,
                // AccessEmpty = row.ACCESS_EMPTY ?? false,
                // DataXml = row.DATAXML,

                // AccountSize = (long)(row.ACCOUNT_SIZE ?? 0),
                // AccountValue = (int)(row.ACCOUNT_VALUE ?? 0),

                // CmAlerted = row.CM_ALERTED ?? false,
                // PreActivated = row.PRE_ACTIVATED ?? false,

                // Sensitive = row.IS_SENSITIVE ?? false,
                // CurrencyType = (long)(row.CURRENCY_TYPE ?? 0),
                // LegalName = row.LEGAL_NAME,
                // Alias = row.ALIAS,

                // Address = row.ADDRESS,
                // LeadCountry = (int)(row.LEAD_COUNTRY ?? 0),
                // LeadCountryName = row.LEAD_COUNTRY_NAME,
                // ContractCurrency = (long)(row.CONTRACT_CURRENCY ?? 0),
                // ContractingEntityName = row.CONTRACTING_ENTITY_NAME,
                // OfferingOrganization = (int)(row.OFFERING_ORG ?? 0),
                // OrganizationName = row.OFFERING_ORG_NAME,

                // InTransition = row.IN_TRANSITION ?? false,
                // SubmissionDate = row.SUBMISSION_DATE,
                // InDumpGeneration = row.IN_DUMP_GENERATION ?? false,
                // InProcessing = (int)(row.IN_PROCESSING ?? 0),
                // InReIndexing = (int)(row.IN_RE_INDEXING ?? 0)
            };

            if (row.COVERAGE_ID != null)
            {
                item.AccountCoverage = new AccountCoverage
                {
                    Id = (long)row.COVERAGE_ID,
                    Name = row.COVERAGE_NAME
                };
            }

            if (row.TIMEZONE_ID != null)
            {
                item.TimeZone = new Timezone
                {
                    Id = (long)row.TIMEZONE_ID,
                    Name = row.TIMEZONE_NAME,
                    Description = row.TIMEZONE_DESCRIPTION,
                    Value = row.TIMEZONE_VALUE
                };
            }

            if (row.TYPE_ID != null)
            {
                item.AccountType = new AccountType
                {
                    Id = (long)row.TYPE_ID,
                    Name = row.TYPE_NAME
                };
            }

            if (row.REGION_ID != null)
            {
                item.Region = new Region
                {
                    Id = (long)row.REGION_ID,
                    Name = row.REGION_NAME
                };
            }

            // item.BusinessUnits = businessUnitRepository.Search(new BusinessUnitQuery { AccountId = item.Id });
            // item.DeliveryCountries = deliveryCountryRepository.Search(new DeliveryCountryQuery { AccountId = item.Id });
            // item.AccountCurrencies = accountCurrencyRepository.Search(new AccountCurrencyQuery { AccountId = item.Id });
            // item.VerticalList = verticalRepository.Search(new VerticalQuery { AccountId = item.Id });

            return item;
        }
        public List<HeatMapData> GetTrendDataForHeatMap(long accountId)
        {
            var parameters = new Dictionary<string, object>();
            var procedure = "USP_JOBS_GET_TREND_REPORT_BY_ACCOUNT_ID";
            parameters["ACCOUNTID"] = accountId;

            return Find(procedure, parameters, row =>
            {
                return new HeatMapData
                {
                    ObligationTypeId = (long)row.OBLIGATIONTYPE,
                    BragRatio = row.BRAGRATIO,
                    StartDate = row.STARTDATE,
                    BragStatus = row.BRAGSTATUS
                };
            });
        }
    }
}