﻿using Lexbase.Models;
using Lexbase.Queries;
using Lexbase.Repositories;
using log4net;
using System.Collections.Generic;

namespace Lexbase.Implementation.Repositories
{
    public class ChangeRequestRepository : RepositoryBase, IChangeRequestRepository
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ChangeRequestRepository));
        private readonly IDictionary<long, User> cache = new Dictionary<long, User>();

        public ChangeRequestRepository() : base("LBUSER")
        {
        }

        public ChangeRequest Get(long id)
        {
            throw new System.NotImplementedException();
        }

        public IList<ChangeRequest> Search(UserQuery query)
        {
            var procedure = "USP_JOBS_GET_USERS";
            var parameters = new Dictionary<string, object>();
            if (query.UserIds != null)
            {
                if (query.UserIds.Count == 0)
                {
                    return new List<ChangeRequest>();
                }

                procedure = "USP_JOBS_GET_CHANGE_REQUESTS_BY_USER_IDS";
                parameters["USER_IDS"] = string.Join(',', query.UserIds);
            }

            return Find(procedure, parameters, Mapper);
        }
        private ChangeRequest Mapper(dynamic row)
        {
            var item = new ChangeRequest
            {
                Id = (long)row.ID,
                Date = row.REQUEST_DATE,
                Subject = row.SUBJECT,
                Reason = row.REASON,
                Details = row.DETAILS,
            };

            item.Type = new ChangeRequestType
            {
                Id = (long)row.TYPE_ID,
                Name = row.TYPE_NAME,
                Category = new ChangeRequestCategory
                {
                    Id = (long)row.TYPE_CATEGORY_ID,
                    Name = row.TYPE_CATEGORY_NAME
                }
            };

            var user = new User
            {
                Id = (long)row.USER_ID,
                FirstName = row.USER_FIRST_NAME,
                MiddleName = row.USER_MIDDLE_NAME,
                LastName = row.USER_LAST_NAME,
                Email = row.USER_EMAIL
            };

            user.FullName = $"{user.FirstName} {user.LastName}";

            item.User = user;

            return item;
        }
    }
}