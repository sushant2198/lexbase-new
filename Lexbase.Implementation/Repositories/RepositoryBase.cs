﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;

namespace Lexbase.Implementation.Repositories
{
    public abstract class RepositoryBase
    {
        public delegate TModel Mapper<TModel>(dynamic data);

        private readonly log4net.ILog log =
            log4net.LogManager.GetLogger(typeof(RepositoryBase));

        private string user;

        protected RepositoryBase(string user)
        {
            this.user = user;
        }

        protected TModel FindOneByQuery<TModel>(
            string sql,
            IDictionary<string, object> param,
            Mapper<TModel> mapper
        )
        {
            var logContext = $"[FindOneByQuery:{user}]";
            // log.Debug($@"{logContext} sql:
            //     {sql}");

            try
            {
                OracleDynamicParameters parameters = new OracleDynamicParameters();
                if (param != null)
                {
                    foreach (var key in param.Keys)
                    {
                        parameters.Add(key, param[key]);
                    }
                }

                using (var connection = Context.Current.GetDbConnection(user))
                {
                    var result = connection.QueryFirstOrDefault<dynamic>(sql, parameters, commandType: CommandType.StoredProcedure);

                    return mapper(result);
                }
            }
            catch (Exception error)
            {
                throw FormattedError(error, user, sql, param);
            }
        }

        protected List<TModel> FindByQuery<TModel>(
            string sql,
            IDictionary<string, object> param,
            Mapper<TModel> mapper)
        {
            var logContext = $"[FindByQuery:{user}]";
            // log.Debug($@"{logContext} sql:
            //     {sql}");

            OracleDynamicParameters parameters = new OracleDynamicParameters();

            if (param != null)
            {
                foreach (var key in param.Keys)
                {
                    parameters.Add(key, param[key]);
                }
            }
            using (var connection = Context.Current.GetDbConnection(user))
            {
                var result = connection.Query<dynamic>(sql, parameters).AsList();

                var values = new List<TModel>();

                foreach (var item in result)
                {
                    var mapped = mapper(item);

                    if (mapped != null)
                    {
                        values.Add(mapped);
                    }
                }

                log.Debug($"{logContext} count: {values.Count}");
                return values;
            }
        }

        protected TModel FindOne<TModel>(
            string procedure,
            IDictionary<string, object> param,
            Mapper<TModel> mapper
        )
        {
            var logContext = $"[FindOne:{user}.{procedure}]";
            log.Debug($"{logContext} Started");

            try
            {
                OracleDynamicParameters parameters = new OracleDynamicParameters();
                if (param != null)
                {
                    foreach (var key in param.Keys)
                    {
                        parameters.Add(key, param[key]);
                    }
                }
                parameters.Cursor = "LIST_CURSOR";

                using (var connection = Context.Current.GetDbConnection(user))
                {
                    var result = connection.QueryFirstOrDefault<dynamic>(procedure, parameters, commandType: CommandType.StoredProcedure);

                    if (result == null)
                    {
                        return default(TModel);
                    }
                    return mapper(result);
                }
            }
            catch (Exception error)
            {
                throw FormattedError(error, user, procedure, param);
            }
        }

        protected List<TModel> Find<TModel>(
            string procedure,
            IDictionary<string, object> param,
            Mapper<TModel> mapper
        ) where TModel : class
        {
            var logContext = $"[Find:{user}.{procedure}]";
            // log.Debug($"{logContext} Started");
            log.Debug($"{logContext} Param {FormattedParams(param)}");

            try
            {
                OracleDynamicParameters parameters = new OracleDynamicParameters();

                if (param != null)
                {
                    foreach (var key in param.Keys)
                    {
                        parameters.Add(key, param[key]);
                    }
                }

                parameters.Cursor = "LIST_CURSOR";
                using (var connection = Context.Current.GetDbConnection(user))
                {
                    var result = connection.Query<dynamic>(procedure, parameters, commandType: CommandType.StoredProcedure);

                    var values = new List<TModel>();

                    foreach (var item in result)
                    {
                        if (item == null)
                        {
                            continue;
                        }
                        var mapped = mapper(item);

                        if (mapped != null)
                        {
                            values.Add(mapped);
                        }
                    }

                    log.Debug($"{logContext} count: {values.Count}");
                    return values;
                }
            }
            catch (Exception error)
            {
                throw FormattedError(error, user, procedure, param);
            }
        }

        protected long Create(string procedure, IDictionary<string, object> param)
        {
            var logContext = $"[Create:{user}.{procedure}]";
            log.Debug($"{logContext} Started");

            try
            {
                OracleDynamicParameters parameters = new OracleDynamicParameters();
                foreach (var key in param.Keys)
                {
                    parameters.Add(key, param[key]);
                }

                parameters.CreatedId = "CREATEDID";

                var val = 0;
                using (var connection = Context.Current.GetDbConnection(user))
                {
                    val = connection.Execute(procedure,
                        parameters,
                        commandType: CommandType.StoredProcedure);
                    log.Debug($"{logContext} Done");

                    return (long)parameters.Get<Decimal>("CREATEDID");
                }
            }
            catch (Exception error)
            {
                throw FormattedError(error, user, procedure, param);
            }
        }


        protected DateTime ExecuteWithDate(string procedure, IDictionary<string, object> param)
        {
            var logContext = $"[Execute:{user}.{procedure}]";
            log.Debug($"{logContext} Started");

            try
            {
                OracleDynamicParameters parameters = new OracleDynamicParameters();
                foreach (var key in param.Keys)
                {
                    parameters.Add(key, param[key]);
                }

                parameters.DateResult = "DATERESULT";

                var val = 0;
                using (var connection = Context.Current.GetDbConnection(user))
                {
                    val = connection.Execute(procedure,
                        parameters,
                        commandType: CommandType.StoredProcedure);
                    log.Debug($"{logContext} Done");

                    return (DateTime)parameters.Get<DateTime>("DATERESULT");
                }
            }
            catch (Exception error)
            {
                throw FormattedError(error, user, procedure, param);
            }
        }

        protected int Execute(string procedure, IDictionary<string, object> param = null)
        {
            var logContext = $"[Execute:{user}.{procedure}]";
            log.Debug($"{logContext} Started");
            try
            {
                OracleDynamicParameters parameters = new OracleDynamicParameters();

                if (param != null)
                {
                    foreach (var key in param.Keys)
                    {
                        parameters.Add(key, param[key]);
                    }
                }

                var val = 0;

                using (var connection = Context.Current.GetDbConnection(user))
                {
                    val = connection.Execute(procedure,
                        parameters,
                        commandType: CommandType.StoredProcedure);
                }

                log.Debug($"{logContext} Done");

                return val;
            }
            catch (Exception error)
            {
                var input = "";
                if (param != null)
                {
                    foreach (var key in param.Keys)
                    {
                        var value = param[key] == null ? "NULL" : param[key].ToString();
                        input = $"{input} {key}={value}, \n";
                    }
                }

                var message = error.Message;

                if (message.Contains("PLS-00201:"))
                {
                    message = "Procedure not found";
                }

                throw new Exception($@"
                error: {message}
                procedure:{user}.{procedure}
                params:
                {input}", error);
            }
        }

        private string FormattedParams(IDictionary<string, object> param)
        {
            var input = "";
            if (param != null)
            {
                foreach (var key in param.Keys)
                {
                    var value = param[key] == null ? "NULL" : param[key].ToString();
                    input = $"{input} {key}={value}, \n";
                }
            }

            return input;
        }

        private Exception FormattedError(
            Exception error,
            string user,
            string procedure,
            IDictionary<string, object> param
            )
        {
            var input = FormattedParams(param);
            var message = error.Message;

            if (message.Contains("PLS-00201:"))
            {
                message = "Procedure not found";
            }

            return new Exception($"error: {message}, \n procedure:{user}.{procedure}, \n params: \n{input}", error);
        }
    }
}