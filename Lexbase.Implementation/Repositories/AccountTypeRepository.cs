using Lexbase.Models;
using Lexbase.Queries;
using Lexbase.Repositories;
using log4net;
using System.Collections.Generic;

namespace Lexbase.Implementation.Repositories
{
    public class AccountTypeRepository : RepositoryBase, IAccountTypeRepository
    {
        private readonly ILog log = LogManager.GetLogger(typeof(AccountTypeRepository));

        public AccountTypeRepository() : base("LBACCOUNTS")
        {
        }

        public AccountType Get(long id)
        {
            return FindOne("USP_JOBS_GET_ACCOUNT_TYPE_BY_ID", new Dictionary<string, object> { { "ACCOUNT_TYPE_ID", id } }, row =>
            {
                return new AccountType
                {
                    Id = (long)row.ID,
                    Name = row.NAME,
                    Active = row.IS_ACTIVE,
                    DateCreated = row.DATE_CREATED,
                    DateUpdated = row.DATE_MODIFIED
                };
            });
        }

        public IList<AccountType> Search(AccountTypeQuery query)
        {
            return Find("USP_JOBS_GET_ACCOUNT_TYPES_BY_ACCOUNT_ID", new Dictionary<string, object> { { "ACCOUNT_ID", query.AccountId.Value } }, row =>
            {
                return new AccountType
                {
                    Id = (long)row.ID,
                    Name = row.NAME,
                    Active = row.IS_ACTIVE,
                    DateCreated = row.DATE_CREATED,
                    DateUpdated = row.DATE_MODIFIED
                };
            });
        }
    }
}