﻿using Lexbase.Models;
using Lexbase.Queries;
using Lexbase.Repositories;
using log4net;
using System.Collections.Generic;

namespace Lexbase.Implementation.Repositories
{
    public class ObligationTriggerLogRepository : RepositoryBase, IObligationTriggerLogRepository
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ObligationTriggerLogRepository));

        public ObligationTriggerLogRepository() : base("LBOBLIGATIONS")
        {
        }

        public void SetSent(long id, System.DateTime sentDate)
        {
            Execute("USP_JOBS_SET_OBLIGATION_TRIGGER_LOG_SENT_BY_ID", new Dictionary<string, object> {
                { "LOGID", id },
                { "SENTDATE",sentDate }
            });
        }

        public ObligationTriggerLog Create(ObligationTriggerLog item)
        {
            var logContext = $"Create";

            log.Debug($"{logContext} Started");

            var id = Create("USP_JOBS_CREATE_OBLIGATION_TRIGGER_LOG", new Dictionary<string, object> {
                { "CHILDOBLIGATIONID", item.ChildObligation.Id },
                { "REMINDERDATE", item.When },
                { "ISACTIVE", item.ChildObligation.IsActive ? 1 : 0 },
                { "REMINDERID", item.Reminder.Id },
                { "FREQUENCYID", item.Reminder.Frequency.Id },
                // { "STATUS", item.Status },
                // { "STATUSHISTORY", item.StatusHistory },
                // { "STATEID", item.StateId },
            });

            log.Info($"{logContext} Id: {id}");

            item.Id = id;

            return item;
        }

        public ObligationTriggerLog Get(long id)
        {
            return FindOne("USP_JOBS_GET_OBLIGATION_TRIGGER_LOG_BY_ID", new Dictionary<string, object> { { "LOGID", id } }, Mapper);
        }

        public IList<ObligationTriggerLog> Search(ObligationTriggerLogQuery query)
        {
            var procedure = "USP_JOBS_GET_OBLIGATION_TRIGGER_LOGS";
            var parameters = new Dictionary<string, object>();
            if (query.AccountIds != null)
            {
                if (query.AccountIds.Count == 0)
                {
                    return new List<ObligationTriggerLog>();
                }

                procedure = "USP_JOBS_GET_OBLIGATION_TRIGGER_LOGS_BY_ACCOUNTIDS";
                parameters["ACCOUNTIDS"] = string.Join(',', query.AccountIds);
            }

            if (query.When.HasValue)
            {
                parameters["REMINDERDATE"] = query.When;
                // parameters["LOGDATE"] = query.When.Value;
            }

            return Find(procedure, parameters, Mapper);
        }

        private ObligationTriggerLog Mapper(dynamic row)
        {
            var item = new ObligationTriggerLog
            {
                Id = (long)row.ID,
            };

            if (row.REMINDER_ID != null)
            {
                item.Reminder = new ReminderFrequency
                {
                    Id = (long)(row.REMINDER_ID ?? 0),
                    Name = row.REMINDER_NAME,
                    Status = row.REMINDER_STATUS
                };
            }

            if (row.CHILD_OBLIGATION_ID != null)
            {
                item.ChildObligation = new ChildObligation
                {
                    Id = (long)row.CHILD_OBLIGATION_ID,
                    Date = row.CHILD_OBLIGATION_DATE,
                    IsActive = (row.CHILD_OBLIGATION_IS_ACTIVE ?? 0) == 0 ? false : true,
                    ActionType = row.CHILD_OBLIGATION_ACTION_TYPE,
                    Status = row.CHILD_OBLIGATION_STATUS,
                    StatusHistory = row.CHILD_OBLIGATION_STATUS_HISTORY
                };
            }

            if (row.CHILD_OBLIGATION_PARENT_ID == null)
            {
                return item;
            }

            var obligation = new Obligation
            {
                Id = (long)row.CHILD_OBLIGATION_PARENT_ID,
                Name = row.CHILD_OBLIGATION_PARENT_NAME,
                IsActive = (row.CHILD_OBLIGATION_PARENT_IS_ACTIVE ?? 0) == 0 ? false : true,
                InProcessing = (row.CHILD_OBLIGATION_PARENT_IN_PROCESSING ?? 0) == 0 ? false : true,

                ActionType = row.CHILD_OBLIGATION_PARENT_ACTION_TYPE,
                IsTrigger = (row.CHILD_OBLIGATION_PARENT_IS_TRIGGER ?? 0) == 0 ? false : true,
                IsFinancial = (row.CHILD_OBLIGATION_PARENT_FINANCIAL ?? 0) == 0 ? false : true,
                Status = row.CHILD_OBLIGATION_PARENT_STATUS,

                StartDate = row.CHILD_OBLIGATION_PARENT_START_DATE,
                CompletionDate = row.CHILD_OBLIGATION_PARENT_COMPLETION_DATE,

                UptoDate = row.CHILD_OBLIGATION_PARENT_UPTO_DATE,
                NextCreationDate = row.CHILD_OBLIGATION_PARENT_NEXT_CREATION_DATE,

                NextDueDate = row.CHILD_OBLIGATION_PARENT_NEXT_DUE_DATE,

                RulesUpdated = (int)(row.CHILD_OBLIGATION_PARENT_RULES_UPDATED ?? 0),

                DateCreated = row.CHILD_OBLIGATION_PARENT_DATE_CREATED,
                DateModified = row.CHILD_OBLIGATION_PARENT_DATA_MODIFIED,
            };

            if (row.CHILD_OBLIGATION_PARENT_FREQUENCY_ID != null)
            {
                obligation.Frequency = new Frequency
                {
                    Id = (long)(row.CHILD_OBLIGATION_PARENT_FREQUENCY_ID ?? 0),
                    Name = row.CHILD_OBLIGATION_PARENT_FREQUENCY_NAME,
                    AdvanceByMonths = (int)(row.CHILD_OBLIGATION_PARENT_FREQUENCY_ADVANCE_BY_MONTHS ?? 0)
                };
            }

            if (row.CHILD_OBLIGATION_PARENT_MANAGER_ID != null)
            {
                obligation.OwnerManager = new User
                {
                    Id = (long)(row.CHILD_OBLIGATION_PARENT_MANAGER_ID ?? 0)
                };
            }

            if (row.CHILD_OBLIGATION_PARENT_OWNER_ID != null)
            {
                obligation.OwnerUser = new User
                {
                    Id = (long)(row.CHILD_OBLIGATION_PARENT_OWNER_ID ?? 0)
                };
            }

            if (row.CHILD_OBLIGATION_PARENT_SHARED_WITH_ID != null)
            {
                obligation.SharedWith = new User
                {
                    Id = (long)(row.CHILD_OBLIGATION_PARENT_SHARED_WITH_ID ?? 0)
                };
            }

            if (row.CHILD_OBLIGATION_PARENT_ACCOUNT_ID != null)
            {
                obligation.Account = new Account
                {
                    Id = (long)(row.CHILD_OBLIGATION_PARENT_ACCOUNT_ID ?? 0)
                };
            }

            if (row.CHILD_OBLIGATION_PARENT_GROUP_FOR_ID != null)
            {
                obligation.GroupForId = (long)(row.CHILD_OBLIGATION_PARENT_GROUP_FOR_ID ?? 0);

                if (row.CHILD_OBLIGATION_PARENT_GROUP_FOR != null)
                {
                    obligation.GroupFor = (GroupFor)row.CHILD_OBLIGATION_PARENT_GROUP_FOR;
                }
            }

            if (row.CHILD_OBLIGATION_PARENT_PHASE_NAME != null || row.CHILD_OBLIGATION_PARENT_PHASE_ID != null)
            {
                obligation.Phase = new Phase
                {
                    Id = (long)(row.CHILD_OBLIGATION_PARENT_PHASE_ID ?? 0),
                    Name = row.CHILD_OBLIGATION_PARENT_PHASE_NAME
                };
            }


            if (row.CHILD_OBLIGATION_PARENT_TYPE_ID != null || row.CHILD_OBLIGATION_PARENT_TYPE_NAME != null)
            {
                obligation.Type = new ObligationType
                {
                    Id = (long)(row.CHILD_OBLIGATION_PARENT_TYPE_ID ?? 0),
                    Name = row.CHILD_OBLIGATION_PARENT_TYPE_NAME
                };
            }

            if (row.CHILD_OBLIGATION_PARENT_SUB_TYPE_ID != null || row.CHILD_OBLIGATION_PARENT_SUB_TYPE_NAME != null)
            {
                obligation.SubType = new ObligationSubType
                {
                    Id = (long)(row.CHILD_OBLIGATION_PARENT_SUB_TYPE_ID ?? 0),
                    Name = row.CHILD_OBLIGATION_PARENT_SUB_TYPE_NAME
                };
            }

            if (row.CHILD_OBLIGATION_PARENT_STATE_ID != null || row.CHILD_OBLIGATION_PARENT_STATE_NAME != null)
            {
                obligation.State = new ObligationState
                {
                    Id = (long)(row.CHILD_OBLIGATION_PARENT_STATE_ID ?? 0),
                    Name = row.CHILD_OBLIGATION_PARENT_STATE_NAME
                };
            }

            if (row.CHILD_OBLIGATION_PARENT_RESPONSIBILITY_ID != null || row.CHILD_OBLIGATION_PARENT_RESPONSIBILITY_NAME != null)
            {
                obligation.Responsibility = new Responsibility
                {
                    Id = (long)(row.CHILD_OBLIGATION_PARENT_RESPONSIBILITY_ID ?? 0),
                    Name = row.CHILD_OBLIGATION_PARENT_RESPONSIBILITY_NAME
                };
            }

            item.ChildObligation.Parent = obligation;

            return item;
        }
    }
}