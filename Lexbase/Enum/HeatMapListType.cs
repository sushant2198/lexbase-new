﻿using System.ComponentModel;

namespace Lexbase.Enum
{
    public enum HeatMapListType
    {
        [Description("CSC Obligations Status")]
        Csc = 1,

        [Description("Customer Obligations Status")]
        Cus = 2,

        [Description("Obligation Owner Status (Top Five)")]
        Owner = 3,
    }
}
