﻿using System.ComponentModel;

namespace Lexbase.Enum
{
    public enum FrequencyTypes
    {
        [Description("MONTHLY")]
        MONTHLY = 1,

        [Description("QUARTERLY")]
        QUARTERLY = 2,

        [Description("> QUARTERLY")]
        GREATERQUARTERLY = 3,
    }
}
