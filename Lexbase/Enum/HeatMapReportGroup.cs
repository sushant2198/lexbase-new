namespace Lexbase.Enum
{
    public enum HeatMapReportGroup
    {
        CSCHeatMap = 1,

        CustomerHeatMap = 2,

        UpcomingSummary = 3,

        PastSummary = 4,

        OwnerOpenWithPenalties = 5,

        OwnerOpenWithoutPenalties = 6,

        OwnerOverdueWithPenalties = 7,

        OwnerOverdueWithoutPenalties = 8
    }
}
