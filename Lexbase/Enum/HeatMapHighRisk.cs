﻿using System.ComponentModel;

namespace Lexbase.Enum
{
    public enum HeatMapHighRisk
    {
        [Description("High Risk Items Description")]
        Highriskdesc = 0,

        [Description("Penalty")]
        Penalty = 1,

        [Description("Due Date")]
        Duedate = 2,

        [Description("Days till Due")]
        Daystilldue = 3,

        [Description("OWNER")]
        Owner = 4,
    }
}
