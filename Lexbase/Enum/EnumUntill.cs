﻿namespace Lexbase.Enum
{
    public static class EnumUntill
    {
        public static T ParseEnum<T>(string value)
        {
            return (T)System.Enum.Parse(typeof(T), value, true);
        }
    }
}
