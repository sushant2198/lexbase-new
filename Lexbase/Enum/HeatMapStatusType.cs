﻿using System.ComponentModel;

namespace Lexbase.Enum
{
    public enum HeatMapStatusType
    {
        [Description("COMPLETED BY DUE DATE")]
        CompletedByDueDate = 1,

        [Description("STILL OPEN")]
        StillOpen = 2,

        [Description("COMPLETED LATE")]
        CompletedLate = 3
    }
}
