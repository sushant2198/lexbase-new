﻿using System.ComponentModel;

namespace Lexbase.Enum
{
    public enum HeatMapReportTypes
    {

        [Description("Open With Penalties")]
        OpenWithPenalties = 1,

        [Description("Overdue with Penalties")]
        OverdueWithPenalties = 2,

        [Description("Open (Without Penalties)")]
        OpenWithoutPenalties = 3,

        [Description("Overdue (Without Penalties)")]
        OverdueWithoutPenalties = 4,

        [Description("Total Open Obligations")]
        TotalOpenObligations = 5,

        [Description("Completed By Due Date")]
        CompletedByDueDate = 6,

        [Description("Completed After Due Date")]
        CompletedAfterDueDate = 7,

        [Description("Total Completed")]
        TotalCompleted = 8
    }
}
