﻿using System.ComponentModel;

namespace Lexbase.Enum
{
    public enum HeatMapDurationTypes
    {
        [Description("Totals")]
        OldTotal = 0,

        [Description("Due <5 days")]
        DueFivedays = 1,

        [Description("Total")]
        Total = 2,

        [Description("Older")]
        Older = 3,
    }
}
