using Lexbase.Models;

namespace Lexbase.Services
{
    public interface IObligationService
    {
        void Activate(long id);

        void Schedule(long id);

        void MarkOverDue(ChildObligation item);

        void SetStatus(ChildObligation item, ObligationTriggerLog trigger);
    }
}