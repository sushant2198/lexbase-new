using Lexbase.Models;

namespace Lexbase.Services
{
    public interface ISAService
    {
        void ActivateMSA(long id, Account account = null);

        void ActivateSA(long id, Account account = null);
    }
}