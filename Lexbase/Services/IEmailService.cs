using Lexbase.Models;
using Lexbase.Providers.Models;

namespace Lexbase.Services
{
    public interface IEmailService
    {
        void Send(User to, string templateName, MessageAttachment attachment = null);

        void Send(User to, string templateName, ChangeRequest item);

        void Send(User to, string templateName, Account account, MessageAttachment attachment = null, Document document = null);
    }
}