using Lexbase.Models;
using Lexbase.Providers.Models;
using System.Collections.Generic;

namespace Lexbase.Services
{
    public interface IExcelService
    {
        MessageAttachment Create(Account account, IDictionary<string, IList<Obligation>> obListMap, string name);

        MessageAttachment Create(Dictionary<string, IList<ObligationTriggerLog>> obligationTriggerLogMap, string name);

        MessageAttachment Create(IDictionary<string, IList<ChildObligation>> itemMap, string name);

        MessageAttachment Create(IDictionary<string, IList<UserAuditLog>> maps, string name);
        MessageAttachment Create(IDictionary<string, HeatMapReport> itemMap, string name);

        MessageAttachment Create(AccountDump dump, string name);

        MessageAttachment Create(IDictionary<string, AccountTrendReportFunctional> maps, string name);

        MessageAttachment Create(IDictionary<string, IList<AccountTrendReportCumulative>> maps, string name);

        MessageAttachment Create(IDictionary<string, IList<AccountInformationData>> maps, string name, string LeadCCM);
    }
}