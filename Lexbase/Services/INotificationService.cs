namespace Lexbase.Services
{
    public interface INotificationService
    {
        bool Send(int timezoneId);

        void SendChangeRequests(int timezoneId);
    }
}