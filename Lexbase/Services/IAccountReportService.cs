﻿using Lexbase.Models;

namespace Lexbase.Services
{
    public interface IAccountReportService
    {
        HeatMapReport GetHeatMap(long accountId);

        AccountTrendReportFunctional GetAccountTrendFunctional(long accountId);
        AccountTrendReportCumulative GetAccountTrendCumulative(long accountId);
    }
}