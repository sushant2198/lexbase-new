using Lexbase.Models;
using System.Collections.Generic;

namespace Lexbase.Services
{
    public interface IAccountService
    {
        void Activate(long accountId);

        void UpdateAccountStatus(IList<Account> accountStaus);

        AccountDump GetAccountDump(long accountId);
    }
}