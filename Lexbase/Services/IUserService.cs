using Lexbase.Models;
using System.Collections.Generic;

namespace Lexbase.Services
{
    public interface IUserService
    {
        IList<User> GetManagers(long accountId);

        IList<User> GetUsers(long accountId, List<User> exclude = null);

        void Activate(User user);

        IList<User> GetManagersForEmail(Account account, EmailType emailType);
    }
}