using Microsoft.Extensions.DependencyInjection;

namespace Lexbase.Register
{
    public interface IRegister
    {
        void Register(ServiceCollection collection);
    }
}