﻿using Lexbase.Models;
using Lexbase.Queries;

namespace Lexbase.Repositories
{
    public interface IChangeRequestRepository : IRepository<ChangeRequest, long, UserQuery>
    { }
}