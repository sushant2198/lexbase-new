﻿using Lexbase.Models;
using Lexbase.Queries;
using System;
using System.Collections.Generic;

namespace Lexbase.Repositories
{
    public interface IChildObligationRepository : IRepository<ChildObligation, long, ChildObligationQuery>
    {
        DateTime Clean(long obligationId);

        void SetStatus(ChildObligation item, string status);

        ChildObligation Create(ChildObligation item);

        IList<ChildObligation> GetByAccount(long accountId);
    }
}