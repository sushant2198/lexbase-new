using Lexbase.Models;
using Lexbase.Queries;

namespace Lexbase.Repositories
{
    public interface IBusinessUnitRepository : IRepository<BusinessUnit, long, BusinessUnitQuery> { }
}