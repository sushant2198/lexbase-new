using Lexbase.Models;
using Lexbase.Queries;

namespace Lexbase.Repositories
{
    public interface IAccountCoverageRepository : IRepository<AccountCoverage, long, AccountCoverageQuery> { }
}