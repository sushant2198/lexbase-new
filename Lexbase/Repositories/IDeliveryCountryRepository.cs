using Lexbase.Models;
using Lexbase.Queries;

namespace Lexbase.Repositories
{
    public interface IDeliveryCountryRepository : IRepository<DeliveryCountry, long, DeliveryCountryQuery> { }
}