using Lexbase.Models;
using Lexbase.Queries;
using System;
using System.Collections.Generic;

namespace Lexbase.Repositories
{
    public interface IAccountRepository : IRepository<Account, long, AccountQuery>
    {
        IList<User> GetManagers(long accountId);

        void StartActivation(long id);

        void CompleteActivation(long id);

        IList<Account> GetAccountStatus();

        void CreateAccountStatusByFG(AccountStatusData item);

        void DeleteAccountStatusByFG(AccountStatusData item);

        void CreateAccountStatusData(AccountStatusData item);

        IList<AccountFreezingStatus> GetAccountFreezingStatus(DateTime StartDate, DateTime EndDate);

        void UpdateInActiveAccountStatus(AccountStatus accountStatus, int isActive);

        List<AccountTrendData> GetAccountTrendData(long accountId);

        List<AccountInformationData> GetAccountInformationReport();

        List<HeatMapData> GetTrendDataForHeatMap(long accountId);
    }
}