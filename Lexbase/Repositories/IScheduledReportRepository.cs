﻿using Lexbase.Models;
using Lexbase.Queries;

namespace Lexbase.Repositories
{
    public interface IScheduledReportRepository : IRepository<ScheduledReport, long, ScheduledReportQuery>
    {
        void StartProcessing(long id);

        void CompleteProcessing(long id, string error);

        void Clean();
    }
}