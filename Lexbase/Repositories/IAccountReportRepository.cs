﻿using Lexbase.Models;
using Lexbase.Queries;
using System.Collections.Generic;

namespace Lexbase.Repositories
{
    public interface IAccountReportRepository
    {
        //HeatMapData GetHeatMapReportCurrentMonthCount(HeatMapQuery query);

        //List<HeatMapData> GetHeatMapReportHisMonthCount(HeatMapQuery query);

        //List<HeatMapData> GetHeatMapOwnerReportCurrentMonthCount(HeatMapQuery query);

        //List<HeatMapData> GetHeatMapOwnerReportHisCount(HeatMapQuery query);
        List<HeatMapData> GetHeatMapChartData(HeatMapQuery query);
        List<HeatMapData> GetHeatMap90ChartData(HeatMapQuery query);
        List<HeatMapData> GetHeatMapReport(HeatMapQuery query);
        List<HeatMapData> GetHighRiskForHeatMap(long accountId);
        void GenerateHeatMap(HeatMapQuery query);

    }
}