﻿using Lexbase.Models;
using Lexbase.Queries;

namespace Lexbase.Repositories
{
    public interface IReminderFrequencyRepository : IRepository<ReminderFrequency, long, ReminderFrequencyQuery> { }
}