using Lexbase.Models;
using Lexbase.Queries;

namespace Lexbase.Repositories
{
    public interface IAccountCurrencyRepository : IRepository<AccountCurrency, long, AccountCurrencyQuery> { }
}