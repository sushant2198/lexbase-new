﻿using Lexbase.Models;
using Lexbase.Queries;

namespace Lexbase.Repositories
{
    public interface IObligationTriggerLogRepository : IRepository<ObligationTriggerLog, long, ObligationTriggerLogQuery>
    {
        ObligationTriggerLog Create(ObligationTriggerLog item);

        void SetSent(long id, System.DateTime sentDate);
    }
}