using Lexbase.Models;
using Lexbase.Queries;
using System.Collections.Generic;

namespace Lexbase.Repositories
{
    public interface IObligationRepository : IRepository<Obligation, long, ObligationQuery>
    {
        IList<User> GetUsers(long accountId);

        string GetActionTypeName(string code);

        string GetTimeZoneForObligation(Obligation item);

        void StartActivation(long id);

        void CompleteActivation(long id);

        void StartProcessing(long id);

        void CompleteProcessing(Obligation item);

        IList<ObligationType> GetObligationTypes();
    }
}