using System.Collections.Generic;

namespace Lexbase.Repositories
{
    public interface IRepository<TModel, TId, TQuery>
    {
        TModel Get(TId id);

        IList<TModel> Search(TQuery query);
    }
}