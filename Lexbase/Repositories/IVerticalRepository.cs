using Lexbase.Models;
using Lexbase.Queries;

namespace Lexbase.Repositories
{
    public interface IVerticalRepository : IRepository<Vertical, long, VerticalQuery> { }
}