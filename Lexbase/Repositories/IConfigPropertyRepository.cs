using System.Collections.Generic;

namespace Lexbase.Repositories
{
    public interface IConfigPropertyRepository
    {
        IDictionary<string, object> GetPropertiesValue(string[] keys);
    }
}