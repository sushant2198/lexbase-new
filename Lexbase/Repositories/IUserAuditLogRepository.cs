﻿using Lexbase.Models;
using Lexbase.Queries;

namespace Lexbase.Repositories
{
    public interface IUserAuditLogRepository : IRepository<UserAuditLog, long, UserAuditLogQuery>
    {
    }
}