using Lexbase.Models;
using Lexbase.Queries;

namespace Lexbase.Repositories
{
    public interface ITimezoneRepository : IRepository<Timezone, long, TimezoneQuery> { }
}