using Lexbase.Models;
using Lexbase.Queries;

namespace Lexbase.Repositories
{
    public interface ISARepository : IRepository<ServiceAgreement, long, ServiceAgreementQuery>
    {
        void StartActivation(long id);

        void CompleteActivation(long id);
    }
}