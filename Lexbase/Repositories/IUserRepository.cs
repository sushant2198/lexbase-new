﻿using Lexbase.Models;
using Lexbase.Queries;

namespace Lexbase.Repositories
{
    public interface IUserRepository : IRepository<User, long, UserQuery>
    {
        // IList<User> Search(IList<long> userIds, bool? isActive = null);

        // IList<User> GetManagers(long accountId);

        // IList<long> GetInactiveManager(long accountId);

        void DeActivate(long userId);

        void StartActivation(long userId);

        void CompleteActivation(long userId);
    }
}