using Lexbase.Models;
using Lexbase.Queries;

namespace Lexbase.Repositories
{
    public interface IMSARepository : IRepository<MServiceAgreement, long, MServiceAgreementQuery>
    {
        void StartActivation(long id);

        void CompleteActivation(long id);
    }
}