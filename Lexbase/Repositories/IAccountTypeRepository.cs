using Lexbase.Models;
using Lexbase.Queries;

namespace Lexbase.Repositories
{
    public interface IAccountTypeRepository : IRepository<AccountType, long, AccountTypeQuery> { }
}