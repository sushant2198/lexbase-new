namespace Lexbase.Queries
{
    public class BusinessUnitQuery
    {
        public long? AccountId { get; set; }
    }
}