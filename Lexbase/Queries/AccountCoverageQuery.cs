namespace Lexbase.Queries
{
    public class AccountCoverageQuery
    {
        public long? AccountId { get; set; }
    }
}