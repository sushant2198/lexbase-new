﻿using System;
using System.Collections.Generic;

namespace Lexbase.Queries
{
    public class ObligationTriggerLogQuery
    {
        public List<long> AccountIds { get; set; }
        public DateTime? When { get; set; }
    }
}