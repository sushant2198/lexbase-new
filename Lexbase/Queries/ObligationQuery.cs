﻿using Lexbase.Models;

namespace Lexbase.Queries
{
    public class ObligationQuery
    {
        public long? AccountId { get; set; }

        public bool? InActive { get; set; }

        public long? OwnerId { get; set; }

        public bool? IsTrigger { get; set; }

        public string ActionType { get; set; }

        public bool ForScheduler { get; set; }

        public long? GroupId { get; set; }

        public GroupFor? GroupType { get; set; }
        // public string OrderBy { get; set; }
    }
}