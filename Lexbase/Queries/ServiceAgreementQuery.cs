using Lexbase.Models;

namespace Lexbase.Queries
{
    public class ServiceAgreementQuery
    {
        public long? AccountId { get; set; }

        public bool? InActive { get; set; }

        public long? ParentId { get; set; }

        public ParentFor? ParentType { get; set; }

        public bool NeedActivation { get; set; }

        public bool IsActive { get; set; }
    }
}