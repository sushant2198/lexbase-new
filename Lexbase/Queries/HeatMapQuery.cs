﻿namespace Lexbase.Queries
{
    public class HeatMapQuery
    {
        public long AccountId { get; set; }
        public int? StatusType { get; set; }
        public int? FrequencyType { get; set; }
    }
}