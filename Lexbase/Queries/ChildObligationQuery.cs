﻿using System;
using System.Collections.Generic;

namespace Lexbase.Queries
{
    public class ChildObligationQuery
    {
        public List<long> AccountIds { get; set; }

        public DateTime? DueDate { get; set; }

        public long? AccountId { get; set; }

        public long? ParentId { get; set; }

        public long? Id { get; set; }

        public string Status { get; set; }

        public DateTime? Date { get; set; }

        public string ActionType { get; set; }

        public int? ReportPeriod { get; set; }

    }
}