namespace Lexbase.Queries
{
    public class AccountQuery
    {
        public bool NeedActivation { get; set; }
        public int? TimezoneId { get; set; }
        public bool? IsActive { get; set; }
    }
}