using System;

namespace Lexbase.Queries
{
    public class TimezoneQuery
    {
        public DateTime? Time { get; set; }
    }
}