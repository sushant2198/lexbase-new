﻿namespace Lexbase.Queries
{
    public class ReminderFrequencyQuery
    {
        public long? FrequencyId { get; set; }
        public string Names { get; set; }
    }
}