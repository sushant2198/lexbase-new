using System.Collections.Generic;

namespace Lexbase.Queries
{
    public class UserQuery
    {
        public long? AccountId { get; set; }

        public List<long> UserIds { get; set; }
    }
}