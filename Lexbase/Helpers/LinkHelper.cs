﻿using Lexbase.Models;

namespace Lexbase.Helpers
{
    public static class LinkHelper
    {
        public static string GetApplicationLink()
        {
            return UlxConfig.Get("Links:Application");
        }

        public static string GetChildObligationLink(ChildObligation item)
        {
            var rootUrl = UlxConfig.Get("Links:Application");
            var template = UlxConfig.GetTemplate("Links:ChildObligation");

            return template(new
            {
                Application = rootUrl,
                ChildObligation = item
            });
        }

        public static string GetObligationLink(Obligation item)
        {
            var rootUrl = UlxConfig.Get("Links:Application");
            var template = UlxConfig.GetTemplate("Links:Obligation");

            return template(new
            {
                Application = rootUrl,
                Obligation = item
            });
        }


        public static string GetHeatMapDataLink(HeatMapData item)
        {
            var rootUrl = UlxConfig.Get("Links:Application");
            var template = UlxConfig.GetTemplate("Links:HeatMapData");

            return template(new
            {
                Application = rootUrl,
                HeatMapData = item
            });
        }

        public static string GetAccountLink(Account item)
        {
            var rootUrl = UlxConfig.Get("Links:Application");
            var template = UlxConfig.GetTemplate("Links:Account");

            return template(new
            {
                Application = rootUrl,
                Account = item
            });
        }

        public static string GetChangeRequestLink(ChangeRequest item)
        {
            var rootUrl = UlxConfig.Get("Links:Application");
            var template = UlxConfig.GetTemplate("Links:ChangeRequest");

            return template(new
            {
                Application = rootUrl,
                ChangeRequest = item
            });
        }

        public static string GetReportsLink()
        {
            var rootUrl = UlxConfig.Get("Links:Application");
            var template = UlxConfig.GetTemplate("Links:Reports");

            return template(new
            {
                Application = rootUrl
            });
        }

        public static string GetTrainingsLink()
        {
            var rootUrl = UlxConfig.Get("Links:Application");
            var template = UlxConfig.GetTemplate("Links:Trainings");

            return template(new
            {
                Application = rootUrl
            });
        }
    }
}
