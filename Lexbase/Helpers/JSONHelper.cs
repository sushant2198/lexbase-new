﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Lexbase.Helpers
{
    public static class JsonHelper
    {
        public static string ToJsonString(IList<IDictionary<string, string>> keyValuePairs)
        {
            return JArray.FromObject(keyValuePairs).ToString();
        }

        public static string ToJsonString(string value, IDictionary<string, string> keyValuePairs)
        {
            if (string.IsNullOrEmpty(value))
            {
                value = "[]";
            }

            var array = JArray.Parse(value);
            var obj = JObject.FromObject(keyValuePairs);
            array.Add(obj);
            return array.ToString();
        }
    }
}