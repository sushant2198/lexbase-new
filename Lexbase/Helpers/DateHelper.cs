﻿using Lexbase.Models;
using System;
using System.Collections.Generic;

namespace Lexbase.Helpers
{
    public class DateHelper
    {
        public static IList<DateTime> GetDates(DateTime startDate, DateTime endDate, string periodicityType, int periodicityValue)
        {
            var retVal = new List<DateTime>();

            startDate = ToBOD(startDate);

            switch (periodicityType.ToLower())
            {
                case "none":
                    return retVal;

                case "once":
                    retVal.Add(startDate);
                    return retVal;

                case "day":
                    for (DateTime date = startDate; date <= endDate; date = date.AddDays(periodicityValue))
                    {
                        var weekDay = ToWeekDay(date);
                        if (!retVal.Contains(weekDay) && weekDay >= startDate)
                        {
                            retVal.Add(date);
                        }
                    }
                    return retVal;

                case "week":
                    for (DateTime date = startDate; date <= endDate; date = date.AddDays(7 * periodicityValue))
                    {
                        var weekDay = ToWeekDay(date);
                        if (weekDay >= startDate)
                        {
                            retVal.Add(date);
                        }
                    }
                    return retVal;

                case "month":
                    for (DateTime date = startDate; date <= endDate; date = date.AddMonths(periodicityValue))
                    {
                        var weekDay = ToWeekDay(date);
                        if (weekDay >= startDate)
                        {
                            retVal.Add(date);
                        }
                    }
                    return retVal;

                case "year":
                    for (DateTime date = startDate; date <= endDate; date = date.AddYears(periodicityValue))
                    {
                        var weekDay = ToWeekDay(date);
                        if (weekDay >= startDate)
                        {
                            retVal.Add(date);
                        }
                    }
                    return retVal;

                default:
                    return retVal;
            }
        }


        public static DateTime GetNext(DateTime date, string periodicityType, int periodicityValue, bool onlyFuture = true)
        {
            DateTime value;
            switch (periodicityType.ToLower())
            {
                case "none":
                    return ToWeekDay(date);

                case "once":
                    return ToWeekDay(date);

                case "day":
                    value = date.AddDays(periodicityValue);
                    break;

                case "week":
                    value = date.AddDays(7 * periodicityValue);
                    break;

                case "month":
                    value = date.AddMonths(periodicityValue);
                    break;

                case "year":
                    value = date.AddYears(periodicityValue);
                    break;

                default:
                    value = date;
                    break;
            }

            if (onlyFuture && !IsFuture(value))
            {
                return GetNext(value, periodicityType, periodicityValue);
            }

            return value;
        }

        public static DateTime Prepone(DateTime date, int by, string period)
        {
            DateTime value;
            switch (period.ToLower())
            {
                case "day":
                    value = date.AddDays(-by);
                    break;

                case "week":
                    value = date.AddDays(-7 * by);
                    break;

                case "month":
                    value = date.AddMonths(-by);
                    break;

                case "year":
                    value = date.AddYears(-by);
                    break;

                default:
                    value = date;
                    break;
            }

            return value;
        }

        public static bool IsSame(DateTime value1, DateTime value2)
        {
            return ToBOD(value1) == ToBOD(value2);
        }

        public static bool IsSameMonth(DateTime value1, DateTime value2)
        {
            return value1.Year == value2.Year && value1.Month == value2.Month;
        }
        public static bool IsFuture(DateTime date)
        {
            var bod = ToBOD(date);
            var today = ToBOD(DateTime.Today);
            return bod > today;
        }

        public static bool IsPast(DateTime date)
        {
            var bod = ToBOD(date);
            var today = ToBOD(DateTime.Today);
            return bod < today;
        }

        public static bool IsToday(DateTime date)
        {
            var bod = ToBOD(date);
            var today = ToBOD(DateTime.Today);
            return bod == today;
        }

        public static DateTime ToWeekDay(DateTime date)
        {
            if (date.DayOfWeek == DayOfWeek.Saturday)
            {
                return date.AddDays(-1);
            }

            if (date.DayOfWeek == DayOfWeek.Sunday)
            {
                return date.AddDays(-2);
            }

            return date;
        }
        public static IList<DateTime> Last14Months()
        {
            var lastYearMonths = new List<DateTime>();

            var firstDayOfMonth = ToBOM(DateTime.Today);

            for (var i = 1; i <= 14; i++)
            {
                lastYearMonths.Add(firstDayOfMonth.AddMonths(-i));
            }
            return lastYearMonths;
        }
        public static IList<DateTime> Last12Months()
        {
            var lastYearMonths = new List<DateTime>();

            var firstDayOfMonth = ToBOM(DateTime.Today);

            for (var i = 0; i < 12; i++)
            {
                lastYearMonths.Add(firstDayOfMonth.AddMonths(-i));
            }
            return lastYearMonths;
        }

        public static DateTime ToBOD(DateTime value, Timezone timezone = null)
        {
            var date = value.ToUniversalTime();
            return new DateTime(value.Year, value.Month, value.Day, 0, 0, 0, DateTimeKind.Utc);
        }

        public static DateTime ToBOM(DateTime? value)
        {
            if (!value.HasValue)
            {
                value = DateTime.Today;
            }
            var date = value.Value.ToUniversalTime();
            return new DateTime(date.Year, date.Month, 1, 0, 0, 0, DateTimeKind.Utc);
        }

        public static string ToDateString(DateTime value)
        {
            return value.ToString(UlxConfig.Get("Formats:Date"));
        }

        public static string ToMonthString(DateTime value)
        {
            return value.ToString(UlxConfig.Get("Formats:Month"));
        }

        public static string ToDateTimeString(DateTime value)
        {
            return value.ToString(UlxConfig.Get("Formats:DateTime"));
        }

        public static string ToTimeString(DateTime value)
        {
            return value.ToString(UlxConfig.Get("Formats:Time"));
        }
    }
}