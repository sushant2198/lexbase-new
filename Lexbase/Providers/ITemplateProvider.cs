using Lexbase.Providers.Models;

namespace Lexbase.Providers
{
    public interface ITemplateProvider
    {
        EmailTemplate GetEmailTemplate(string templateName);
    }
}