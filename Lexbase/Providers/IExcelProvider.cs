using Lexbase.Providers.Models;

namespace Lexbase.Providers
{
    public interface IExcelProvider
    {
        IExcel CreateExcel(string name);
    }
}