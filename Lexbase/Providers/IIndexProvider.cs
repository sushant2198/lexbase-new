﻿namespace Lexbase.Providers
{
    public interface IIndexProvider
    {
        void ReIndex(string name);
    }
}
