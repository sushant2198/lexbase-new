﻿namespace Lexbase.Providers.Models
{
    public class MessageAttachment
    {
        public string Path { get; set; }
        public string FileName { get; set; }
    }
}