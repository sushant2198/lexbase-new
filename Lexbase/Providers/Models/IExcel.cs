using NPOI.HSSF.UserModel;
using System.Collections.Generic;

namespace Lexbase.Providers.Models
{
    public interface IExcel
    {
        string Name { get; set; }

        HSSFWorkbook Workbook { get; }

        Sheet CreateSheet(string sheetName);

        IDictionary<string, Sheet> Sheets { get; }

        Sheet ActiveSheet { get; }

        string Save();
    }
}