﻿using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;

namespace Lexbase.Providers.Models
{
    public class Sheet
    {
        private ISheet sheet;

        //private IExcel excel;

        private ICellStyle headerCellStyle;

        private ICellStyle columnCellStyle;

        private ICellStyle dataCellStyle;

        private readonly ICellStyle hyperlinkCellStyle;
        private readonly ICellStyle backGroundBlackCellStyle;
        private readonly ICellStyle backGroundBlueCellStyle;
        private readonly ICellStyle backGroundGreenCellStyle;
        private readonly ICellStyle backGroundAmberCellStyle;
        private readonly ICellStyle backGroundRedCellStyle;
        private readonly ICellStyle backGroundGreyCellStyle;
        private readonly ICellStyle backGroundWhiteCellStyle;
        private readonly ICellStyle backGroundPinkCellStyle;
        private readonly ICellStyle backGroundOrangeCellStyle;
        private readonly ICellStyle backGroundCreamCellStyle;
        private readonly ICellStyle backGroundCoralCellStyle;
        private readonly ICellStyle backGroundHyperBlackCellStyle;
        private readonly ICellStyle backGroundHyperBlueCellStyle;
        private readonly ICellStyle backGroundHyperGreenCellStyle;
        private readonly ICellStyle backGroundHyperAmberCellStyle;
        private readonly ICellStyle backGroundHyperRedCellStyle;
        private readonly ICellStyle backGroundHyperGreyCellStyle;
        private readonly ICellStyle backGroundHyperWhiteCellStyle;
        private readonly ICellStyle backGroundHyperPinkCellStyle;
        private readonly ICellStyle backGroundHyperOrangeCellStyle;
        private readonly ICellStyle backGroundHyperCreamCellStyle;
        private readonly ICellStyle backGroundHyperCoralCellStyle;
        private IFont headerFont;

        public Sheet(
            ISheet sheet,
            //IExcel excel,
            ICellStyle headerCellStyle,
            ICellStyle columnCellStyle,
            ICellStyle dataCellStyle,
            ICellStyle hyperlinkCellStyle,
            ICellStyle backGroundBlackCellStyle,
            ICellStyle backGroundBlueCellStyle,
            ICellStyle backGroundGreenCellStyle,
            ICellStyle backGroundAmberCellStyle,
            ICellStyle backGroundRedCellStyle,
            ICellStyle backGroundGreyCellStyle,
            ICellStyle backGroundWhiteCellStyle,
            ICellStyle backGroundPinkCellStyle,
            ICellStyle backGroundOrangeCellStyle,
            ICellStyle backGroundCreamCellStyle,
            ICellStyle backGroundCoralCellStyle,
            ICellStyle backGroundHyperBlackCellStyle,
            ICellStyle backGroundHyperBlueCellStyle,
            ICellStyle backGroundHyperGreenCellStyle,
            ICellStyle backGroundHyperAmberCellStyle,
            ICellStyle backGroundHyperRedCellStyle,
            ICellStyle backGroundHyperGreyCellStyle,
            ICellStyle backGroundHyperWhiteCellStyle,
            ICellStyle backGroundHyperPinkCellStyle,
            ICellStyle backGroundHyperOrangeCellStyle,
            ICellStyle backGroundHyperCreamCellStyle,
            ICellStyle backGroundHyperCoralCellStyle,
            IFont headerFont
        )
        {
            //this.excel = excel;
            this.headerCellStyle = headerCellStyle;
            this.columnCellStyle = columnCellStyle;
            this.dataCellStyle = dataCellStyle;
            this.hyperlinkCellStyle = hyperlinkCellStyle;
            this.backGroundBlackCellStyle = backGroundBlackCellStyle;
            this.backGroundBlueCellStyle = backGroundBlueCellStyle;
            this.backGroundGreenCellStyle = backGroundGreenCellStyle;
            this.backGroundAmberCellStyle = backGroundAmberCellStyle;
            this.backGroundRedCellStyle = backGroundRedCellStyle;
            this.backGroundGreyCellStyle = backGroundGreyCellStyle;
            this.backGroundWhiteCellStyle = backGroundWhiteCellStyle;
            this.backGroundPinkCellStyle = backGroundPinkCellStyle;
            this.backGroundOrangeCellStyle = backGroundOrangeCellStyle;
            this.backGroundCreamCellStyle = backGroundCreamCellStyle;
            this.backGroundCoralCellStyle = backGroundCoralCellStyle;
            this.backGroundHyperBlackCellStyle = backGroundHyperBlackCellStyle;
            this.backGroundHyperBlueCellStyle = backGroundHyperBlueCellStyle;
            this.backGroundHyperGreenCellStyle = backGroundHyperGreenCellStyle;
            this.backGroundHyperAmberCellStyle = backGroundHyperAmberCellStyle;
            this.backGroundHyperRedCellStyle = backGroundHyperRedCellStyle;
            this.backGroundHyperGreyCellStyle = backGroundHyperGreyCellStyle;
            this.backGroundHyperWhiteCellStyle = backGroundHyperWhiteCellStyle;
            this.backGroundHyperPinkCellStyle = backGroundHyperPinkCellStyle;
            this.backGroundHyperOrangeCellStyle = backGroundHyperOrangeCellStyle;
            this.backGroundHyperCreamCellStyle = backGroundHyperCreamCellStyle;
            this.backGroundHyperCoralCellStyle = backGroundHyperCoralCellStyle;
            this.headerFont = headerFont;
            this.sheet = sheet;
            this.sheet.DefaultColumnWidth = UnitedLexConstants.DEFAULT_COLUMN_WIDTH;
            //this.sheet.DefaultRowHeight = UnitedLexConstants.DEFAULT_ROW_HEIGHT;
        }

        public ICell AddHeaderRow(string text, int rowNo = 0, int colNo = 0)
        {
            var headerString = new HSSFRichTextString(text);
            headerString.ApplyFont(headerFont);

            var row = sheet.CreateRow(rowNo);

            var cell = row.CreateCell(colNo);
            cell.CellStyle = GetStyle("style:header"); ;

            cell.SetCellValue(headerString);

            return cell;
        }

        public void AddHeaderColumns(IList<string> columns, int rowNo)
        {
            var row = sheet.CreateRow(rowNo);

            var colNo = 0;

            foreach (var item in columns)
            {
                var headerColumnString = new HSSFRichTextString(item);
                var cell = row.CreateCell(colNo++);
                cell.SetCellValue(headerColumnString);
                cell.CellStyle = GetStyle("style:column");
            }
        }

        public void AddHeaderColumn(string text, int rowNo, int colNo = 0)
        {
            var row = sheet.GetRow(rowNo);
            var cell = row.GetCell(colNo);

            if (!string.IsNullOrEmpty(text) && text.StartsWith("[") && text.EndsWith("]") && text.Contains("|"))
            {
                var style = "style:data";
                var parts = text.Split('|');
                var part = parts[0].Substring(1);

                var linkval = parts[1];// TODO
                if (linkval.Contains("HeatMapData"))
                {
                    if (int.TryParse(part, out int a))
                    {
                        cell.SetCellValue(a);
                    }
                }
                else
                {
                    cell.SetCellValue(part);
                }
                var attribute = parts[1].Substring(0, parts[1].Length - 1);
                if (attribute.StartsWith("style"))
                {
                    style = attribute;
                }

                if (attribute.StartsWith("http"))
                {
                    cell.Hyperlink = new HSSFHyperlink(HyperlinkType.Url)
                    {
                        Address = attribute
                    };

                    style = "style:HyperLink";
                    if (parts.Length > 2 && parts[2].StartsWith("style"))
                    {
                        if (attribute.Contains("HeatMapData"))
                        {
                            var style1 = parts[2].Substring(0, parts[2].Length - 1);
                            var len = style1.Length;
                            style = style + style1.Substring(6, len - 6);
                        }
                        else
                        {
                            style = parts[2].Substring(0, parts[2].Length - 1);
                        }
                    }
                }
                cell.CellStyle = GetStyle(style);
            }
            else
            {
                cell.SetCellValue(text);
            }

        }

        public void AddDataRowColumn(string data, int rowNo)
        {
            var row = sheet.CreateRow(rowNo);
            var colNo = 0;

            var cell = row.CreateCell(colNo++);

            cell.SetCellValue(new HSSFRichTextString(data));
            cell.CellStyle = GetStyle("style:data");
        }


        public void AddData(string data, int rowNo, int colNo, string style = "style:data")
        {
            var row = sheet.GetRow(rowNo);

            if (row == null)
            {
                row = sheet.CreateRow(rowNo);
            }

            var cell = row.CreateCell(colNo);

            cell.SetCellValue(new HSSFRichTextString(data));
            cell.CellStyle = GetStyle(style);
        }

        public void AddDataRow(IList<string> dataRow, int rowNo)
        {
            var row = sheet.CreateRow(rowNo);
            var colNo = 0;

            foreach (var item in dataRow)
            {
                var cell = row.CreateCell(colNo++);

                var style = "style:data";

                // if format is [Link|http://a.com] TODO: use regex
                if (!string.IsNullOrEmpty(item) && item.StartsWith("[") && item.EndsWith("]") && item.Contains("|"))
                {
                    var parts = item.Split('|');
                    var text = parts[0].Substring(1);

                    //cell.SetCellValue(text);

                    var linkval = parts[1];// TODO
                    if (linkval.Contains("HeatMapData"))
                    {
                        Int32 a;
                        if (Int32.TryParse(text, out a))
                        {
                            a = Convert.ToInt32(text);
                            cell.SetCellValue(a);
                        }
                        else
                        {
                            cell.SetCellValue(text);
                        }
                    }
                    else
                    {
                        cell.SetCellValue(text);
                    }


                    var attribute = parts[1].Substring(0, parts[1].Length - 1);

                    if (attribute.StartsWith("style"))
                    {
                        style = attribute;
                    }

                    if (attribute.StartsWith("http"))
                    {
                        cell.Hyperlink = new HSSFHyperlink(HyperlinkType.Url)
                        {
                            Address = attribute
                        };

                        style = "style:HyperLink";
                        if (parts.Length > 2 && parts[2].StartsWith("style"))
                        {
                            if (attribute.Contains("HeatMapData"))
                            {
                                var style1 = parts[2].Substring(0, parts[2].Length - 1);
                                var len = style1.Length;
                                style = style + style1.Substring(6, len - 6);
                            }
                            else
                            {
                                style = parts[2].Substring(0, parts[2].Length - 1);
                            }
                        }
                    }
                }
                else
                {
                    cell.SetCellValue(new HSSFRichTextString(item));
                }

                cell.CellStyle = GetStyle(style);
            }
        }


        private ICellStyle GetStyle(string style)
        {
            switch (style.ToLower())
            {
                case "style:header":
                    return headerCellStyle;

                case "style:column":
                    return columnCellStyle;

                case "style:data":
                    return dataCellStyle;

                case "style:hyperlink":
                    return hyperlinkCellStyle;

                case "style:blackbg":
                    return backGroundBlackCellStyle;

                case "style:bluebg":
                    return backGroundBlueCellStyle;

                case "style:greenbg":
                    return backGroundGreenCellStyle;

                case "style:amberbg":
                    return backGroundAmberCellStyle;

                case "style:redbg":
                    return backGroundRedCellStyle;

                case "style:greybg":
                    return backGroundGreyCellStyle;

                case "style:whitebg":
                    return backGroundWhiteCellStyle;

                case "style:pinkbg":
                    return backGroundPinkCellStyle;

                case "style:orangebg":
                    return backGroundOrangeCellStyle;

                case "style:creambg":
                    return backGroundCreamCellStyle;

                case "style:coralbg":
                    return backGroundCoralCellStyle;

                #region Color and Hyperlink
                case "style:hyperlinkblackbg":
                    return backGroundHyperBlackCellStyle;

                case "style:hyperlinkbluebg":
                    return backGroundHyperBlueCellStyle;

                case "style:hyperlinkgreenbg":
                    return backGroundHyperGreenCellStyle;

                case "style:hyperlinkamberbg":
                    return backGroundHyperAmberCellStyle;

                case "style:hyperlinkredbg":
                    return backGroundHyperRedCellStyle;

                case "style:hyperlinkgreybg":
                    return backGroundHyperGreyCellStyle;

                case "style:hyperlinkwhitebg":
                    return backGroundHyperWhiteCellStyle;

                case "style:hyperlinkpinkbg":
                    return backGroundHyperPinkCellStyle;

                case "style:hyperlinkorangebg":
                    return backGroundHyperOrangeCellStyle;

                case "style:hyperlinkcreambg":
                    return backGroundHyperCreamCellStyle;

                case "style:hyperlinkcoralbg":
                    return backGroundHyperCoralCellStyle;
                #endregion

                default:
                    return dataCellStyle;

            }

        }

        public void AddMergeRegion(int firstRow, int lastRow, int firstCol, int lastCol)
        {
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(firstRow, lastRow, firstCol, lastCol));
        }
    }

    public abstract class ExcelBase : IExcel
    {
        private ICellStyle columnCellStyle;
        private ICellStyle dataCellStyle;
        private ICellStyle hyperlinkCellStyle;

        private ICellStyle backGroundWhiteCellStyle;
        private ICellStyle backGroundBlackCellStyle;
        private ICellStyle backGroundBlueCellStyle;
        private ICellStyle backGroundGreenCellStyle;
        private ICellStyle backGroundAmberCellStyle;
        private ICellStyle backGroundRedCellStyle;
        private ICellStyle backGroundGreyCellStyle;
        private ICellStyle backGroundPinkCellStyle;
        private ICellStyle backGroundOrangeCellStyle;
        private ICellStyle backGroundCreamCellStyle;
        private ICellStyle backGroundCoralCellStyle;
        private ICellStyle backGroundHyperBlackCellStyle;
        private ICellStyle backGroundHyperBlueCellStyle;
        private ICellStyle backGroundHyperGreenCellStyle;
        private ICellStyle backGroundHyperAmberCellStyle;
        private ICellStyle backGroundHyperRedCellStyle;
        private ICellStyle backGroundHyperGreyCellStyle;
        private ICellStyle backGroundHyperWhiteCellStyle;
        private ICellStyle backGroundHyperPinkCellStyle;
        private ICellStyle backGroundHyperOrangeCellStyle;
        private ICellStyle backGroundHyperCreamCellStyle;
        private ICellStyle backGroundHyperCoralCellStyle;
        private IFont headerFont;

        private IFont hyperlinkFont;

        private IFont boldFont;
        private IFont invertedBoldFont;
        private ICellStyle headerCellStyle;
        //private HSSFWorkbook templateWorkbook;
        protected ExcelBase(string name)
        {
            Name = name;
            var excelTemplate = $"{UlxConfig.ExcelTemplates}/{name}";
            if (File.Exists(excelTemplate))
            {
                FileStream fs = new FileStream(excelTemplate, FileMode.Open, FileAccess.Read);
                Workbook = new HSSFWorkbook(fs, true);
            }
            else
            {
                Workbook = new HSSFWorkbook();
                HSSFPalette palette = Workbook.GetCustomPalette();

                palette.SetColorAtIndex(HSSFColor.BlueGrey.Index,
                        (byte)66,
                        (byte)66,
                        (byte)111
                    );
            }
            headerFont = Workbook.CreateFont();
            headerFont.FontName = HSSFFont.FONT_ARIAL;
            headerFont.FontHeightInPoints = (short)20;
            headerFont.Boldweight = (short)FontBoldWeight.Bold;
            headerFont.Color = IndexedColors.Red.Index;
            headerFont.Underline = FontUnderlineType.Single;

            hyperlinkFont = Workbook.CreateFont();
            hyperlinkFont.Underline = FontUnderlineType.Single;
            hyperlinkFont.Color = IndexedColors.Blue.Index;

            boldFont = Workbook.CreateFont();
            boldFont.Boldweight = (short)FontBoldWeight.Bold;

            invertedBoldFont = Workbook.CreateFont();
            invertedBoldFont.Boldweight = (short)FontBoldWeight.Bold;
            invertedBoldFont.Color = IndexedColors.White.Index;

            headerCellStyle = CreateStyle("header");
            columnCellStyle = CreateStyle("column");
            dataCellStyle = CreateStyle("data");
            hyperlinkCellStyle = CreateStyle("hyperlink");

            backGroundBlackCellStyle = CreateStyle("blackbg");
            backGroundBlueCellStyle = CreateStyle("bluebg");
            backGroundGreenCellStyle = CreateStyle("greenbg");
            backGroundAmberCellStyle = CreateStyle("amberbg");
            backGroundRedCellStyle = CreateStyle("redbg");
            backGroundGreyCellStyle = CreateStyle("greybg");
            backGroundWhiteCellStyle = CreateStyle("whitebg");
            backGroundPinkCellStyle = CreateStyle("pinkbg");
            backGroundOrangeCellStyle = CreateStyle("orangebg");
            backGroundCreamCellStyle = CreateStyle("creambg");
            backGroundCoralCellStyle = CreateStyle("coralbg");

            backGroundHyperBlackCellStyle = CreateStyle("hyperblackbg");
            backGroundHyperBlueCellStyle = CreateStyle("hyperbluebg");
            backGroundHyperGreenCellStyle = CreateStyle("hypergreenbg");
            backGroundHyperAmberCellStyle = CreateStyle("hyperamberbg");
            backGroundHyperRedCellStyle = CreateStyle("hyperredbg");
            backGroundHyperGreyCellStyle = CreateStyle("hypergreybg");
            backGroundHyperWhiteCellStyle = CreateStyle("hyperwhitebg");
            backGroundHyperPinkCellStyle = CreateStyle("hyperpinkbg");
            backGroundHyperOrangeCellStyle = CreateStyle("hyperorangebg");
            backGroundHyperCreamCellStyle = CreateStyle("hypercreambg");
            backGroundHyperCoralCellStyle = CreateStyle("hypercoralbg");
            Sheets = new Dictionary<string, Sheet>();
        }

        private ICellStyle CreateStyle(string type)
        {
            var style = Workbook.CreateCellStyle();

            style.VerticalAlignment = VerticalAlignment.Top;
            style.WrapText = true;

            var bordered = false;
            switch (type)
            {
                case "header":
                    style.FillForegroundColor = IndexedColors.Grey25Percent.Index;
                    break;

                case "data":
                    bordered = true;
                    break;

                case "column":
                    style.FillForegroundColor = IndexedColors.BlueGrey.Index;
                    style.FillPattern = FillPattern.SolidForeground;
                    style.SetFont(invertedBoldFont);
                    bordered = true;
                    break;

                case "hyperlink":
                    bordered = true;
                    style.SetFont(hyperlinkFont);
                    break;

                case "blackbg":
                    style.FillForegroundColor = IndexedColors.Black.Index;
                    style.FillPattern = FillPattern.SolidForeground;
                    bordered = true;
                    break;

                case "bluebg":
                    style.FillForegroundColor = IndexedColors.Blue.Index;
                    style.FillPattern = FillPattern.SolidForeground;
                    bordered = true;
                    break;

                case "greenbg":
                    style.FillForegroundColor = IndexedColors.Green.Index;
                    style.FillPattern = FillPattern.SolidForeground;
                    bordered = true;
                    break;

                case "amberbg":
                    style.FillForegroundColor = IndexedColors.Yellow.Index;
                    style.FillPattern = FillPattern.SolidForeground;
                    bordered = true;
                    break;

                case "redbg":
                    style.FillForegroundColor = IndexedColors.Red.Index;
                    style.FillPattern = FillPattern.SolidForeground;
                    bordered = true;
                    break;

                case "greybg":
                    style.FillForegroundColor = IndexedColors.Grey25Percent.Index;
                    style.FillPattern = FillPattern.SolidForeground;
                    bordered = true;
                    break;

                case "pinkbg":
                    style.FillForegroundColor = IndexedColors.Rose.Index;
                    style.FillPattern = FillPattern.SolidForeground;
                    bordered = true;
                    break;

                case "orangebg":
                    style.FillForegroundColor = IndexedColors.LightOrange.Index;
                    style.FillPattern = FillPattern.SolidForeground;
                    bordered = true;
                    break;

                case "creambg":
                    style.FillForegroundColor = IndexedColors.LightYellow.Index;
                    style.FillPattern = FillPattern.SolidForeground;
                    bordered = true;
                    break;

                case "coralbg":
                    style.FillForegroundColor = IndexedColors.Coral.Index;
                    style.FillPattern = FillPattern.SolidForeground;
                    bordered = true;
                    break;

                #region color and Link
                case "hyperblackbg":
                    style.SetFont(hyperlinkFont);
                    style.FillForegroundColor = IndexedColors.Black.Index;
                    style.FillPattern = FillPattern.SolidForeground;
                    bordered = true;
                    break;

                case "hyperbluebg":
                    style.SetFont(hyperlinkFont);
                    style.FillForegroundColor = IndexedColors.Blue.Index;
                    style.FillPattern = FillPattern.SolidForeground;
                    bordered = true;
                    break;

                case "hypergreenbg":
                    style.SetFont(hyperlinkFont);
                    style.FillForegroundColor = IndexedColors.Green.Index;
                    style.FillPattern = FillPattern.SolidForeground;
                    bordered = true;
                    break;

                case "hyperamberbg":
                    style.SetFont(hyperlinkFont);
                    style.FillForegroundColor = IndexedColors.Yellow.Index;
                    style.FillPattern = FillPattern.SolidForeground;
                    bordered = true;
                    break;

                case "hyperredbg":

                    style.FillForegroundColor = IndexedColors.Red.Index;
                    style.FillPattern = FillPattern.SolidForeground;
                    style.SetFont(hyperlinkFont);
                    bordered = true;
                    break;

                case "hypergreybg":
                    style.FillForegroundColor = IndexedColors.Grey25Percent.Index;
                    style.FillPattern = FillPattern.SolidForeground;
                    bordered = true;
                    break;

                case "hyperpinkbg":
                    style.SetFont(hyperlinkFont);
                    style.FillForegroundColor = IndexedColors.Rose.Index;
                    style.FillPattern = FillPattern.SolidForeground;
                    bordered = true;
                    break;

                case "hyperorangebg":
                    style.SetFont(hyperlinkFont);
                    style.FillForegroundColor = IndexedColors.LightOrange.Index;
                    style.FillPattern = FillPattern.SolidForeground;
                    bordered = true;
                    break;

                case "hypercreambg":
                    style.SetFont(hyperlinkFont);
                    style.FillForegroundColor = IndexedColors.LightYellow.Index;
                    style.FillPattern = FillPattern.SolidForeground;
                    bordered = true;
                    break;

                case "hypercoralbg":
                    style.SetFont(hyperlinkFont);
                    style.FillForegroundColor = IndexedColors.Coral.Index;
                    style.FillPattern = FillPattern.SolidForeground;
                    bordered = true;
                    break;
                #endregion

                case "whitebg":
                    style.FillForegroundColor = IndexedColors.White.Index;
                    style.FillPattern = FillPattern.SolidForeground;
                    bordered = true;
                    break;

                default:
                    break;
            }

            if (bordered)
            {
                style.BorderBottom = BorderStyle.Thin;
                style.BottomBorderColor = IndexedColors.Black.Index;

                style.BorderTop = BorderStyle.Thin;
                style.TopBorderColor = IndexedColors.Black.Index;

                style.BorderLeft = BorderStyle.Thin;
                style.LeftBorderColor = IndexedColors.Black.Index;

                style.BorderRight = BorderStyle.Thin;
                style.RightBorderColor = IndexedColors.Black.Index;
            }

            return style;
        }

        public string Name { get; set; }

        public HSSFWorkbook Workbook { get; private set; }

        public IDictionary<string, Sheet> Sheets { get; private set; }

        public Sheet ActiveSheet { get; private set; }

        public Sheet CreateSheet(string sheetName)
        {
            ISheet sheet;
            sheet = Workbook.GetSheet(sheetName);
            if (sheet != null)
            {
                //((HSSFSheet)Workbook.GetSheetAt(0)).CopyTo(Workbook, sheetName, true, true);

            }
            else
            {
                sheet = Workbook.CreateSheet(sheetName);
            }
            ActiveSheet = new Sheet(
                sheet,
                //this,
                headerCellStyle,
                columnCellStyle,
                dataCellStyle,
                hyperlinkCellStyle,
                backGroundBlackCellStyle,
                backGroundBlueCellStyle,
                backGroundGreenCellStyle,
                backGroundAmberCellStyle,
                backGroundRedCellStyle,
                backGroundGreyCellStyle,
                backGroundWhiteCellStyle,
                backGroundPinkCellStyle,
                backGroundOrangeCellStyle,
                backGroundCreamCellStyle,
                backGroundCoralCellStyle,
                backGroundHyperBlackCellStyle,
                backGroundHyperBlueCellStyle,
                backGroundHyperGreenCellStyle,
                backGroundHyperAmberCellStyle,
                backGroundHyperRedCellStyle,
                backGroundHyperGreyCellStyle,
                backGroundHyperWhiteCellStyle,
                backGroundHyperPinkCellStyle,
                backGroundHyperOrangeCellStyle,
                backGroundHyperCreamCellStyle,
                backGroundHyperCoralCellStyle,
                headerFont
            );
            this.Sheets[sheetName] = ActiveSheet;
            return ActiveSheet;
        }
        public abstract string Save();
    }
}