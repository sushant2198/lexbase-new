using HandlebarsDotNet;
using System;

namespace Lexbase.Providers.Models
{
    public class EmailTemplate
    {
        private readonly Func<object, string> subjectTemplate;
        private readonly Func<object, string> bodyTemplate;

        public EmailTemplate(string subject, string body)
        {
            subjectTemplate = Handlebars.Compile(subject);
            bodyTemplate = Handlebars.Compile(body);
        }

        public string GetSubject(object data = null)
        {
            return subjectTemplate(data ?? new { });
        }

        public string GetBody(object data = null)
        {
            return bodyTemplate(data ?? new { });
        }
    }
}