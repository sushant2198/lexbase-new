namespace Lexbase.Providers.Models
{
    public class Message
    {
        public string Body { get; set; }

        public string Subject { get; set; }

        public MessageAttachment Attachment { get; set; }
    }
}