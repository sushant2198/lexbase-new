﻿namespace Lexbase.Providers
{
    public interface ICacheProvider
    {
        void Remove(string key);
    }
}
