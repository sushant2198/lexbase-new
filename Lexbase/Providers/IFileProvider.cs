﻿namespace Lexbase.Providers
{
    public interface IFileProvider
    {
        string Upload(string name);
    }
}
