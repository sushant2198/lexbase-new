﻿namespace Lexbase.Providers
{
    public interface IBlobProvider
    {
        void DeleteTempBlob();
    }
}
