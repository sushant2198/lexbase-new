using Lexbase.Models;
using Lexbase.Providers.Models;
using System.Collections.Generic;

namespace Lexbase.Providers
{
    public interface IEmailProvider
    {
        void Send(
            Message message,
            User to,
            User from,
            IList<User> cc = null,
            IList<User> bcc = null);
    }
}