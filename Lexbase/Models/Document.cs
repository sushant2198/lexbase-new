namespace Lexbase.Models
{
    public class Document
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public GroupFor Type { get; set; }
    }
}