using System;

namespace Lexbase.Models
{
    public class Obligation
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public ObligationType Type { get; set; }
        public ObligationSubType SubType { get; set; }
        public ObligationState State { get; set; }
        public string ActionType { get; set; }
        public bool IsTrigger { get; set; }
        public bool IsFinancial { get; set; }
        public Account Account { get; set; }
        public GroupFor GroupFor { get; set; }
        public long GroupForId { get; set; }
        public string GroupForName { get; set; }
        public User OwnerUser { get; set; }
        public User OwnerManager { get; set; }
        public User SharedWith { get; set; }
        public Frequency Frequency { get; set; }
        public DateTime? UptoDate { get; set; }
        public DateTime? NextDueDate { get; set; }
        public DateTime? NextCreationDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public DateTime? DateModified { get; set; }
        public DateTime? DateCreated { get; set; }
        public Phase Phase { get; set; }
        public int RulesUpdated { get; set; }
        public Responsibility Responsibility { get; set; }

        // public IList<Responsibility> ResponsibilityList { get; set; }
        public string TimeZone { get; set; }

        public bool IsActive { get; set; }

        public bool InProcessing { get; set; }

        public bool IsAlerted
        {
            get
            {
                return ActionType == "A";
            }
        }

        public bool IsMonitored
        {
            get
            {
                return ActionType == "M";
            }
        }
    }
}