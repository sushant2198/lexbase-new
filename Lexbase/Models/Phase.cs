namespace Lexbase.Models
{
    public class Phase
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}