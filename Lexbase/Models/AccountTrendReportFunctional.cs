﻿using System.Collections.Generic;

namespace Lexbase.Models
{
    public class AccountTrendReportFunctional
    {
        public Account Account { get; set; }

        public Dictionary<ObligationType, IList<AccountTrendData>> List { get; set; }
    }
}
