﻿using System;
using System.Collections.Generic;

namespace Lexbase.Models
{
    public class AccountStatusData
    {
        public long Id { get; set; }
        public int ObligationType { get; set; }
        public double BragRatio { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public IList<long> ChildObligationIds { get; set; }
    }
}