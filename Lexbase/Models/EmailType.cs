namespace Lexbase.Models
{
    public enum EmailType
    {
        UserActivation,
        ACTIVATION_EMAIL,
        DE_ACTIVATION_EMAIL,
        PRE_ACTIVATION_REQUEST_MAIL,
        AMENDMENT_PREACTIVATION_REQUEST_MAIL,
        PRE_ACTIVATION_MAIL,
        OBLIGATION_TRIGGER_TO_NO_TRIGGER,
        ARCHIVED_ACCOUNT_EMAIL
    }
}