using System;
using System.Collections.Generic;

namespace Lexbase.Models
{
    public class Account
    {
        public long Id { get; set; }
        public bool? Sensitive { get; set; }
        public bool? Active { get; set; }
        public bool? InTransition { get; set; }
        public bool? InDumpGeneration { get; set; }
        public int InProcessing { get; set; }
        public int InReIndexing { get; set; }
        public int IsArchive { get; set; }
        public int IsActivate { get; set; }
        public int AccountAmendmentStatus { get; set; }
        public DateTime? AmendmentBannerDate { get; set; }
        public bool GlActive { get; set; }
        public bool AccessEmpty { get; set; }
        public bool CmAlerted { get; set; }
        public bool PreActivated { get; set; }
        public long UserId { get; set; }

        public string DataXml { get; set; }

        public string HistoricData { get; set; }

        public long CurrencyType { get; set; }
        public long ContractCurrency { get; set; }

        // TODO:
        // public IList<ContractEntity> ContractingEntity { get; set; }
        public string ContractingEntityName { get; set; }

        public int OfferingOrganization { get; set; }
        public string OrganizationName { get; set; }

        public string Name { get; set; }
        public string LegalName { get; set; }
        public string Alias { get; set; }
        public string Address { get; set; }

        public int LeadCountry { get; set; }
        public string LeadCountryName { get; set; }

        public Timezone TimeZone { get; set; }

        public decimal AccountValue { get; set; }
        public long AccountSize { get; set; }

        private int bragStatus = -1;
        public int BragStatus { get { return bragStatus; } set { bragStatus = value; } }

        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public DateTime? ReActivationDate { get; set; }

        public DateTime? SubmissionDate { get; set; }

        public AccountType AccountType { get; set; }

        public long VerticalId { get; set; }
        public Vertical Vertical { get; set; }

        public Region Region { get; set; }

        public IList<long> RegionIdList { get; set; }

        public IList<Document> DocumentList { get; set; }

        public IList<Document> NtDocumentList { get; set; }

        public IDictionary<long, User> UserMap { get; set; }

        public IList<User> NonManagers { get; set; }

        public IList<User> Managers { get; set; }

        public IList<User> AccountManagerList { get; set; }

        public IList<User> RegionalPOCList { get; set; }

        public User AccountExecutive { get; set; }

        public User ServiceDeliveryHead { get; set; }

        public User FinanceManager { get; set; }

        public User HRManager { get; set; }

        public User PhaseManager { get; set; }

        public User LeadManager { get; set; }

        public User ContractAdministrator { get; set; }

        public User ContractSpecialist { get; set; }

        public AccountCoverage AccountCoverage { get; set; }

        public IList<BusinessUnit> BusinessUnits { get; set; }
        public IList<DeliveryCountry> DeliveryCountries { get; set; }
        public IList<AccountCurrency> AccountCurrencies { get; set; }
        public IList<Vertical> VerticalList { get; set; }
    }
}