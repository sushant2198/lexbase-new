﻿namespace Lexbase.Models
{
    public class ChangeRequestCategory
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}