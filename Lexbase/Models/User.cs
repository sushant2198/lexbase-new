namespace Lexbase.Models
{
    public class User
    {
        public long Id { get; set; }

        public string LoginId { get; set; }
        public long LoginFailureCount { get; set; }
        public long ResetPasswordAttempt { get; set; }
        public string Password { get; set; }
        public string OldPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public long RoleId { get; set; }
        public string RoleName { get; set; }
        public string PreviousRoleName { get; set; }
        public long ClientId { get; set; }
        public long ClientName { get; set; }
        public Client Client { get; set; }

        public bool Active { get; set; }
        public bool UsCitizenship { get; set; }
        public bool SensitiveAccess { get; set; }
        public long ManagerId { get; set; }
        public long ManagerType { get; set; }
    }
}