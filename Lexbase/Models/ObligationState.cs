namespace Lexbase.Models
{
    public class ObligationState
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}