﻿using Lexbase.Enum;

namespace Lexbase.Models
{
    public class HeatMapData : AccountTrendData
    {
        public string Id { get; set; }

        public HeatMapReportGroup? ReportGroup { get; set; }

        public HeatMapReportTypes? Type { get; set; }

        //public List<HeatMapHighRisk> HighRisk { get; set; }
        public string Period { get; set; } // Upcoming, Total, Months ...

        public int? Count { get; set; }

        public User Owner { get; set; }

        public string Header { get; set; }
        public decimal HighriskId { get; set; }
        public string Description { get; set; }
        public string Penalty { get; set; }
        public string Duedate { get; set; }
        public string DaystillDue { get; set; }


    }
}
