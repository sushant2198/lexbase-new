﻿namespace Lexbase.Models
{
    public class UserAuditLogEvent
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
