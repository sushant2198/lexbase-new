namespace Lexbase.Models
{
    public enum ParentFor
    {
        Account = 1,
        MSA = 2,
        SOW = 3,
        SA = 5,
        SubSOW = 8
    }
}