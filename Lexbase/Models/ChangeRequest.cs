﻿using System;

namespace Lexbase.Models
{
    public class ChangeRequest
    {
        public long Id { get; set; }

        public DateTime Date { get; set; }

        public string Subject { get; set; }

        public string Reason { get; set; }

        public string Details { get; set; }

        public ChangeRequestType Type { get; set; }

        public User User { get; set; }

        public Account Account { get; set; }

        public string Link { get; set; }

    }
}