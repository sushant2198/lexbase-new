using System.Collections.Generic;

namespace Lexbase.Models
{
    public class AccountTrendReportCumulative
    {
        public Account Account { get; set; }

        public IList<AccountTrendData> List { get; set; }
    }
}
