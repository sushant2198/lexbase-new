﻿using System;
using System.Collections.Generic;

namespace Lexbase.Models
{
    public class AccountStatus
    {
        public long Id { get; set; }
        public int BragStatus { get; set; }
        public double BragRatio { get; set; }
        public Dictionary<long, double> RatioMap { get; set; }
        public int CurrentStatus { get; set; }
        public int ObligationType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Freezingtatus { get; set; }
    }
}