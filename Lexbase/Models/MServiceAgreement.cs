using System;

namespace Lexbase.Models
{
    public class MServiceAgreement
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public Account Account { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string Data { get; set; }
        public String VerticalName { get; set; }
        public int Mfc { get; set; }
        public int ThirdPartyIPO { get; set; }
        public int OffShoreServices { get; set; }
        public int TerminationAssistance { get; set; }
        public String SelectedExtract { get; set; }
        public String SelectedDocument { get; set; }
        public String SelectedReference { get; set; }
        public bool IsActive { get; set; }
    }
}