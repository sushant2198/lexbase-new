﻿namespace Lexbase.Models
{
    public class Frequency
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public int AdvanceByMonths { get; set; }
    }
}