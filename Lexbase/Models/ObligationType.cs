namespace Lexbase.Models
{
    public class ObligationType
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}