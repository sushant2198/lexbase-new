﻿using System.Collections.Generic;

namespace Lexbase.Models
{
    public class HeatMapReport
    {
        public Account Account { get; set; }
        public List<Dictionary<string, HeatMapData>> HighRiskData { get; set; }
        public List<Dictionary<string, HeatMapData>> TrendData { get; set; }
        public List<Dictionary<string, HeatMapData>> CSCs { get; set; }

        public List<Dictionary<string, HeatMapData>> CUSs { get; set; }

        public List<List<Dictionary<string, HeatMapData>>> Owners { get; set; }

        public Dictionary<string, HeatMapData> DueIn30Days { get; set; }

        public Dictionary<string, HeatMapData> DueIn90Days { get; set; }
    }

}
