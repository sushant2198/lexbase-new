﻿namespace Lexbase.Models
{
    public class AccountFreezingStatus
    {
        public long Id { get; set; }
        public bool FreezingStatus { get; set; }
    }
}