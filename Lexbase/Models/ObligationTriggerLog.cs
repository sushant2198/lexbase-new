﻿using System;

namespace Lexbase.Models
{
    public class ObligationTriggerLog
    {
        public long Id { get; set; }
        public ChildObligation ChildObligation { get; set; }
        public DateTime When { get; set; }
        public ReminderFrequency Reminder { get; set; }
    }
}