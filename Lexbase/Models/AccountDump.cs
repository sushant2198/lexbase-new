﻿using System.Collections.Generic;

namespace Lexbase.Models
{
    public class AccountDump
    {
        public Account Account { get; set; }

        public List<Document> Documents { get; set; }

        public IList<Obligation> Obligations { get; set; }

        public IList<ChildObligation> ChildObligations { get; set; }
    }
}