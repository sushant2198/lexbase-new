namespace Lexbase.Models
{
    public class AccountCurrency
    {
        public long AccountId { get; set; }
        public long ContractCurrency { get; set; }
    }
}