﻿using System.Collections.Generic;

namespace Lexbase.Models
{
    public class AccountInformationData
    {
        public long AccountId { get; set; }
        public string Name { get; set; }
        public string AccountType { get; set; }
        public string Status { get; set; }
        public string RegionName { get; set; }
        public string AccountValue { get; set; }
        public string CurrencyType { get; set; }
        public string AccountDateCreated { get; set; }
        public string IsActive { get; set; }
        public string AccountDateModified { get; set; }
        public string AccountDateConfirmed { get; set; }
        public string TimeZone { get; set; }
        public string Sensitive { get; set; }
        public string LegelName { get; set; }
        public string AccountSize { get; set; }
        public string Alias { get; set; }
        public string Address { get; set; }
        public string InTransition { get; set; }
        public string SubmissionDate { get; set; }
        public string LeadCountryName { get; set; }
        public IList<User> Managers { get; set; }
        public string AccountClassification { get; set; }
        public string DeliveryCountryName { get; set; }
        public string LeadCCM { get; set; }
    }
}
