namespace Lexbase.Models
{
    public enum GroupFor
    {
        Account = 1,
        MSA = 2,
        SOW = 3,
        SA = 5,
        SubSOW = 8,
        Obligation = 6
    }
}