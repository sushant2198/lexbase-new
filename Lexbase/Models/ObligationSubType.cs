namespace Lexbase.Models
{
    public class ObligationSubType
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}