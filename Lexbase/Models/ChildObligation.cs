﻿using System;

namespace Lexbase.Models
{
    public class ChildObligation
    {
        public long Id { get; set; }

        public DateTime Date { get; set; }

        public string ActionType { get; set; }

        public bool IsActive { get; set; }

        public bool GlActive { get; set; }

        public string Status { get; set; }

        public string StatusHistory { get; set; }

        public Obligation Parent { get; set; }

        public DateTime? DateCreated { get; set; }

        public DateTime? DateModified { get; set; }

        public DateTime? DateCompleted { get; set; }

        public DateTime? SubmissionDate { get; set; }

        public DateTime? CustomerApprovalDate { get; set; }
        public DateTime? DueDate { get; set; }
        public string FunctionalGroup { get; set; }
        public bool IsFinancial { get; set; }
        public bool ApprovalRequired { get; set; }
        public int ObligationType { get; set; }
    }
}