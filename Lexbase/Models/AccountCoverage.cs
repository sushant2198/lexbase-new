namespace Lexbase.Models
{
    public class AccountCoverage
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }
}