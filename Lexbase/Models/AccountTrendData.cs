﻿using System;

namespace Lexbase.Models
{
    public class AccountTrendData
    {
        public long ObligationTypeId { get; set; }
        public decimal? BragStatus { get; set; }
        public decimal? BragRatio { get; set; }
        public DateTime StartDate { get; set; }
    }
}
