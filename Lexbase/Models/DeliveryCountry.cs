namespace Lexbase.Models
{
    public class DeliveryCountry
    {
        public long AccountId { get; set; }
        public long ContractId { get; set; }
        public long CountryId { get; set; }
        public string CountryName { get; set; }
        public int GroupFor { get; set; }
    }
}