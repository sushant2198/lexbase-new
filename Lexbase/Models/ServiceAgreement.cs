namespace Lexbase.Models
{
    public class ServiceAgreement
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public Account Account { get; set; }
        public MServiceAgreement MSA { get; set; }
        public int Mfc { get; set; }
        public int ThirdPartyIPO { get; set; }
        public int OffShoreServices { get; set; }
        public int TerminationAssistance { get; set; }
        public string SelectedExtract { get; set; }
        public string SelectedDocument { get; set; }
        public string SelectedReference { get; set; }
        public dynamic IsActive { get; set; }
    }
}