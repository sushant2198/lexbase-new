﻿using System;

namespace Lexbase.Models
{
    public class UserAuditLog
    {
        public long Id { get; set; }
        public DateTime Date { get; set; }
        public UserAuditLogEvent Event { get; set; }
        public string GroupName { get; set; }
        public GroupFor GroupFor { get; set; }
        public long GroupId { get; set; }
        public User User { get; set; }
        public Account Account { get; set; }
        public ServiceAgreement SA { get; set; }
        public MServiceAgreement MSA { get; set; }
        public Obligation Obligation { get; set; }

    }
}
