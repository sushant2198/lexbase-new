namespace Lexbase.Models
{
    public class ReminderFrequency
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public Frequency Frequency { get; set; }
        public int DaysBack { get; set; }
    }
}