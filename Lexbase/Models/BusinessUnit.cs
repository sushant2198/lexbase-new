using System;

namespace Lexbase.Models
{
    public class BusinessUnit
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}