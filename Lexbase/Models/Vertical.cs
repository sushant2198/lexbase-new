namespace Lexbase.Models
{
    public class Vertical
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}