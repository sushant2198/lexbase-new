﻿using System;

namespace Lexbase.Models
{
    public class ScheduledReport
    {
        public long Id { get; set; }
        public string JobName { get; set; }
        public string JobParam { get; set; }
        public DateTime? StartedOn { get; set; }
        public DateTime? CompletedOn { get; set; }
        public string Error { get; set; }

    }
}