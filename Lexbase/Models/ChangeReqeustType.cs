﻿namespace Lexbase.Models
{
    public class ChangeRequestType
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public ChangeRequestCategory Category { get; set; }
    }
}