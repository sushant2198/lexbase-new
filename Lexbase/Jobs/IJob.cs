namespace Lexbase.Jobs
{
    public interface IJob
    {
        void Execute(string param);
    }
}