using Lexbase.Register;
using Microsoft.Extensions.DependencyInjection;
using Oracle.ManagedDataAccess.Client;
using System.Data;

namespace Lexbase
{
    public class Context
    {
        private static Context instance;

        private static readonly string contextLock = "contextLock";
        private static readonly string diLock = "diLock";

        //TODO Make it thread level
        public static Context Current
        {
            get
            {
                if (instance != null)
                {
                    return instance;
                }

                lock (contextLock)
                {
                    if (instance != null)
                    {
                        return instance;
                    }

                    instance = new Context();

                    return instance;
                }
            }
        }

        public bool IsDebug { get; private set; }

        private readonly ServiceCollection collection;
        private ServiceProvider serviceProvider;

        private Context()
        {
            collection = new ServiceCollection();

            IsDebug = UlxConfig.GetBool("IsDebug", true);
        }

        public IDbConnection GetDbConnection(string user)
        {
            var connectionString = UlxConfig.GetConnectionString(user);

            var connection = new OracleConnection(connectionString);

            connection.Open();

            return connection;
        }

        public void Register(IRegister register)
        {
            register.Register(collection);
        }

        public TInterface GetService<TInterface>()
        {
            if (serviceProvider == null)
            {
                lock (diLock)
                {
                    if (serviceProvider == null)
                    {
                        serviceProvider = collection.BuildServiceProvider();
                    }
                }
            }
            return serviceProvider.GetService<TInterface>();
        }
    }
}