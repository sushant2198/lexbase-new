using System;

namespace Lexbase.Exceptions
{
    public class JobException : Exception
    {
        public JobException()
        {
        }

        public JobException(string message) : base(message)
        {
        }
    }
}