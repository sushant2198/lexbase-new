using System.Collections.Generic;

namespace Lexbase
{
    public static class UnitedLexConstants
    {
        public static int FOR_ACCOUNT = 1;
        public static int FOR_MSA = 2;
        public static int FOR_SOW = 3;
        public static int FOR_SUBSOW = 8;
        public static int FOR_SEARCH = 4;
        public static int FOR_SA = 5;
        public static int FOR_OBLIGATION = 6;
        public static int FOR_CHILD_OBLIGATION = 7;

        public static int FOR_SUB_SOW = 8;
        public static int FOR_CHANGE_REQUEST = 9;

        public static int FOR_DOCUMENT = 10;
        public static int FOR_TRIGGER_LOG = 11;
        public static int FOR_AMENDMENT_REQUEST = 11;

        public static string OBLIGATION_RESPONSIBILITY_NONE = "None";

        public static IDictionary<long, string> OBLIGATION_RESPONSIBILITY_MAP = new Dictionary<long, string> {
            { 1L, UlxConfig.Get ("OBLIGATION_RESPONSIBILITY_TOKEN") },
            { 3L, UlxConfig.Get ("OBLIGATION_RESPONSIBILITY_TOKEN") + " Subcontractor" },
            { 2L, "Customer" },
        };

        public static IDictionary<long, string> OBLIGATION_PHASE_MAP = new Dictionary<long, string> {
            { 2L, "Steady Phase" },
            { 3L, "Termination" },
            { 1L, "Transition" }
        };

        public static IDictionary<long, string> ATOS_OBLIGATION_PHASE_MAP = new Dictionary<long, string> {
            { 2L, "Steady Phase" },
            { 3L, "Termination" },
            { 1L, "Transformation" },
            { 4L, "Initial Transition" },
            { 5L, "Close Out Transition" }
        };

        public static string NONSENSITIVE = "EMAIL_RECEIVER_UAT_NON_SENSITIVE";
        public static string SENSITIVE = "EMAIL_RECEIVER_UAT_SENSITIVE";

        public static short DEFAULT_ROW_HEIGHT = 500;
        public static int DEFAULT_COLUMN_WIDTH = 20;

        // public static IDictionary<int, string> WEEKDAY_MAP = new Dictionary<int, string> {
        // };
    }
}