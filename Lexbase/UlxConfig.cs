using HandlebarsDotNet;
using log4net;
using Microsoft.Azure.KeyVault;
using Microsoft.Azure.Services.AppAuthentication;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;

namespace Lexbase
{
    public static class UlxConfig
    {
        private static ILog log = LogManager.GetLogger(typeof(UlxConfig));
        private static IConfigurationRoot _configuration;
        private static IConfigurationSection _appSettings;
        private static IDictionary<string, Func<object, string>> templates = new Dictionary<string, Func<object, string>>();

        private static IConfigurationRoot configuration
        {
            get
            {
                if (_configuration == null)
                {
                    var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "production";
                    _configuration = new ConfigurationBuilder()
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                        .AddJsonFile($"appsettings.{environment.ToLower()}.json", optional: true, reloadOnChange: true)
                        .AddEnvironmentVariables()
                        .Build();
                }

                return _configuration;
            }
        }

        private static IConfigurationSection appSettings
        {
            get
            {
                if (_appSettings == null)
                {
                    _appSettings = configuration.GetSection("AppSettings");
                }

                return _appSettings;
            }
        }

        public static string GetConnectionString(string user)
        {
            var connectionString = configuration.GetConnectionString(user);

            if (connectionString.StartsWith("KeyVault:", StringComparison.Ordinal))
            {
                connectionString = GetFromVault(connectionString.Substring(9));
            }

            return connectionString;
        }

        public static string GetFromVault(string keyVaultKey)
        {
            AzureServiceTokenProvider azureServiceTokenProvider = new AzureServiceTokenProvider();

            KeyVaultClient keyVaultClient = new KeyVaultClient(new KeyVaultClient.AuthenticationCallback(azureServiceTokenProvider.KeyVaultTokenCallback));

            var value = keyVaultClient.GetSecretAsync(keyVaultKey).Result.Value;

            return value;
        }

        public static Func<object, string> GetTemplate(string key)
        {
            if (!templates.ContainsKey(key))
            {
                var value = Get(key);
                templates[key] = Handlebars.Compile(value);
            }

            return templates[key];
        }

        public static string Get(string key, string defaultValue = null)
        {
            // key = key.Replace(' ', '_');
            var value = appSettings[key];

            if (string.IsNullOrEmpty(value))
            {
                if (!key.Contains(":") && string.IsNullOrEmpty(defaultValue))
                {
                    return key;
                }
                return defaultValue;
            }

            if (value.StartsWith("KeyVault:", StringComparison.Ordinal))
            {
                value = GetFromVault(value.Substring(9));
            }
            return value;
        }

        public static string GetString(string key)
        {
            return Get(key);
        }

        public static int GetInteger(string key, int defaultValue = 0)
        {
            var value = Get(key);

            if (!int.TryParse(value, out int config))
            {
                return defaultValue;
            }

            return config;
        }

        public static long GetLong(string key, long defaultValue = 0)
        {
            var value = Get(key);

            if (long.TryParse(value, out long config))
            {
                return config;
            }
            return defaultValue;
        }

        public static bool GetBool(string key, bool defaultValue = false)
        {
            var value = Get(key);

            if (bool.TryParse(value, out bool config))
            {
                return config;
            }

            return defaultValue;
        }

        public static long LeadCCM
        {
            get { return GetLong("Managers:LeadCCM"); }
        }

        public static long SCCM
        {
            get { return GetLong("Managers:SCCM"); }
        }

        public static long AccountManagerType
        {
            get { return GetLong("ACCOUNT_MANAGER"); }
        }

        public static long AccountExecutiveType
        {
            get { return GetLong("ACCOUNT_EXECUTIVE"); }
        }

        public static long AccountLeadManagerType
        {
            get { return GetLong("LEAD_ACCOUNT_MANAGER"); }
        }

        public static long ServiceDeliveryType
        {
            get { return GetLong("SERVICE_DELIVERY_HEAD"); }
        }

        public static long FinanceManagerType
        {
            get { return GetLong("FINANCE_MANAGER"); }
        }

        public static long HRManagerType
        {
            get { return GetLong("HUMAN_RESOURCE_MANAGER"); }
        }

        public static long PhaseManagerType
        {
            get { return GetLong("PHASE_MANAGER"); }
        }

        public static long ContractAdministratorType
        {
            get { return GetLong("CONTRACTS_ADMINISTRATOR"); }
        }

        public static long ContractSpecialistType
        {
            get { return GetLong("CONTRACTS_SPECIALIST"); }
        }

        public static long RegionalPOCType
        {
            get { return GetLong("REGIONAL_POC"); }
        }


        public static string TimeZone
        {
            get { return Get("APPLICATION_TIMEZONE"); }
        }

        public static string CurrentFolder
        {
            get
            {
                return Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            }
        }

        public static string EmailTemplates
        {
            get
            {
                return Get("EMAIL_TEMPLATES", Path.Combine(CurrentFolder, "Templates/Emails"));
            }
        }
        public static string ExcelTemplates
        {
            get
            {
                return Get("EXCEL_TEMPLATES", Path.Combine(CurrentFolder, "Templates/Excels"));
            }
        }
    }
}