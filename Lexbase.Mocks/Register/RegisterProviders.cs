using Lexbase.Implementation.Providers;
using Lexbase.Mocks.Providers;
using Lexbase.Providers;
using Lexbase.Register;
using Microsoft.Extensions.DependencyInjection;

namespace Lexbase.Mocks.Register
{
    public class RegisterProviders : IRegister
    {
        public void Register(ServiceCollection collection)
        {
            collection.AddScoped<IExcelProvider, ExcelProvider>();
            collection.AddScoped<IFileProvider, MockedBlobProvider>();
            collection.AddScoped<ITemplateProvider, TemplateProvider>();
            collection.AddScoped<IEmailProvider, MockedEmailProvider>();
            collection.AddScoped<ICacheProvider, MockedCacheProvider>();
            collection.AddScoped<IIndexProvider, IndexProvider>();
            collection.AddScoped<IBlobProvider, BlobProvider>();

        }
    }
}