﻿using Lexbase.Providers;
using log4net;

namespace Lexbase.Mocks.Providers
{
    public class MockedCacheProvider : ICacheProvider
    {
        private readonly ILog log = LogManager.GetLogger(typeof(MockedCacheProvider));

        public void Remove(string key)
        {
            var logContext = $"[Remove:{key}]";
            log.Debug($"{logContext} Done");
        }
    }
}
