﻿using Lexbase.Providers;
using log4net;

namespace Lexbase.Mocks.Providers
{
    public class MockedBlobProvider : IFileProvider
    {
        private readonly ILog log = LogManager.GetLogger(typeof(MockedBlobProvider));

        public string Upload(string name)
        {
            var logContext = $"[Upload:{name}]";
            log.Debug($"{logContext} Done");
            return name;
        }
    }
}
