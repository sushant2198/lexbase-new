using Lexbase.Models;
using Lexbase.Providers;
using Lexbase.Providers.Models;
using log4net;
using System;
using System.Collections.Generic;

namespace Lexbase.Mocks.Providers
{
    public class MockedEmailProvider : IEmailProvider
    {
        private readonly ILog log = LogManager.GetLogger(typeof(MockedEmailProvider));

        public void Send(Message message, User to, User from, IList<User> cc = null, IList<User> bcc = null)
        {
            var ccEmail = "";
            var bccEmail = "";

            if (cc != null)
            {
                foreach (var item in cc)
                {
                    ccEmail += $"{item.Email};";
                }
            }
            else
            {
                var configCCEmail = UlxConfig.Get("Emails:CC:Email");

                if (!string.IsNullOrEmpty(configCCEmail))
                {
                    if (configCCEmail.Contains(';'))
                    {
                        var emails = configCCEmail.Split(';', StringSplitOptions.RemoveEmptyEntries);
                        foreach (var item in emails)
                        {
                            ccEmail += $"{item};";
                        }
                    }
                    else
                    {
                        var ccFirstName = UlxConfig.Get("Emails:CC:Firstname");
                        var ccLastName = UlxConfig.Get("Emails:CC:LastName");
                        if (string.IsNullOrEmpty(ccFirstName))
                        {
                            ccEmail += $"{ccFirstName} {ccLastName} <{configCCEmail}>";
                        }
                        else
                        {
                            ccEmail += $"{configCCEmail}";
                        }
                    }
                }
            }

            if (bcc != null)
            {
                foreach (var item in bcc)
                {
                    bccEmail += $"{item.Email};";
                }
            }
            else
            {
                var configBCCEmail = UlxConfig.Get("Emails:BCC:Email");

                if (!string.IsNullOrEmpty(configBCCEmail))
                {
                    if (configBCCEmail.Contains(';'))
                    {
                        var emails = configBCCEmail.Split(';', StringSplitOptions.RemoveEmptyEntries);
                        foreach (var item in emails)
                        {
                            ccEmail += $"{item};";
                        }
                    }
                    else
                    {
                        var bccFirstName = UlxConfig.Get("Emails:BCC:Firstname");
                        var bccLastName = UlxConfig.Get("Emails:BCC:LastName");
                        if (string.IsNullOrEmpty(bccFirstName))
                        {
                            ccEmail += $"{bccFirstName} {bccLastName} <{configBCCEmail}>";
                        }
                        else
                        {
                            ccEmail += $"{configBCCEmail}";
                        }
                    }
                }
            }

            log.Info(new
            {
                subject = message.Subject,
                body = message.Body,
                to = new
                {
                    Email = to.Email,
                    FirstName = to.FirstName,
                    LastName = to.LastName
                },
                cc = ccEmail,
                bcc = bccEmail,
                attchment = message.Attachment != null ? message.Attachment.Path : "",
                smtp = new
                {
                    Port = UlxConfig.GetInteger("SMTP:Port"),
                    Host = UlxConfig.Get("SMTP:Host"),
                    UserName = UlxConfig.Get("SMTP:UserName"),
                    Password = UlxConfig.Get("SMTP:Password")
                }
            });
        }
    }
}